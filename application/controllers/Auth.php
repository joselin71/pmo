<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	public function __construct(){
        parent::__construct();
        $this->load->model("Usuarios_model");

    }
	public function index()
	{
        //traer el nivel del usuario, para ver que pueden hacer
        if($this->session->userdata("login")){
            //logueado vuelve a la vista
            redirect(base_url()."principal");
        }
        else{
            $this->load->view('admin/login');
        }
	 
    }
    /*Roles:1)Operadores.3)Departamento Compras.11)Secretarios.15)Operadores multiSecretarias.16)Operadores Hacienda.99)Admin*/

    public function login()
    {
        $username=$this->input->post("username");
        $password=$this->input->post("password");
        $res=$this->Usuarios_model->login($username,$password);
        //var_dump($res);
        
        if(!$res){
            $this->session->set_flashdata("error","Usuario y/o Clave Incorrecta");
            redirect(base_url()."auth");
        }else{
            if($res->nivel==2){$rol=2;}
            if($res->nivel==3){$rol=3;}
            if($res->nivel==11){$rol=11;}
            if($res->nivel==15){$rol=15;}
            if($res->nivel==16){$rol=16;}
            if($res->nivel==99){$rol=99;}
            

            $data=array(
                
                'id'=>$res->id,
                'nombre'=>$res->apynom,
                'rol'=>$rol,
                'usuario'=>$res->dni,
                'unidad'=>$res->secretaria,
                'unidadN'=>$res->nomsec,
                'login'=>TRUE
            );
            
            $this->session->set_userdata($data);
            redirect(base_url()."principal");
            
            
            
        }
    }
    
    public function logout(){
        $this->session->sess_destroy();
        redirect(base_url());
    }

   
}
