<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuarios extends CI_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->model('Usuarios_model');
        

    }
    public function index()
    {  
        $data=array(
            'usuarios'=>$this->Usuarios_model->getUsuarios(),
        );
        
        $this->load->view('layouts/header');
        $this->load->view('layouts/aside');
        $this->load->view('admin/usuarios/list',$data);
        $this->load->view('layouts/footer');
		
    }
    /**Formulario para agregar un usuario */
    public function add()
    {
        $dependencia=$this->Usuarios_model->getSecretarias();
        $data=array(
            'dependencia'=>$dependencia
        );
        $this->load->view('layouts/header');
        $this->load->view('layouts/aside');
        $this->load->view('admin/usuarios/add',$data);
        $this->load->view('layouts/footer');
    }
    /**Agrega un usuario */
    public function storeUser()
    {
        $dni=$this->input->post("dni");
        $res=$this->Usuarios_model->getUsuarioxDNI($dni);
        if($res)
        {
            $this->session->set_flashdata("error","Ya existe el Usuario-Verifique los Datos ingresados");
			redirect(base_url()."mantenimiento/usuarios");
        }
        else
        {
            $apynom=strtoupper($this->input->post("apellido"));
            $level=$this->input->post("nivel");
            switch ($level) {
                case '1':
                    $nivel=2;
                    break;
                case '2':
                    $nivel=16;
                    break;
                case '3':
                    $nivel=3;
                    break;
                case '4':
                    $nivel=11;
                    break;
            }
            $secretaria=$this->input->post("secret");
            $correo=$this->input->post("correo");
            $tipo=$this->input->post("tipouser");
            $estado="A";$clave=md5($dni);
            $data = array(
                'apynom' =>$apynom ,
                'dni'   =>$dni,
                'clave' =>$clave,
                'nivel' =>$nivel,
                'estado'=>$estado,
                'secretaria'=>$secretaria,
                'email'=>$correo,
                'horas'=>$tipo
             );
             if($this->Usuarios_model->saveUsuarios($data))
             {
                echo "<script>alert('Usuario Agregado Correctamente');</script>";
                $ruta=base_url().'/administrador/usuarios';
                echo "<script>window.location='".$ruta."';</script>";
             }
             else
             {
                $this->session->set_flashdata("error","No se pudo dar de Alta,Verificar");
                redirect(base_url()."administrador/usuarios/add");
             }

        }

    }
    /**Formulario de edicion de Usuario */
    public function editUser($id)
    {
        $dependencia=$this->Usuarios_model->getSecretarias();
        $usuario=$this->Usuarios_model->getUsuarioxID($id);
        $data = array(
            'usuario' =>$usuario,
            'dependencia'=>$dependencia,
        );
       // var_dump($data);/*
        $this->load->view('layouts/header');
        $this->load->view('layouts/aside');
        $this->load->view('admin/usuarios/edit',$data);
        $this->load->view('layouts/footer');

    }
    /**Actualiza los datos del usuario */
    public function storeEdUser($id)
    {
        $apynom=strtoupper($this->input->post("apellido"));
        $level=$this->input->post("nivel");
        switch ($level) {
            case '1':
                $nivel=2;break;
            case '2':
                $nivel=16;break;
            case '3':
                $nivel=3;break;
            case '4':
                $nivel=11;break;
            }
            $secretaria=$this->input->post("secret");
            $correo=$this->input->post("correo");
            $tipo=$this->input->post("tipouser");
            $estado="A";
            $data = array(
                'apynom' =>$apynom ,
                'nivel' =>$nivel,
                'estado'=>$estado,
                'secretaria'=>$secretaria,
                'email'=>$correo,
                'horas'=>$tipo
             );
             if($this->Usuarios_model->saveEdUsuarios($id,$data))
             {
                echo "<script>alert('Usuario Modificado Correctamente');</script>";
                $ruta=base_url().'/administrador/usuarios';
                echo "<script>window.location='".$ruta."';</script>";
             }
             else
             {
                $this->session->set_flashdata("error","No se pudo dar de Alta,Verificar");
                redirect(base_url()."administrador/usuarios/editUser/".$id);
             }

    }
    /**Eliminar-cambiar de estado usuarios */
    public function deleteUser($id)
    {
        $data = array('estado' =>"I" , );
        if($this->Usuarios_model->deleteUsuario($id,$data))
        {
            echo "<script>alert('Usuario Desactivado Correctamente');</script>";
            $ruta=base_url().'/administrador/usuarios';
            echo "<script>window.location='".$ruta."';</script>";

        }
        else
        {
            $this->session->set_flashdata("error","No se pudo Eliminar-Reintente");
            redirect(base_url()."administrador/usuarios");

        }

    }
    /**Blanquear clave de usuario */
    public function blanqueaUser($id)
    {
        $new=md5("12345");
        $data = array('clave' =>$new , );
        if($this->Usuarios_model->blanqueaUsuario($id,$data))
        {
            echo "<script>alert('Calve Blanqueada Correctamente');</script>";
            $ruta=base_url().'/administrador/usuarios';
            echo "<script>window.location='".$ruta."';</script>";

        }
        else
        {
            $this->session->set_flashdata("error","No se pudo Eliminar-Reintente");
            redirect(base_url()."administrador/usuarios");

        }

    }
    /**Formulario de modificacion de clave de usuario por parte del usuario */
    public function modificaClave()
    {
        $this->load->view("layouts/header");
        $this->load->view("layouts/aside");
        $this->load->view("admin/usuarios/modiClave");
        $this->load->view("layouts/footer");
    }
    /**Modifica la clave del usuario  */
    public function confirmaNewPass()
    {
        $dni=trim($this->input->post("dni"));
        $ant=trim($this->input->post("ant"));
        $new=trim($this->input->post("new"));
        if($this->Usuarios_model->getClave($dni,md5($ant)))
        {
            $clave=md5($new);
            $data = array('clave' =>$clave , );
            if($this->Usuarios_model->updateClave($dni,$data))
            {
                echo "<script>alert('Calve Modificada Correctamente');</script>";
                $ruta=base_url().'principal';
                echo "<script>window.location='".$ruta."';</script>";
            }
            else
            {
                $this->session->set_flashdata("error","No se pudo Modificar-Reintente");
                redirect(base_url()."administrador/usuarios/modificaClave");

            }
        }
        else
            {
                $this->session->set_flashdata("error","Clave Anterior Erronea-Reintente");
                redirect(base_url()."administrador/usuarios/modificaClave");

            }


    }
}