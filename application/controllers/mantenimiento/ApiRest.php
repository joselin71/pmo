<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//use chriskacerguis\RestServer\RestController;
//use ApiRest\Libraries\REST_Controller;
require APPPATH . 'libraries/REST_Controller.php';
/*require APPPATH.'libraries/Format.php';*/

class ApiRest extends REST_Controller {
    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->database();
        $this->load->model("Catalogo_model");
    }
    public function index_get()
    {
        $this->load->view("layouts/header");
        $this->load->view("layouts/aside");
        $this->load->view("admin/api/apiDoc");
        $this->load->view("layouts/footer");
    }
    public function apiCatalogo_get()
    {
        $catalogo=$this->Catalogo_model->getArticulos();
        if($catalogo)
        {
            $this->response($catalogo, REST_Controller::HTTP_OK);
        }
        else
        {
            $this->response([
                'status' => FALSE,
                'message' => 'No articulo were found'
            ], REST_Controller::HTTP_NOT_FOUND);
        }


    }
    public function apiCatalogoID_get($id)
    {
        $catalogo=$this->Catalogo_model->getArticuloxID($id);
        if($catalogo)
        {
            $this->response($catalogo, REST_Controller::HTTP_OK);
        }
        else
        {
            $this->response([
                'status' => FALSE,
                'message' => 'No articulo were found'
            ], REST_Controller::HTTP_NOT_FOUND);
        }

    }
  }
