<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Catalogo extends CI_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->model('Catalogo_model');
      
  
    }
     /**Formulario para el CRUD de Rubros */
     public function rubros()
     {
         $res=$this->Catalogo_model->getRubros();
         if($res)
         {
            $datos = array(
                'rubros' =>$res ,
                );
         }else{ $datos = array(""); }
         
         $this->load->view("layouts/header");
         $this->load->view("layouts/aside");
         $this->load->view("admin/catalogo/listRubro",$datos);
         $this->load->view("layouts/footer");

     }
     /**Formulario de Alta de Rubros Excepcional */
     public function addRubro()
     {
        $this->load->view("layouts/header");
        $this->load->view("layouts/aside");
        $this->load->view("admin/catalogo/addRubro");
        $this->load->view("layouts/footer");

     }
     /**Create un nuevo rubro siempre y cuando no exista */
     public function storerubro()
     {
         $nrubro=$this->input->post("numrub");
         $descrubro=strtoupper(trim($this->input->post("detrub")));
         /**Validacion del Formulario */
         $this->form_validation->set_rules('numrub',' Rubro','required|is_unique[rubros.rubro]');
         if($this->form_validation->run())
         {
             $data = array(
                 'rubro' =>$nrubro ,
                 'rubrodesc'=>$descrubro,
                 'fecha_alta'=>date("Y-m-d"),
                );
             $res=$this->Catalogo_model->addRubro($data);
             if($res)
             {
                 $this->session->set_flashdata('correcto','Rubro Agregado Correctamente');
                 redirect(base_url().'mantenimiento/catalogo/addRubro');

             }
             else
             {
                 $this->session->set_flashdata('error','No se agrego el Rubro');
                 redirect(base_url().'mantenimiento/catalogo/addRubro');

             }

         }  
         else
         {
             $this->session->set_flashdata('error','No se pudo grabar el Rubro');
             $this->addRubro();

         }

     }
     /**Formulario para la edicion de un Rubro */
     public function editRubro($id)
     {
        $data=array(
			'lista'=>$this->Catalogo_model->getRubroxID($id),
        );
        $this->load->view("layouts/header");
        $this->load->view("layouts/aside");
        $this->load->view("admin/catalogo/editRubro",$data);
        $this->load->view("layouts/footer");
     }
     /**Actualiza los datos del Rubro */
     public function edstorerubro()
     {
        $id=$this->input->post("idr");
        $nrubro=$this->input->post("numrub");
        $descrubro=strtoupper(trim($this->input->post("detrub")));
        $estado=$this->input->post("estad");
        $rubroactual=$this->Catalogo_model->getRubroxID($id);
        if($nrubro==$rubroactual->rubro)
        {$unique='';}
        else{$unique='|is_unique[rubros.rubro]';}
        /**Validacion del Formulario */
        $this->form_validation->set_rules('numrub',' Rubro','required'.$unique);
        if($this->form_validation->run())
        {
            $data = array(
                'rubro' =>$nrubro ,
                'rubrodesc'=>$descrubro,
                'estado'=>$estado,
                'fecha_update'=>date("Y-m-d"),
               );
            $res=$this->Catalogo_model->updateRubro($data,$id);
            if($res)
            {
                /*Si cambia el estado tambien debe cambiar el se los subrubros de este rubro y de los articulos integrantes */
                $dataSub = array(
                    'estado' =>$estado ,
                    'fecha_update'=>date("Y-m-d"),
                    );
                $res2=$this->Catalogo_model->updateSubrubro($dataSub,$id);
                $this->session->set_flashdata('correcto','Rubro Modificado Correctamente');
                redirect(base_url().'mantenimiento/catalogo/rubros');

            }
            else
            {
                $this->session->set_flashdata('error','No se modifico el Rubro');
                redirect(base_url().'mantenimiento/catalogo/editRubro/'.$id);

            }

        }  
        else
        {
            $this->session->set_flashdata('error','No se pudo grabar el Rubro');
            $this->addRubro();

        }
     }
     /**Formulario para el CRUD de Sub-Rubros */
     public function subrubros()
     {
         $res=$this->Catalogo_model->getSubRubros();
         if($res)
         {
            $datos = array(
                'subrubros' =>$res ,
                );
         }else{ $datos = array(""); }
         
         $this->load->view("layouts/header");
         $this->load->view("layouts/aside");
         $this->load->view("admin/catalogo/listSubRubro",$datos);
         $this->load->view("layouts/footer");

     }
   
      /**Formulario de Alta de Sub-Rubros Excepcional */
      public function addSubRubro()
      {
         $data = array('rubros' =>$this->Catalogo_model->getRubros() , );
         $this->load->view("layouts/header");
         $this->load->view("layouts/aside");
         $this->load->view("admin/catalogo/addSubRubro",$data);
         $this->load->view("layouts/footer");
 
      }
      /**Create un nuevo rubro siempre y cuando no exista */
      public function storesubrubro()
      {
          $nror=$this->input->post("rubro");
          $nsrubro=$this->input->post("numsrub");
          $descsrubro=strtoupper(trim($this->input->post("detsrub")));
          $rubro=$this->Catalogo_model->getRubroxID($nror);
          /**Validacion del Formulario */
          $this->form_validation->set_rules('numsrub',' Sub-Rubro','required|is_unique[subrubros.subrubro]');
          if($this->form_validation->run())
          {
              $data = array(
                  'subrubro' =>$nsrubro ,
                  'subrubdesc'=>$descsrubro,
                  'irubro'=>$rubro->rubro,
                  'idrubro'=>$nror,
                  'fecha_alta'=>date("Y-m-d"),
                 );
              $res=$this->Catalogo_model->addSRubro($data);
              if($res)
              {
                  $this->session->set_flashdata('correcto','Sub-Rubro Agregado Correctamente');
                  redirect(base_url().'mantenimiento/catalogo/subrubros');
 
              }
              else
              {
                  $this->session->set_flashdata('error','No se agrego el Rubro');
                  redirect(base_url().'mantenimiento/catalogo/addSubRubro');
 
              }
 
          }  
          else
          {
              $this->session->set_flashdata('error','No se pudo grabar el Sub-Rubro');
              $this->addSubRubro();
 
          }
 
      }
      /**Formulario para la edicion de un Rubro */
      public function editsubRubro($id)
      {
         $data=array(
             'lista'=>$this->Catalogo_model->getSubRubroxID($id),
             'rubros' =>$this->Catalogo_model->getRubros() ,
         );
         $this->load->view("layouts/header");
         $this->load->view("layouts/aside");
         $this->load->view("admin/catalogo/editSubRubro",$data);
         $this->load->view("layouts/footer");
      }
      /**Actualiza los datos del Rubro */
      public function edstoresubrubro()
      {
         $rubro=$this->input->post("rubro");//nuevo rubro
         $nrubro=$this->Catalogo_model->getRubroxID($rubro);

         $id=$this->input->post("idr");
         //$nrubro=$this->input->post("numrub");//rubro actual
         $snrubro=$this->input->post("numsrub");
         $descsrubro=strtoupper(trim($this->input->post("detsubrub")));
         $estado=$this->input->post("estad");
         $subrubroactual=$this->Catalogo_model->getSubRubroxID($id);
         if($snrubro==$subrubroactual->subrubro)
         {$unique='';}
         else{$unique='|is_unique[subrubros.subrubro]';}
         /**Validacion del Formulario */
         $this->form_validation->set_rules('numsrub',' Sub-Rubro','required'.$unique);
         if($this->form_validation->run())
         {
             $data = array(
                 'subrubro'=>$snrubro,
                 'subrubdesc'=>$descsrubro,
                 'irubro' =>$nrubro->rubro ,
                 'idrubro'=>$rubro,                 
                 'estado'=>$estado,
                 'fecha_update'=>date("Y-m-d"),
                );
                var_dump($id);var_dump($data);
             $res=$this->Catalogo_model->updateSubRubro($data,$id);
             if($res)
             {
                 $this->session->set_flashdata('correcto','Sub-Rubro Modificado Correctamente');
                 redirect(base_url().'mantenimiento/catalogo/subrubros');
 
             }
             else
             {
                 $this->session->set_flashdata('error','No se modifico el Sub-Rubro');
                 redirect(base_url().'mantenimiento/catalogo/editSubRubro/'.$id);
 
             }
 
         }  
         else
         {
             $this->session->set_flashdata('error','No se pudo grabar el Sub-Rubro-reintente');
             $this->editSubRubro($id);
 
         }
      }
      /**Listado Principal CRUD Depositos */
      public function depositos()
      {
          $data = array('lista' =>$this->Catalogo_model->getDepositos() , );
          $this->load->view("layouts/header");
          $this->load->view('layouts/aside');
          $this->load->view('admin/catalogo/listDeposito',$data);
          $this->load->view('layouts/footer');
      }
      /**Formulario para dar de alta un nuevo Deposito */
      public function addDeposito()
      {
          $this->load->view('layouts/header');
          $this->load->view('layouts/aside');
          $this->load->view('admin/catalogo/addDeposito');
          $this->load->view('layouts/footer');
      }
      /**Genera un nuevo deposito */
      public function storedeposito()
      {
          $nombre=trim(strtoupper($this->input->post('detdep')));
          $data = array('nombre' =>$nombre , );
          $res=$this->Catalogo_model->addDeposito($data);
             if($res)
             {
                 $this->session->set_flashdata('correcto','Deposito Creado Correctamente');
                 redirect(base_url().'mantenimiento/catalogo/depositos');
 
             }
             else
             {
                 $this->session->set_flashdata('error','No se genero el Deposito');
                 redirect(base_url().'mantenimiento/catalogo/addDeposito');
 
             }
       }
       /**Formulario para la edicion de un Deposito */
     public function editDeposito($id)
     {
        $data=array(
			'lista'=>$this->Catalogo_model->getDepositoxID($id),
        );
        $this->load->view("layouts/header");
        $this->load->view("layouts/aside");
        $this->load->view("admin/catalogo/editDeposito",$data);
        $this->load->view("layouts/footer");
     }
     /**Actualiza los datos del Deposito,nombre y estado */
     public function edstoreDeposito()
     {
        $id=$this->input->post("idr");
        $descripcion=strtoupper(trim($this->input->post("detdep")));
        $estado=$this->input->post("estad");
        $data = array(
                'nombre' =>$descripcion ,
                'estado'=>$estado,
               );
        $res=$this->Catalogo_model->updateDeposito($data,$id);
        if($res)
        {
            $this->session->set_flashdata('correcto','Deposito Modificado Correctamente');
            redirect(base_url().'mantenimiento/catalogo/depositos');
        }
        else
        {
            $this->session->set_flashdata('error','No se modifico el Deposito');
            redirect(base_url().'mantenimiento/catalogo/editDeposito/'.$id);
        }
     }
     /**Formulario para Crud de unidades de Medida */
     public function unidadesMedida()
     {
         $data = array('lista' =>$this->Catalogo_model->getUndMedida() , );
         $this->load->view("layouts/header");
         $this->load->view("layouts/aside");
         $this->load->view("admin/catalogo/listUndMed",$data);
         $this->load->view("layouts/footer");
     }
      /**Formulario de Alta de Unidades de Medida */
      public function addUndMedida()
      {
         $this->load->view("layouts/header");
         $this->load->view("layouts/aside");
         $this->load->view("admin/catalogo/addUndMedida");
         $this->load->view("layouts/footer");
 
      }
      /**Create una nueva unidad de medida siempre y cuando no exista */
      public function storeundmedida()
      {
          $codigo=strtoupper(trim($this->input->post("codund")));
          $descripcion=strtoupper(trim($this->input->post("detcod")));
          $valor=$this->input->post("valor");
          /**Validacion del Formulario */
          $this->form_validation->set_rules('codund',' Codigo','required|is_unique[unidadmedida.cod]');
          if($this->form_validation->run())
          {
              $data = array(
                  'cod' =>$codigo ,
                  'descripcion'=>$descripcion,
                  'valor'=>$valor,
                 );
              $res=$this->Catalogo_model->addUndMedida($data);
              if($res)
              {
                  $this->session->set_flashdata('correcto','Unidad de Medida Agregada Correctamente');
                  redirect(base_url().'mantenimiento/catalogo/unidadesMedida');
 
              }
              else
              {
                  $this->session->set_flashdata('error','No se agrego la Unidad de Medida');
                  redirect(base_url().'mantenimiento/catalogo/addUndMedida');
 
              }
 
          }  
          else
          {
              $this->session->set_flashdata('error','No se pudo grabar la Unidad de Medida');
              $this->addUndMedida();
 
          }
 
      }
      /**Formulario para la edicion de una unidad de Medida */
      public function editUndMedida($id)
      {
         $data=array(
             'lista'=>$this->Catalogo_model->getUndMedidaxID($id),
         );
         $this->load->view("layouts/header");
         $this->load->view("layouts/aside");
         $this->load->view("admin/catalogo/editUndMedida",$data);
         $this->load->view("layouts/footer");
      }
      /**Actualiza los datos de la Unidad de Medida */
      public function edstoreundmedida()
      {
         $id=$this->input->post("idr");
         $codigo=strtoupper(trim($this->input->post("codund")));
         $descripcion=strtoupper(trim($this->input->post("detcod")));
         $valor=$this->input->post("valor");
         $estado=$this->input->post("estad");
         $codactual=$this->Catalogo_model->getUndMedidaxID($id);
         if($codigo==$codactual->cod)
         {$unique='';}
         else{$unique='|is_unique[unidadmedida.cod]';}
         /**Validacion del Formulario */
         $this->form_validation->set_rules('codund',' Codigo','required'.$unique);
         if($this->form_validation->run())
         {
             $data = array(
                 'cod' =>$codigo ,
                 'descripcion'=>$descripcion,
                 'estado'=>$estado,
                 'valor'=>$valor,
                );
             $res=$this->Catalogo_model->updateUndMedida($data,$id);
             if($res)
             {
                 $this->session->set_flashdata('correcto','Unidad de Medida Modificada Correctamente');
                 redirect(base_url().'mantenimiento/catalogo/unidadesmedida');
 
             }
             else
             {
                 $this->session->set_flashdata('error','No se modifico la Unidad de Medida');
                 redirect(base_url().'mantenimiento/catalogo/editUndMedida/'.$id);
 
             }
 
         }  
         else
         {
             $this->session->set_flashdata('error','No se pudo grabar la Unidad de Medida');
             $this->addUndMedida();
 
         }
      }
      /**Formulario para Crud de Proveedores */
     public function proveedores()
     {
         $data = array('lista' =>$this->Catalogo_model->getProveedores() , );
         $this->load->view("layouts/header");
         $this->load->view("layouts/aside");
         $this->load->view("admin/catalogo/listProveedores",$data);
         $this->load->view("layouts/footer");
     }
      /**Formulario de Alta Proveedores */
      public function addProveedor()
      {
         $this->load->view("layouts/header");
         $this->load->view("layouts/aside");
         $this->load->view("admin/catalogo/addProveedor");
         $this->load->view("layouts/footer");
 
      }
      /**Create un Proveedor siempre y cuando no exista(Numero) */
      public function storeproveedor()
      {
          $codigo=strtoupper(trim($this->input->post("nroProv")));
          $cuit=trim($this->input->post("numct"));
          $razonsoc=strtoupper(trim($this->input->post("razon")));
          $fantasiasoc=strtoupper(trim($this->input->post("fanta")));
          $domfiscal=strtoupper(trim($this->input->post("fiscal")));
          $domreal=strtoupper(trim($this->input->post("real")));
          $domlocal=strtoupper(trim($this->input->post("local")));
          $correo=trim($this->input->post("email"));
          $contacto=trim($this->input->post("contacto"));
          $convenio=trim($this->input->post("convmt"));
          $postal=trim($this->input->post("cp"));

          /**Validacion del Formulario */
          $this->form_validation->set_rules('nroProv',' Numero','required|is_unique[proveedores.nroProv]');
          if($this->form_validation->run())
          {
              $data = array(
                  'nroProv' =>$codigo ,
                  'cuit'=>$cuit,
                  'razonsocial'=>$razonsoc,
                  'razonfantasia'=>$fantasiasoc,
                  'domiciliofiscal'=>$domfiscal,
                  'domicilioreal'=>$domreal,
                  'localidad'=>$domlocal,
                  'email'=>$correo,
                  'tel'=>$contacto,
                  'conv_multi'=>$convenio,
                  'cp'=>$postal,
                  'fechaalta'=>date("Y-m-d"),
                  'estado'=>"A",
                 );
              $res=$this->Catalogo_model->addProveedor($data);
              if($res)
              {
                  $this->session->set_flashdata('correcto','Proveedor Agregado Correctamente');
                  redirect(base_url().'mantenimiento/catalogo/proveedores');
 
              }
              else
              {
                  $this->session->set_flashdata('error','No se agrego el Proveedor');
                  redirect(base_url().'mantenimiento/catalogo/addProveedor');
 
              }
 
          }  
          else
          {
              $this->session->set_flashdata('error','No se pudo grabar el Proveedor');
              $this->addProveedor();
 
          }
 
      }
      /**Formulario para la edicion de un Proveedor ,analizar si se inhabilitan los articulos o si son articulos genericos*/
      public function editProveedor($id)
      {
         $data=array(
             'lista'=>$this->Catalogo_model->getProveedorxID($id),
         );
         $this->load->view("layouts/header");
         $this->load->view("layouts/aside");
         $this->load->view("admin/catalogo/editProveedor",$data);
         $this->load->view("layouts/footer");
      }
      /**Actualiza los datos del Proveedor */
      public function edstoreProveedor()
      {
         $id=$this->input->post("idr");
         $codigo=strtoupper(trim($this->input->post("nroProv")));
         $cuit=trim($this->input->post("numct"));
         $razonsoc=strtoupper(trim($this->input->post("razon")));
         $fantasiasoc=strtoupper(trim($this->input->post("fanta")));
         $domfiscal=strtoupper(trim($this->input->post("fiscal")));
         $domreal=strtoupper(trim($this->input->post("real")));
         $domlocal=strtoupper(trim($this->input->post("local")));
         $correo=trim($this->input->post("email"));
         $contacto=trim($this->input->post("contacto"));
         $convenio=trim($this->input->post("convmt"));
         $postal=trim($this->input->post("cp"));
         $estado=$this->input->post("estad");
         $provactual=$this->Catalogo_model->getProveedorxID($id);
         if($codigo==$provactual->nroProv)
         {$unique='';}
         else{$unique='|is_unique[proveedor.nroProv]';}
         /**Validacion del Formulario */
         $this->form_validation->set_rules('nroProv',' Numero','required'.$unique);
         if($this->form_validation->run())
         {
            $data = array(
                'nroProv' =>$codigo ,
                'cuit'=>$cuit,
                'razonsocial'=>$razonsoc,
                'razonfantasia'=>$fantasiasoc,
                'domiciliofiscal'=>$domfiscal,
                'domicilioreal'=>$domreal,
                'localidad'=>$domlocal,
                'email'=>$correo,
                'tel'=>$contacto,
                'conv_multi'=>$convenio,
                'cp'=>$postal,
                'estado'=>$estado,
               );
             $res=$this->Catalogo_model->updateProveedor($data,$id);
             if($res)
             {
                 $this->session->set_flashdata('correcto','Proveedor Modificado Correctamente');
                 redirect(base_url().'mantenimiento/catalogo/proveedores');
 
             }
             else
             {
                 $this->session->set_flashdata('error','No se modifico el proveedor');
                 redirect(base_url().'mantenimiento/catalogo/editProveedor/'.$id);
 
             }
 
         }  
         else
         {
             $this->session->set_flashdata('error','No se pudo grabar el Proveedor');
             $this->addProveedor();
 
         }
      }
      /**Formulario para Crud de actividades de proveedores queda en standby hasta redefinir si la actividad se toma de la tabla rubro y luego se le agrega el coodigo de atp */
     public function actProveedor()
     {
         $data = array('lista' =>$this->Catalogo_model->getUndMedida() , );
         $this->load->view("layouts/header");
         $this->load->view("layouts/aside");
         $this->load->view("admin/catalogo/listActProv",$data);
         $this->load->view("layouts/footer");
     }
      /**Formulario de Alta de Actividades del Proveedor */
      public function addActProveedor()
      {
         $this->load->view("layouts/header");
         $this->load->view("layouts/aside");
         $this->load->view("admin/catalogo/addUndMedida");
         $this->load->view("layouts/footer");
 
      }
      /**Create una nueva actividad de proveedor siempre y cuando no exista */
      public function storeactproveedor()
      {
          $codigo=strtoupper(trim($this->input->post("codund")));
          $descripcion=strtoupper(trim($this->input->post("detcod")));
          $valor=$this->input->post("valor");
          /**Validacion del Formulario */
          $this->form_validation->set_rules('codund',' Codigo','required|is_unique[unidadmedida.cod]');
          if($this->form_validation->run())
          {
              $data = array(
                  'cod' =>$codigo ,
                  'descripcion'=>$descripcion,
                  'valor'=>$valor,
                 );
              $res=$this->Catalogo_model->addUndMedida($data);
              if($res)
              {
                  $this->session->set_flashdata('correcto','Unidad de Medida Agregada Correctamente');
                  redirect(base_url().'mantenimiento/catalogo/unidadesMedida');
 
              }
              else
              {
                  $this->session->set_flashdata('error','No se agrego la Unidad de Medida');
                  redirect(base_url().'mantenimiento/catalogo/addUndMedida');
 
              }
 
          }  
          else
          {
              $this->session->set_flashdata('error','No se pudo grabar la Unidad de Medida');
              $this->addUndMedida();
 
          }
 
      }
      /**Formulario para la edicion de una actividad de proveedor */
      public function editActProveedor($id)
      {
         $data=array(
             'lista'=>$this->Catalogo_model->getUndMedidaxID($id),
         );
         $this->load->view("layouts/header");
         $this->load->view("layouts/aside");
         $this->load->view("admin/catalogo/editUndMedida",$data);
         $this->load->view("layouts/footer");
      }
      /**Actualiza los datos de la actividad */
      public function edstoreactproveedor()
      {
         $id=$this->input->post("idr");
         $codigo=strtoupper(trim($this->input->post("codund")));
         $descripcion=strtoupper(trim($this->input->post("detcod")));
         $valor=$this->input->post("valor");
         $estado=$this->input->post("estad");
         $codactual=$this->Catalogo_model->getUndMedidaxID($id);
         if($codigo==$codactual->cod)
         {$unique='';}
         else{$unique='|is_unique[unidadmedida.cod]';}
         /**Validacion del Formulario */
         $this->form_validation->set_rules('codund',' Codigo','required'.$unique);
         if($this->form_validation->run())
         {
             $data = array(
                 'cod' =>$codigo ,
                 'descripcion'=>$descripcion,
                 'estado'=>$estado,
                 'valor'=>$valor,
                );
             $res=$this->Catalogo_model->updateUndMedida($data,$id);
             if($res)
             {
                 $this->session->set_flashdata('correcto','Unidad de Medida Modificada Correctamente');
                 redirect(base_url().'mantenimiento/catalogo/unidadesmedida');
 
             }
             else
             {
                 $this->session->set_flashdata('error','No se modifico la Unidad de Medida');
                 redirect(base_url().'mantenimiento/catalogo/editUndMedida/'.$id);
 
             }
 
         }  
         else
         {
             $this->session->set_flashdata('error','No se pudo grabar la Unidad de Medida');
             $this->addUndMedida();
 
         }
      }
      /**Listado de articulos para realizar CRUD */
      public function catalogo()
      {
        //estructura se ocupa para el alta de un articulo  
        //$estructura=$this->Catalogo_model->getEstructura();
          $articulos=$this->Catalogo_model->getArticulos();
          $data = array(
              'lista' =>$articulos ,
             );
        
          $this->load->view("layouts/header");
          $this->load->view("layouts/aside");
          $this->load->view("admin/catalogo/listCatalogo",$data);
          $this->load->view("layouts/footer");

      }
      /**Lista el detalle de un articulo */
      public function view(){
        $id=$this->input->post("id");
		$data=array(
			'lista'=>$this->Catalogo_model->getArticuloxID($id),
            'codigos'=>$this->Catalogo_model->getEANxID($id),
            'inventario'=>$this->Catalogo_model->getInventarioxID($id),
		);
       //var_dump($data);
	   $this->load->view("admin/catalogo/view",$data);
    }
    /**Generar un nuevo articulo */
    public function addCatalogo()
    {
        //$estructura=$this->Catalogo_model->getEstructura();
        $rubro=$this->Catalogo_model->getRubro();
        $subrubro=$this->Catalogo_model->getSubRubro();
        $undmedida=$this->Catalogo_model->getUndMedida();
        $data=array(
            'rubro'=>$rubro,
            'subrubro'=>$subrubro,
            'undmedida'=>$undmedida,
        );
        //$data = array('rubros' =>$estructura , );
        $this->load->view("layouts/header");
        $this->load->view("layouts/aside");
        $this->load->view("admin/catalogo/addCatalogo",$data);
        $this->load->view("layouts/footer");

    }
    /**Create un Proveedor siempre y cuando no exista(Numero) */
    public function storecatalogo()
    {
        $rubro=$this->input->post("rubro");
        $srubro=$this->input->post("subrubro");
        /*solucion para salir del paso*/
        $idestructura=$this->Catalogo_model->getIDRubro($rubro,$srubro);
        /** */
        $descripcion=strtoupper(trim($this->input->post("descart")));
        $vto=$this->input->post("fecvto");
        $inventario=strtoupper($this->input->post("inventario"));
        $undprincipal=$this->input->post("undmedida");
        $minimo=$this->input->post("minval");
        $maximo=$this->input->post("maxval");
        $costo=$this->input->post("pvval");
        $data = array(
                'descripcion' =>$descripcion ,
                'fechaalta'=>date("Y-m-d"),
                'fechavto'=>$vto,
                'inventariable'=>$inventario,
                'minimo'=>$minimo,
                'maximo'=>$maximo,
                'precio'=>$costo,
                'subrubros_id'=>$idestructura->id,
                'rubro_id'=>$idestructura->idrubro ,
                'unidadmedida_id'=>$undprincipal,
               );
        $res=$this->Catalogo_model->addArticulo($data);
        if($res)
        {
            /**hay que agregar el empaque por defecto a la tabla unidadxarticulo */
            $this->session->set_flashdata('correcto','Articulo Agregado Correctamente');
            redirect(base_url().'mantenimiento/catalogo/catalogo');
        }
        else
        {
            $this->session->set_flashdata('error','No se agrego el Proveedor');
            redirect(base_url().'mantenimiento/catalogo/addCatalogo');
        }
        

    }
    /**aca vendria el edit del articulo, despues lo hacemos */
    /**Formulario para agregar unidades de empaques al articulo */
    public function addEmpaque()
    {
        $undmedida=$this->Catalogo_model->getUndMedida();
        $articulos=$this->Catalogo_model->getArticulos();
        $data = array(
            'articulo' =>$articulos ,
            'medida'=>$undmedida,
         );
        $this->load->view("layouts/header");
        $this->load->view("layouts/aside");
        $this->load->view("admin/catalogo/addEmpaque",$data);
        $this->load->view("layouts/footer");


    }
    /**graba las unidades de empaque */
    public function storeunempaque()
    {
        $art=$this->input->post("art");
        $undmed=$this->input->post("emp");
        $data = array(
            'unidadmedida_id' =>$undmed ,
            'articulos_id'=>$art
         );
        $verifico=$this->Catalogo_model->getVerifico($undmed,$art);
        if($verifico)
        {
            $this->session->set_flashdata("error","ARTICULO YA TIENE ESA UNIDAD DE EMPAQUE");
            redirect(base_url()."mantenimiento/catalogo/addEmpaque");

        }
        else
        {
            if($this->Catalogo_model->saveEmpaque($data))
            {
                echo "<script>alert('Empaque Agregado Correctamente');</script>";
                $ruta=base_url().'/mantenimiento/catalogo/catalogo';
                echo "<script>window.location='".$ruta."';</script>";
            }
            else
            {
                $this->session->set_flashdata("error","No se pudo grabar-Reintente");
                redirect(base_url()."mantenimiento/catalogo/addEmpaque");
            }

        }

    }
    /**Formulario para agregar EAN al articulo */
    public function addEAN()
    {
        $proveedores=$this->Catalogo_model->getProveedores();
        $articulos=$this->Catalogo_model->getArticulos();
        $data = array(
            'articulo' =>$articulos ,
            'proveedor'=>$proveedores,
         );
        $this->load->view("layouts/header");
        $this->load->view("layouts/aside");
        $this->load->view("admin/catalogo/addEAN",$data);
        $this->load->view("layouts/footer");


    }
    /**graba los EAN de un articulo */
    public function storeEAN()
    {
        $proveedor=$this->input->post("prov");
        $catalogo=$this->input->post("art");
        $eanart=trim($this->input->post("eanart"));
        $data = array(
            'ean' =>$eanart ,
            'articulos_id'=>$catalogo,
            'proveedores_id'=>$proveedor,
         );
        //$verifico=$this->Catalogo_model->getVerifico($undmed,$art);
        //if($verifico)
        //{
        //    $this->session->set_flashdata("error","ARTICULO YA TIENE ESA UNIDAD DE EMPAQUE");
        //    redirect(base_url()."mantenimiento/catalogo/addEmpaque");

       // }
       // else
       // {
            if($this->Catalogo_model->saveEAN($data))
            {
                echo "<script>alert('EAN Agregado Correctamente');</script>";
                $ruta=base_url().'/mantenimiento/catalogo/catalogo';
                echo "<script>window.location='".$ruta."';</script>";
            }
            else
            {
                $this->session->set_flashdata("error","No se pudo grabar-Reintente");
                redirect(base_url()."mantenimiento/catalogo/addEAN");
            }

       // }

    }
    /**Formulario para agregar EAN al articulo */
    public function addInventario()
    {
        $proveedores=$this->Catalogo_model->getProveedores();
        $articulos=$this->Catalogo_model->getArticulosInv();
        $data = array(
            'articulo' =>$articulos ,
            'proveedor'=>$proveedores,
         );
        $this->load->view("layouts/header");
        $this->load->view("layouts/aside");
        $this->load->view("admin/catalogo/addInventario",$data);
        $this->load->view("layouts/footer");


    }
    /**graba los EAN de un articulo */
    public function storeInventario()
    {
        $catalogo=$this->input->post("art");
        $inventario=strtoupper(trim($this->input->post("codinv")));
        $oficina=strtoupper(trim($this->input->post("ofinv")));
        $observa=strtoupper(trim($this->input->post("obseinv")));
        $data = array(
            'nroinventario' =>$inventario ,
            'fecha_alta'=>date("Y-m-d"),
            'oficina'=>$oficina,
            'observa'=>$observa,
            'catalogos_id'=>$catalogo
         );
        //$verifico=$this->Catalogo_model->getVerifico($undmed,$art);
        //if($verifico)
        //{
        //    $this->session->set_flashdata("error","ARTICULO YA TIENE ESA UNIDAD DE EMPAQUE");
        //    redirect(base_url()."mantenimiento/catalogo/addEmpaque");

       // }
       // else
       // {
            if($this->Catalogo_model->saveInventario($data))
            {
                echo "<script>alert('Inventario Agregado Correctamente');</script>";
                $ruta=base_url().'/mantenimiento/catalogo/catalogo';
                echo "<script>window.location='".$ruta."';</script>";
            }
            else
            {
                $this->session->set_flashdata("error","No se pudo grabar-Reintente");
                redirect(base_url()."mantenimiento/catalogo/addInventario");
            }

       // }

    }
 

}