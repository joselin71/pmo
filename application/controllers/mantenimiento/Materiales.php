<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Materiales extends CI_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->model('Materiales_model');
        $this->load->library('image_lib');
  
    }
    public function index()
    {
        $secretaria=$this->session->userdata("unidad");
        $rol=$this->session->userdata("rol");
        $data=array(
            'pedidos'=>$this->Materiales_model->getPedidos($secretaria,$rol),

        );
        $this->load->view("layouts/header");
        $this->load->view("layouts/aside");
        $this->load->view("admin/materiales/list",$data);
        $this->load->view("layouts/footer");
    }
    /**Generar un nuevo Pedido */
    public function add()
    {
        $secretaria=$this->session->userdata("unidad");
        $dniuser=$this->session->userdata("usuario");
        $dependencia=$this->Materiales_model->getDependencias($secretaria);
        $programa=$this->Materiales_model->getProgramas($secretaria);
        $pedido=$this->Materiales_model->getPedidoDetalle($secretaria,$dniuser);
        $data=array(
            'dependencia'=>$dependencia,
            'secretaria'=>$this->session->userdata("unidad") ,
            'secretariaN'=>$this->session->userdata("unidadN") ,
            'programa'=>$programa,
            'pedido'=>$pedido,
        );
        $this->load->view("layouts/header");
		$this->load->view("layouts/aside");
		$this->load->view("admin/materiales/add",$data);
		$this->load->view("layouts/footer");
    }
    /**Formulario para agregar un Articulo individual */
    public function adddetalle()
    {
        $rubro=$this->Materiales_model->getRubros();
        $subrubro=$this->Materiales_model->getSubRubros();
        $data=array(
            'rubro'=>$rubro,
            'subrubro'=>$subrubro,
        );
        $this->load->view("layouts/header");
        $this->load->view("layouts/aside");
        $this->load->view("admin/materiales/adddetalle",$data);
        $this->load->view("layouts/footer");
    }
    /**Graba un articulo individual y vuelve al formulario de carga de cabecera */
    public function store()
    {
        $secretaria=$this->session->userdata("unidad");
        $dniuser=$this->session->userdata("usuario");
        $fechacarga=date("Y-m-d");
        $cantidad=$this->input->post("montof");
        $importeu=$this->input->post("montop");
        $bienes=strtoupper($this->input->post("bienes"));
        $rubro=$this->input->post("rubro");
        $subr=$this->input->post("subrubro");
        $importet=$this->input->post("totalf");
        $aniopedido=date("Y");
        $idaleatorio=0;$renglon=0;$idpedido=0;
     	$data=array(
		 'cantidad'=>$cantidad,
		 'importedetalle'=>$importet,
		 'idrubro'=>$rubro,
		 'idsubr'=>$subr,
         'detallepedido'=>$bienes,
         'idpedido'=>$idpedido,
         'idsol'=>$secretaria,
         'aniopedido'=>$aniopedido,
         'importelinea'=>$importeu,
         'idaleatorio'=>$idaleatorio,
         'renglon'=>$renglon,
         'dniuser'=>$dniuser
        );
        if($this->Materiales_model->save($data))
        {
            echo "<script>alert('Articulo Grabado Correctamente');</script>";
            $ruta=base_url().'/mantenimiento/materiales/add';
            echo "<script>window.location='".$ruta."';</script>";
        }else
        {
            $this->session->set_flashdata("error","No se pudo grabar-Reintente");
            redirect(base_url()."mantenimiento/materiales/adddetalle");
        }
    }
     /**Edicion de un articulo sin cabecera */
     public function edit($id)
     {
         $rubro=$this->Materiales_model->getRubros();
         $subrubro=$this->Materiales_model->getSubRubros();
         $lista=$this->Materiales_model->getLinea($id);
         $idped=$lista->idaleatorio;
         $data= array(
             'rubro'=>$rubro,
             'subrubro'=>$subrubro,
             'lista' =>$lista,
             'id'=>$idped
          );
          $this->load->view("layouts/header");
          $this->load->view("layouts/aside");
          $this->load->view("admin/materiales/viewEdPed",$data);
          $this->load->view("layouts/footer");
 
     }
     /** Modifica un renglon de un pedido sin cabecera*/
     public function edstore()
    {
        $id=$this->input->post("idCod");
        $cantidad=$this->input->post("montof");
        $importeu=$this->input->post("montop");
        $bienes=strtoupper($this->input->post("bienes"));
        $rubro=$this->input->post("rubro");
        $subr=$this->input->post("subrubro");
        $importet=$this->input->post("totalf");
        $data=array(
		 'cantidad'=>$cantidad,
		 'importedetalle'=>$importet,
		 'idrubro'=>$rubro,
		 'idsubr'=>$subr,
         'detallepedido'=>$bienes,
         'importelinea'=>$importeu,
        );
        if($this->Materiales_model->edsave($data,$id))
        {
            echo "<script>alert('Articulo Modificado Correctamente');</script>";
            $ruta=base_url().'/mantenimiento/materiales/add';
            echo "<script>window.location='".$ruta."';</script>";
        }else
        {
            $this->session->set_flashdata("error","No se pudo grabar-Reintente");
            redirect(base_url()."mantenimiento/materiales/add");
        }
    }
     /**Elimina un articulo sin cabecera */
     public function delete($id)
     {
         if($this->Materiales_model->deleteArt($id))
         {
             echo "<script>alert('Articulo Eliminado Correctamente');</script>";
             $ruta=base_url().'/mantenimiento/materiales/add';
             echo "<script>window.location='".$ruta."';</script>";
 
         }
         else
         {
             $this->session->set_flashdata("error","No se pudo Eliminar-Reintente");
             redirect(base_url()."mantenimiento/materiales/add");
 
         }
     }
     /**Graba la cabecera del pedido y actualiza el detalle */
     public function storecab()
     {
         $secretaria=$this->session->userdata("unidad");
         $dniuser=$this->session->userdata("usuario");
         $totalletra=$this->input->post("totalletra");
         $cta=$this->input->post("cuenta");
         if($cta==1){$cuenta="CAJA CHICA";}
         if($cta==2){$cuenta="CUENTA GENERAL";}
         if($cta==3){$cuenta="FDO.REPARACION MANTENIMIENTO Y CONTROL CALLES";}
         if($cta==4){$cuenta="FDOS.DE TERCEROS Y OTROS EN GARANTIA";}
         if($cta==5){$cuenta="FDOS.REPAROS EN GENERAL";}
         if($cta==6){$cuenta="GAS.IND.UTIL.OF.PAR.MOTOC";}
         if($cta==7){$cuenta="FDO.ESP.OBRAS INFRAESTRUCTURA";}
         if($cta==8){$cuenta="LOTERIA CHAQUEÑA";}
         if($cta==9){$cuenta="CUENTA ESPECIAL APORTE FINANCI";}
         if($cta==10){$cuenta="PRESIDENCIA CONSEJO";}
         if($cta==11){$cuenta="PRODISM";}
         if($cta==12){$cuenta="PROGRAMA COMEDORES INFANTILES";}
         if($cta==13){$cuenta="FONDO FEDERAL SOLIDARIO";}
         if($cta==14){$cuenta="CTA.ESP.SUBASTA PUBLICA";}
         if($cta==15){$cuenta="FONDO DE DESARROLLO LOCAL";}
         if($cta==16){$cuenta="OTROS";}
         $tipotramite=$this->input->post("tipotramite");
         $tipopedido=$this->input->post("tipopedido");
         $idsecre=$this->input->post("secret");
         $idssecre=$this->input->post("subsecret");
         $iddg=$this->input->post("dirgral");
         $destmat=$this->input->post("destmat");
         $fechavisado="1900-01-01";$fechaingresado="1900-01-01";
         $fechaautorizado="1900-01-01";
         $resultado=$this->Materiales_model->getNpedido();
         $aniopedido=date("Y");$fechapedido=date("Y-m-d");
         $nropedido=0;
         if($aniopedido > $resultado->anioped)
         {
             $nropedido=1;
         }
         else{
             $nropedido=($resultado->nroped) + 1;
         }
         $estado="CARGADO";
         $actuacion="";
         $programa=$this->input->post("prg");
         $subprograma=$this->input->post("sbprg");
         $cobertura=$this->input->post("cobertura");
         $totalped=$this->Materiales_model->getbuscarTP($dniuser,$secretaria,$aniopedido);
         $data=array(
          'nropedido'=>$nropedido,
          'aniopedido'=>$aniopedido,
          'fechapedido'=>$fechapedido,
          'estado'=>$estado,
          'totalped'=>$totalped->total,
          'totalletra'=>$totalletra,
          'idsolicitante'=>$dniuser,
          'idpedido'=>$nropedido,
          'isecre'=>$idsecre,
          'isubsecre'=>$idssecre,
          'idg'=>$iddg,
          'destinomat'=>$destmat,
          'cuenta'=>$cuenta,
          'actuacion'=>$actuacion,
          'fechavisado'=>$fechavisado,
          'fechaingresado'=>$fechaingresado,
          'fechaautorizado'=>$fechaautorizado,
          'tipotramite'=>$tipotramite,
          'tipopedido'=>$tipopedido,
          'idprg'=>$programa,
          'idsbprg'=>$subprograma,
          'cobertura'=>$cobertura
         );
         if($this->Materiales_model->saveCab($data))
         {
             $ultimo=$this->Materiales_model->getLastID($dniuser);
             $dataSecuencia=array(
                 'nroped'=>$nropedido,
                 'anioped'=>$aniopedido
             );
             $actualizarSecuencia=$this->Materiales_model->actualizarSecuencia($dataSecuencia);
             $dataCompra=array(
                 'aleatorio'=>$ultimo->idpedidomateriales,
                 'secretaria'=>$secretaria,
                 'aniop'=>$aniopedido,
                 'nrop'=>$nropedido,
                 'estimado'=>$totalped->total,
                 'pedmat'=>0,
                 'aniooc'=>0,
                 'nrooc'=>0,
                 'asignado'=>0,
                 'fecoc'=>"1900-01-01",
                 'proveedor'=>"",
                 'actuacions'=>"",
                 'fecas'=>"1900-01-01",
                 'nropv'=>0
             );
             $actualizarCompras=$this->Materiales_model->actualizarCompras($dataCompra);
             $dataAleatorio=array(
                 'idpedido'=>$nropedido,
                 'idaleatorio'=>$ultimo->idpedidomateriales
             );
             $actualizarAleatorio=$this->Materiales_model->actualizarAleatorio($aniopedido,$idsecre,$dniuser,$dataAleatorio);
 
             echo "<script>alert('PEDIDO Grabado Correctamente');</script>";
             $ruta=base_url().'/mantenimiento/materiales';
             echo "<script>window.location='".$ruta."';</script>";
         }else
         {
             $this->session->set_flashdata("error","No se pudo grabar-Reintente");
             redirect(base_url()."mantenimiento/materiales/add");
         }
     }
     /**Impresion de un Pedido grabado */
     public function printPed($id)
    {
         $res=$this->Materiales_model->getPedidoCabecera($id);
         $res2=$this->Materiales_model->getPedidoPrograma($id);
         $res3=$this->Materiales_model->getPedidoDetallado($id);
         $secretaria=$res->detsec;$ssecretaria=$res->detsubsec;$cuenta=$res->cuenta;$destino=$res->destinomat;
         $nropedido=$res->nropedido;$aniopedido=$res->aniopedido;
         $cobertura=$res->cobertura;$totalped=$res->totalped;
         $totalletra=$res->totalletra;$apynom=$res->apynom;
         $idaleatorio=$res->idpedidomateriales;
         $programa=$res2->descripcion;$sprograma=$res2->descripcionsp;
         $rutalogo1=base_url()."assets/template/img/FVhead.jpg";
         
         ///////////prueba de codigo de barra
         $this->load->library('ciqrcode');  
         header("Content-Type: image/png");         
         $params['data'] ="Sec:".$secretaria."-Id:".$id."-Ped:".$nropedido."-An:".$aniopedido;
         $params['level'] = 'H';
         $params['size'] =4; 
         //decimos el directorio a guardar el codigo qr, en este 
         //caso una carpeta en la raíz llamada qr_code
         //$params['savename'] = FCPATH."uploads/qr_code/".$id.".png";
         //Prueba para nginx
         $params['savename'] =base_url()."uploads/qr_code/".$id.".png";
         //generamos el código qr
         $ff=$this->ciqrcode->generate($params);
         //require_once APPPATH.'third_party/fpdf/fpdf.php';
         $this->load->library('fpdf_manager');
         $pdf = new fpdf_manager();
         //$pdf = new PDF();//hoja vertical
         //$pdf=new PDF('L','mm','A4');//hoja horizontal
         $pdf->AliasNbPages();
         $pdf->AddPage();$pdf->Ln(5);$pdf->SetFont('Arial','B',12);
         ////////////////////////////////////////////////////////
         //$pdf->Image($ff,10,5,20,20);
         $pdf->Cell(190,10,'SOLICITUD DE PEDIDO DE MATERIALES',0,'T','C');
         $pdf->SetFont('Arial','B',9);$pdf->Ln(10);
         $pdf->Cell(100,7,'INTERVENCION DE COMPRAS',1,'T','C');
         $pdf->Cell(25,7,'................',1,'B','C');$pdf->Cell(20,7,'NUMERO',1,'B','C');
         $pdf->Cell(25,7,'......./........./.........',1,'T','C');$pdf->Cell(20,7,'FECHA',1,'T','C'); $pdf->Ln(7);
         $pdf->SetFont('Arial','B',9);
         $pdf->Cell(30,7,'Oficina Solicitante ',1,'T','L');
         $pdf->Cell(40,7,'Secretaria ',1,'T','C');
         $pdf->Cell(120,7,utf8_decode($secretaria),1,'T','C');$pdf->Ln(7);
         $pdf->Cell(30,7,'Sub-Secretaria ',1,'T','C');
         $pdf->Cell(160,7,utf8_decode($ssecretaria),1,'T','C'); //$pdf->Ln(7);
         $pdf->Ln(7);$pdf->SetFont('Arial','B',9);
         $pdf->Cell(30,5,'Cuenta Destino',1,'T','C');$pdf->SetFont('Arial','',8);
         $pdf->Cell(160,5,utf8_decode($cuenta),1,'T','C');
         $pdf->Ln(5);
         $pdf->SetFont('Arial','B',9);
         $pdf->Cell(190,7,'Destino Material/Servicio:','LR','T','L');$pdf->Ln(7);$pdf->SetFont('Arial','',7);
         $pdf->MultiCell(190,6,utf8_decode($destino),'LR','L');
         $pdf->SetFont('Arial','B',6);
         $pdf->Cell(20,4,'Programa ',1,'T','C');
         $pdf->Cell(170,4,utf8_decode($programa),1,'T','L'); 
         $pdf->Ln(4);
         $pdf->Cell(20,4,'Sub-Programa ',1,'T','C');
         $pdf->Cell(170,4,utf8_decode($sprograma),1,'T','L');
         $pdf->Ln(4);
         $pdf->Cell(18,5,'Partida Presup.',1,'T','C');
         $pdf->Cell(5,5,'NRO.',1,'T','C');
         $pdf->Cell(10,5,'CANT.',1,'T','C');
         $pdf->Cell(110,5,'DESCRIPCION DE BIENES/SERVICIOS ',1,'T','C');
         $pdf->Cell(23,5,'PV-UNIT.$ ',1,'T','C');
         $pdf->Cell(24,5,'TOTAL $ ',1,'T','C');
         $pdf->Ln(5);
         $conteo=1;$tmontof=0;$conteo2=1;
         $pdf->SetFont('Arial','',6);
         foreach($res3 as $det)
         {
              $longitud =strlen($det->detallepedido); 
              $items = round($longitud / 85) + 1;
              $linf = 0; $lsup = 85;
              if($longitud > 85)
              { 
                  $pdf->Cell(18,5,"",0,'L','R');
                  $pdf->Cell(5,5,number_format($conteo2,0,",","."),0,'L','R');
                  $pdf->Cell(10,5,number_format($det->cantidad,2,",","."),0,'T','C');
                  for($i=0; $i<$items; $i++){
                      $cadena[$i] = substr($det->detallepedido, $linf, $lsup);
                      $linf += 85;
                      $cadena2=implode($cadena);$cadena[$i]="";
                      $pdf->Cell(110,5,utf8_decode($cadena2),0,'T','L');
                      if($i==0){
                          $pdf->Cell(22,5,chr(36).' '.number_format($det->importelinea,2,",","."),0,'R','R');
                          $pdf->Cell(22,5,chr(36).' '.number_format($det->importedetalle,2,",","."),0,'R','R');
                          }else{
                              $pdf->Cell(22,5,"",0,'R','R');
                              $pdf->Cell(22,5,"",0,'R','R');
                              }
                          $pdf->Ln(4);
                          $pdf->Cell(18,5,"",0,'L','R');
                          $pdf->Cell(5,5,"",0,'L','R');
                          $pdf->Cell(10,5,"",0,'T','C');
                          $conteo++;
                    }
                }
                else
                {
                    $pdf->Cell(18,5,"",0,'L','R');
                    $pdf->Cell(5,5,number_format($conteo2,0,",","."),0,'L','R');
                    $pdf->Cell(10,5,number_format($det->cantidad,2,",","."),0,'T','C');
                    $pdf->Cell(110,5,utf8_decode($det->detallepedido),0,'T','L');
                    $pdf->Cell(22,5,chr(36).' '.number_format($det->importelinea,2,",","."),0,'R','R');
                    $pdf->Cell(22,5,chr(36).' '.number_format($det->importedetalle,2,",","."),0,'R','R');
                    $conteo++;
                }
                $conteo2++;$pdf->Ln(4); $tmontof=$tmontof+$det->importedetalle;
                if($conteo>25)
                {
                    $pdf->Cell(10,5,'Total:',0,'T','C');
                    $pdf->Cell(145,5,utf8_decode($totalletra),0,'T','L');
                    $pdf->Cell(35,5,chr(36).' '.number_format($tmontof,2,",","."),0,'T','C');
                    $pdf->Ln(4);
                    $pdf->Cell(35,5,'Pedido de Secretaria Nro:',0,'T','L');
                    $pdf->Cell(10,5,$nropedido,0,'T','L');
                    $pdf->Cell(5,5,'-',0,'T','C');
                    $pdf->Cell(10,5,$aniopedido,0,'T','C');
                    $pdf->Cell(10,5,'Operador:',0,'T','R');
                    $pdf->Cell(40,5,$apynom,0,'T','R');
                    $pdf->Ln(4);
                    $pdf->Cell(30,5,'Cobertura de Pedido en meses:',0,'T','L');
                    $pdf->Cell(15,5,$cobertura,0,'T','R');
                    $pdf->Cell(40,5,'Id-Aleatorio:',0,'T','C');$pdf->Cell(10,5,$idaleatorio,0,'T','L');
                    $pdf->Ln(5);
                    $pdf->Cell(190,5,'AUTORIZACIONES',1,'T','C');$pdf->Ln(5);
                    $pdf->Cell(70,5,'DIRECCION GRAL.',1,'T','C');
                    $pdf->Cell(60,5,'SUBSECRETARIA',1,'T','C');
                    $pdf->Cell(60,5,'SECRETARIA',1,'T','C');
                    $pdf->Ln(5);
                    $pdf->Cell(70,5,'',1,'T','C');
                    $pdf->Cell(60,5,'',1,'T','C');
                    $pdf->Cell(60,5,'',1,'T','C');
                    $pdf->Ln(5);
                    $pdf->Cell(190,5,'INTERVENCION DIRECCION GRAL DE ADMINISTRACION-CORRESPONDE',1,'T','C');$pdf->Ln(5);
                    $pdf->Cell(40,5,'COMPRA DIRECTA',1,'T','C');$pdf->Cell(30,5,'',1,'T','C');
                    $pdf->Cell(40,5,'CONCURSO PRECIO',1,'T','C');$pdf->Cell(20,5,'',1,'T','C');
                    $pdf->Cell(40,5,'LICITACION PRIVADA',1,'T','C');$pdf->Cell(20,5,'',1,'T','C');
                    $pdf->Ln(5);
                    $pdf->Cell(100,5,'AUTORIZACION SECRETARIA DE ECONOMIA',1,'T','C');
                    $pdf->Cell(90,5,'',1,'T','C');
                    /////////////////
                    $pdf->AddPage();
                    $pdf->SetFont('Arial','B',6);
                    $pdf->Cell(18,5,'Partida Presup.',1,'T','C');
                    $pdf->Cell(5,5,'NRO.',1,'T','C');
                    $pdf->Cell(10,5,'CANT.',1,'T','C');
                    $pdf->Cell(110,5,'DESCRIPCION DE BIENES/SERVICIOS ',1,'T','C');
                    $pdf->Cell(23,5,'PV-UNIT.$ ',1,'T','C');
                    $pdf->Cell(24,5,'TOTAL $ ',1,'T','C');
                    $pdf->Ln(5);
                    $conteo=1;$pdf->SetFont('Arial','',6);
                }

         }	//foreach/////////////
         if($conteo<25)
         {
             for($i=$conteo;$i<25;$i++)
             {
                 $pdf->Cell(10,5,'',0,'T','R');
                 $pdf->Cell(25,5,'',0,'T','C');
                 $pdf->Cell(110,5,'',0,'T','L');
                 $pdf->Cell(35,5,'',0,'T','R');
                 $pdf->Ln(5);
            }
        }//////////////
        $pdf->Cell(10,5,'Total:',0,'T','C');
        $pdf->Cell(145,5,utf8_decode($totalletra),0,'T','L');
        $pdf->Cell(35,5,chr(36).' '.number_format($tmontof,2,",","."),0,'T','C');$pdf->Ln(5);
        $pdf->Cell(35,5,'Pedido de Secretaria Nro:',0,'T','L');
        $pdf->Cell(10,5,$nropedido,0,'T','L');$pdf->Cell(5,5,'-',0,'T','C');
        $pdf->Cell(10,5,$aniopedido,0,'T','C');
        $pdf->Cell(10,5,'Operador:',0,'T','R');
        $pdf->Cell(40,5,utf8_decode($apynom),0,'T','R');
        $pdf->Ln(4);
        $pdf->Cell(30,5,'Cobertura de Pedido en meses:',0,'T','L');
        $pdf->Cell(15,5,$cobertura,0,'T','R');
        $pdf->Cell(40,5,'Id-Aleatorio:',0,'T','C');$pdf->Cell(10,5,$idaleatorio,0,'T','L');$pdf->Ln(5);
        $pdf->Cell(190,5,'AUTORIZACIONES',1,'T','C');$pdf->Ln(5);
        $pdf->Cell(70,5,'DIRECCION GRAL.',1,'T','C');
        $pdf->Cell(60,5,'SUBSECRETARIA',1,'T','C');
        $pdf->Cell(60,5,'SECRETARIA',1,'T','C');
        $pdf->Ln(5);
        $pdf->Cell(70,8,'',1,'T','C');
        $pdf->Cell(60,8,'',1,'T','C');
        $pdf->Cell(60,8,'',1,'T','C');
        $pdf->Ln(8);
        $pdf->Cell(190,5,'INTERVENCION DIRECCION GRAL DE ADMINISTRACION-CORRESPONDE',1,'T','C');$pdf->Ln(5);
        $pdf->Cell(40,5,'COMPRA DIRECTA',1,'T','C');$pdf->Cell(30,5,'',1,'T','C');
        $pdf->Cell(40,5,'CONCURSO PRECIO',1,'T','C');$pdf->Cell(20,5,'',1,'T','C');
        $pdf->Cell(40,5,'LICITACION PRIVADA',1,'T','C');$pdf->Cell(20,5,'',1,'T','C');
        $pdf->Ln(5);
        $pdf->Cell(100,10,'AUTORIZACION SECRETARIA DE ECONOMIA',1,'T','C');
        $pdf->Cell(90,10,'',1,'T','C');$pdf->Ln(15);
        $pdf->Output('Formulario de Pedido de Materiales','I');
    }
     /**Edita un pedido ya grabado */
     public function editPed($id)
     {
         $secretaria=$this->session->userdata("unidad");
         $dniuser=$this->session->userdata("usuario");
         $dependencia=$this->Materiales_model->getDependencias($secretaria);
         $programa=$this->Materiales_model->getProgramas($secretaria);
         $lista=$this->Materiales_model->getPedidoxID($id);
         $nropedido=$lista[0]->nropedido;
         $aniopedido=$lista[0]->aniopedido;
          $data=array(
             'dependencia'=>$dependencia,
             'secretaria'=>$this->session->userdata("unidad") ,
             'secretariaN'=>$this->session->userdata("unidadN") ,
             'programa'=>$programa,
             'lista'=>$lista,
             'id'=>$id,
             'nropedido'=>$nropedido,
             'aniopedido'>=$aniopedido
         );     
         $this->load->view("layouts/header");
         $this->load->view("layouts/aside");
         $this->load->view("admin/materiales/addEdit",$data);
         $this->load->view("layouts/footer");
     }
     /**Obtiene los datos de un pedido grabado para modificar */
     public function adddetalleEd($id)
    {
        $rubro=$this->Materiales_model->getRubros();
        $subrubro=$this->Materiales_model->getSubRubros();
        $lista=$this->Materiales_model->getPedidoxID($id);
        $nroped=$lista[0]->nropedido;
        $anioped=$lista[0]->aniopedido;
        $data=array(
            'rubro'=>$rubro,
            'subrubro'=>$subrubro,
            'id'=>$id,
            'nroped'=>$nroped,
            'anioped'=>$anioped

        );
        $this->load->view("layouts/header");
        $this->load->view("layouts/aside");
        $this->load->view("admin/materiales/adddetalleEd",$data);
        $this->load->view("layouts/footer");

    }
    /**Agrega un articulo a un pedido ya grabado */
    public function storeEd($id)
    {
        $secretaria=$this->session->userdata("unidad");
        $dniuser=$this->session->userdata("usuario");
        $cantidad=$this->input->post("montof");
        $importeu=$this->input->post("montop");
        $bienes=strtoupper($this->input->post("bienes"));
        $rubro=$this->input->post("rubro");
        $subr=$this->input->post("subrubro");
        $importet=$this->input->post("totalf");
        $aniopedido=$this->input->post("anioped");
        $idpedido=$this->input->post("nroped");
        $idaleatorio=$id;$renglon=0;
     	$data=array(
		 'cantidad'=>$cantidad,
		 'importedetalle'=>$importet,
		 'idrubro'=>$rubro,
		 'idsubr'=>$subr,
         'detallepedido'=>$bienes,
         'idpedido'=>$idpedido,
         'idsol'=>$secretaria,
         'aniopedido'=>$aniopedido,
         'importelinea'=>$importeu,
         'idaleatorio'=>$idaleatorio,
         'renglon'=>$renglon,
         'dniuser'=>$dniuser
        );
        if($this->Materiales_model->save($data))
        {
            echo "<script>alert('Articulo Grabado Correctamente');</script>";
            $ruta=base_url().'/mantenimiento/materiales/editPed/'.$id;
            echo "<script>window.location='".$ruta."';</script>";
        }else
        {
            $this->session->set_flashdata("error","No se pudo grabar-Reintente");
            redirect(base_url()."mantenimiento/materiales/editPed/".$id);
        }
    }
    /**Formulario de Edicion de un articulo de un pedido ya grabado */
    public function editP($id)
    {
        $rubro=$this->Materiales_model->getRubros();
        $subrubro=$this->Materiales_model->getSubRubros();
        $lista=$this->Materiales_model->getLinea($id);
        $idped=$lista->idaleatorio;
        $data= array(
            'rubro'=>$rubro,
            'subrubro'=>$subrubro,
            'lista' =>$lista,
            'id'=>$idped
         );
         $this->load->view("layouts/header");
         $this->load->view("layouts/aside");
         $this->load->view("admin/materiales/viewEdPedP",$data);
         $this->load->view("layouts/footer");

    }
    /**Edita un articulo de un pedido ya grabado */
    public function edstoreP()
    {
        $id=$this->input->post("idCod");
        $idPed=$this->input->post("idPed");
        $cantidad=$this->input->post("montof");
        $importeu=$this->input->post("montop");
        $bienes=strtoupper($this->input->post("bienes"));
        $rubro=$this->input->post("rubro");
        $subr=$this->input->post("subrubro");
        $importet=$this->input->post("totalf");
        $data=array(
		 'cantidad'=>$cantidad,
		 'importedetalle'=>$importet,
		 'idrubro'=>$rubro,
		 'idsubr'=>$subr,
         'detallepedido'=>$bienes,
         'importelinea'=>$importeu,
        );
        if($this->Materiales_model->edsave($data,$id))
        {
            echo "<script>alert('Articulo Modificado Correctamente');</script>";
            $ruta=base_url().'/mantenimiento/materiales/editPed/'.$idPed;
            echo "<script>window.location='".$ruta."';</script>";
        }else
        {
            $this->session->set_flashdata("error","No se pudo grabar-Reintente");
            redirect(base_url()."mantenimiento/materiales/viewEdPed");
        }
    }
    /**Elimina un articulo de un pedido ya grabado */
    public function deleteP($id)
    {
        
        $lista=$this->Materiales_model->getLinea($id);
        $idPed=$lista->idaleatorio;
        if($this->Materiales_model->deleteArt($id))
        {
            echo "<script>alert('Articulo Eliminado Correctamente');</script>";
            $ruta=base_url().'/mantenimiento/materiales/editPed/'.$idPed;
            echo "<script>window.location='".$ruta."';</script>";

        }
        else
        {
            $this->session->set_flashdata("error","No se pudo Eliminar-Reintente");
            redirect(base_url()."mantenimiento/materiales/editPed/".$idPed);

        }
    }
    /**Graba la cabecera de un pedido grabado que se edita */
    public function storecabEd($id)
    {
        $secretaria=$this->session->userdata("unidad");
        $dniuser=$this->session->userdata("usuario");
        $totalletra=$this->input->post("totalletra");
        $cta=$this->input->post("cuenta");
        if($cta==1){$cuenta="CAJA CHICA";}
        if($cta==2){$cuenta="CUENTA GENERAL";}
        if($cta==3){$cuenta="FDO.REPARACION MANTENIMIENTO Y CONTROL CALLES";}
        if($cta==4){$cuenta="FDOS.DE TERCEROS Y OTROS EN GARANTIA";}
        if($cta==5){$cuenta="FDOS.REPAROS EN GENERAL";}
        if($cta==6){$cuenta="GAS.IND.UTIL.OF.PAR.MOTOC";}
        if($cta==7){$cuenta="FDO.ESP.OBRAS INFRAESTRUCTURA";}
        if($cta==8){$cuenta="LOTERIA CHAQUEÑA";}
        if($cta==9){$cuenta="CUENTA ESPECIAL APORTE FINANCI";}
        if($cta==10){$cuenta="PRESIDENCIA CONSEJO";}
        if($cta==11){$cuenta="PRODISM";}
        if($cta==12){$cuenta="PROGRAMA COMEDORES INFANTILES";}
        if($cta==13){$cuenta="FONDO FEDERAL SOLIDARIO";}
        if($cta==14){$cuenta="CTA.ESP.SUBASTA PUBLICA";}
        if($cta==15){$cuenta="FONDO DE DESARROLLO LOCAL";}
        if($cta==16){$cuenta="OTROS";}
        $tipotramite=$this->input->post("tipotramite");
        $tipopedido=$this->input->post("tipopedido");
        $idsecre=$this->input->post("secret");
        $idssecre=$this->input->post("subsecret");
        $iddg=$this->input->post("dirgral");
        $destmat=$this->input->post("destmat");
        $estado="CARGADO";
        $programa=$this->input->post("prg");
        $subprograma=$this->input->post("sbprg");
        $cobertura=$this->input->post("cobertura");
        
        $totalped=$this->Materiales_model->getbuscarTPEd($id);
        $data=array(
		 'estado'=>$estado,
         'totalped'=>$totalped->total,
         'totalletra'=>$totalletra,
         'isecre'=>$idsecre,
         'isubsecre'=>$idssecre,
         'idg'=>$iddg,
         'destinomat'=>$destmat,
         'cuenta'=>$cuenta,
         'tipotramite'=>$tipotramite,
         'tipopedido'=>$tipopedido,
         'idprg'=>$programa,
         'idsbprg'=>$subprograma,
         'cobertura'=>$cobertura
        );
        if($this->Materiales_model->saveCabEd($data,$id))
        {
            $dataCompra=array(
                'estimado'=>$totalped->total
            );
            $actualizarCompras=$this->Materiales_model->actualizarComprasEd($dataCompra,$id);
            echo "<script>alert('PEDIDO Modificado Correctamente');</script>";
            $ruta=base_url().'/mantenimiento/materiales';
            echo "<script>window.location='".$ruta."';</script>";
        }else
        {
            $this->session->set_flashdata("error","No se pudo grabar-Reintente");
            redirect(base_url()."mantenimiento/materiales");
        }
    }
    /**Formulario para autorizar un pedido segun el nivel */
    public function auth_Ped($id)
    { 
        $rol=$this->session->userdata("rol");
        $data=array(
			'lista'=>$this->Materiales_model->getPedidoAuthxID($id),
            'rol'=>$rol
        );
        $this->load->view("layouts/header");
        $this->load->view("layouts/aside");
        $this->load->view("admin/materiales/auth_Ped",$data);
        $this->load->view("layouts/footer");

    }
    /**Autorizacion del Pedido segun el nivel */
    public function updateAuthPed(){
		$idC=$this->input->post("idCod");
        $estad=$this->input->post("estad");
        $observa="";
        $fechavisado="1900-01-01";$fechaingresado="1900-01-01";
        $fechaautorizado="1900-01-01";
        if($estad==1){$ed="VISADO";$fechavisado=date("Y-m-d");}
        if($estad==2){$ed="CARGADO";}
        if($estad==3){$ed="INGRESADO";$fechaingresado=date("Y-m-d");}
        if($estad==4){$ed="AUTORIZADO";$fechaautorizado=date("Y-m-d");}
        if($estad==5){$ed="OBSERVADO";}
        if($estad==6){$ed="ANULADO";}
		$observa=$observa." ".$this->input->post("observa");
		$data=array(
			'estado'=>$ed,
            'actuacion'=>$observa,
            'fechavisado'=>$fechavisado,
            'fechaingresado'=>$fechaingresado,
            'fechaautorizado'=>$fechaautorizado
		);        
		if($this->Materiales_model->updateAuthPed($idC,$data))
        {
            echo "<script>alert('Pedido Autorizado Correctamente');</script>";
            $ruta=base_url().'/mantenimiento/materiales';
            echo "<script>window.location='".$ruta."';</script>";

		}else{
			$this->session->set_flashdata("error","No se pudo actualizar-Reintente");
			redirect(base_url()."mantenimiento/materiales/auth_ped/".$idC);
    	}
     }
     /*Realizar Pedido de cualquier secretaria, solo personal autorizado */

    public function pedCentral()
    {   
        $rol=$this->session->userdata("rol");
        $data=array(
            'pedidos'=>$this->Materiales_model->getPedidosCentral($rol),

        );
        $this->load->view("layouts/header");
        $this->load->view("layouts/aside");
        $this->load->view("admin/materiales/listCentral",$data);
        $this->load->view("layouts/footer");
    }
    /*Formulario de Pedido de cualquier secretaria, solo personal autorizado */
    public function addSCentral()
    {
        $dependencia=$this->Materiales_model->getSecretarias();
        $data=array(
            'dependencia'=>$dependencia
        );
        $this->load->view("layouts/header");
		$this->load->view("layouts/aside");
		$this->load->view("admin/materiales/addSCentral",$data);
		$this->load->view("layouts/footer");

    }
    /** Seleccion de Secretaria para realizar pedidos,se crea variable de sesion para saber la secretaria correspondiente al pedido*/
    public function addCentral()
    {
        if($this->session->userdata("temp"))
        {
            $secretaria=$this->session->userdata("temp");
        }
        
        
        else
        {
            $secretaria=$this->input->post("secret");  
            $this->session->set_userdata("temp",$secretaria);  
        }
        $dniuser=$this->session->userdata("usuario");
        $dependencia=$this->Materiales_model->getDependencias($secretaria);
        $programa=$this->Materiales_model->getProgramas($secretaria);
        $pedido=$this->Materiales_model->getPedidoDetalle($secretaria,$dniuser);
        $data=array(
            'dependencia'=>$dependencia,
            'secretaria'=>$secretaria ,
            'secretariaN'=>$dependencia[0]->detsec,
            'programa'=>$programa,
            'pedido'=>$pedido,
        );
        $this->load->view("layouts/header");
		$this->load->view("layouts/aside");
		$this->load->view("admin/materiales/addCentral",$data);
		$this->load->view("layouts/footer");


    }
    /**Reenvia al formulario de carga de un articulo de la secretaria seleccionada */
    public function addCdetalle()
    {
        $id=$this->session->userdata("temp");
        $rubro=$this->Materiales_model->getRubros();
        $subrubro=$this->Materiales_model->getSubRubros();
        $dependencia=$this->Materiales_model->getSecretarias();
        $data=array(
            'rubro'=>$rubro,
            'subrubro'=>$subrubro,
            'secretaria'=>$dependencia[0]->sec,
            'nsecre'=>$id
            
        );
        $this->load->view("layouts/header");
        $this->load->view("layouts/aside");
        $this->load->view("admin/materiales/addCdetalle",$data);
        $this->load->view("layouts/footer");
    }
    /**Guarda articulo en un pedido de cualquier secretaria sin cabecera y retorna al formulario de carga */
    public function storeCentral()
    {
        $secretaria=$this->input->post("sec");
        $dniuser=$this->session->userdata("usuario");
        $fechacarga=date("Y-m-d");
        $cantidad=$this->input->post("montof");
        $importeu=$this->input->post("montop");
        $bienes=strtoupper($this->input->post("bienes"));
        $rubro=$this->input->post("rubro");
        $subr=$this->input->post("subrubro");
        $importet=$this->input->post("totalf");
        $aniopedido=date("Y");
        $idaleatorio=0;$renglon=0;$idpedido=0;
     	$data=array(
		 'cantidad'=>$cantidad,
		 'importedetalle'=>$importet,
		 'idrubro'=>$rubro,
		 'idsubr'=>$subr,
         'detallepedido'=>$bienes,
         'idpedido'=>$idpedido,
         'idsol'=>$secretaria,
         'aniopedido'=>$aniopedido,
         'importelinea'=>$importeu,
         'idaleatorio'=>$idaleatorio,
         'renglon'=>$renglon,
         'dniuser'=>$dniuser
        );
        if($this->Materiales_model->save($data))
        {
            echo "<script>alert('Articulo Grabado Correctamente');</script>";
            $ruta=base_url().'/mantenimiento/materiales/addCentral';
            echo "<script>window.location='".$ruta."';</script>";
        }else
        {
            $this->session->set_flashdata("error","No se pudo grabar-Reintente");
            redirect(base_url()."mantenimiento/materiales/addCdetalle");
        }
    }
    /**Edita un pedido por secretaria sin cabecera */
    public function editCentral($id)
    {
        $idPedd=explode("-",$id)[0];
        $idPed=explode("-",$id)[1];
        $rubro=$this->Materiales_model->getRubros();
        $subrubro=$this->Materiales_model->getSubRubros();
        $data= array(
            'rubro'=>$rubro,
            'subrubro'=>$subrubro,
            'lista' =>$this->Materiales_model->getLinea($idPedd),
            'idped'=>$idPed 
         );
         $this->load->view("layouts/header");
         $this->load->view("layouts/aside");
         $this->load->view("admin/materiales/viewEdPedCentral",$data);
         $this->load->view("layouts/footer");

    }
    /**Actualiza un articulo grabado de un pedido sin cabecera */
    public function edstoreCentral()
    {
        $recibeid=$this->input->post("idCod");
        $id=explode("-",$recibeid)[0];
        $nid=explode("-",$recibeid)[1];
        $cantidad=$this->input->post("montof");
        $importeu=$this->input->post("montop");
        $bienes=strtoupper($this->input->post("bienes"));
        $rubro=$this->input->post("rubro");
        $subr=$this->input->post("subrubro");
        $importet=$this->input->post("totalf");
        $data=array(
		 'cantidad'=>$cantidad,
		 'importedetalle'=>$importet,
		 'idrubro'=>$rubro,
		 'idsubr'=>$subr,
         'detallepedido'=>$bienes,
         'importelinea'=>$importeu,
        );
        if($this->Materiales_model->edsave($data,$id))
        {
            echo "<script>alert('Articulo Modificado Correctamente');</script>";
            $ruta=base_url().'/mantenimiento/materiales/addCentral';
            echo "<script>window.location='".$ruta."';</script>";
        }else
        {
            $this->session->set_flashdata("error","No se pudo grabar-Reintente");
            redirect(base_url()."mantenimiento/materiales/addCentral");
        }
    }
    /**Elimina un articulo de un pedido por secretaria sin cabecera */
    public function deleteCentral($id)
    {
        $idPed=explode("-",$id)[0];
        if($this->Materiales_model->deleteArt($id))
        {
            echo "<script>alert('Articulo Eliminado Correctamente');</script>";
            $ruta=base_url().'/mantenimiento/materiales/addCentral';
            echo "<script>window.location='".$ruta."';</script>";

        }
        else
        {
            $this->session->set_flashdata("error","No se pudo Eliminar-Reintente");
            redirect(base_url()."mantenimiento/materiales/addCentral");

        }
    }
    /**Graba la cabecera de un pedido por secretaria */
    public function storecabCentral()
    {
        $secretaria=$this->input->post("secret");
        $dniuser=$this->session->userdata("usuario");
        $totalletra=$this->input->post("totalletra");
        $cta=$this->input->post("cuenta");
        if($cta==1){$cuenta="CAJA CHICA";}
        if($cta==2){$cuenta="CUENTA GENERAL";}
        if($cta==3){$cuenta="FDO.REPARACION MANTENIMIENTO Y CONTROL CALLES";}
        if($cta==4){$cuenta="FDOS.DE TERCEROS Y OTROS EN GARANTIA";}
        if($cta==5){$cuenta="FDOS.REPAROS EN GENERAL";}
        if($cta==6){$cuenta="GAS.IND.UTIL.OF.PAR.MOTOC";}
        if($cta==7){$cuenta="FDO.ESP.OBRAS INFRAESTRUCTURA";}
        if($cta==8){$cuenta="LOTERIA CHAQUEÑA";}
        if($cta==9){$cuenta="CUENTA ESPECIAL APORTE FINANCI";}
        if($cta==10){$cuenta="PRESIDENCIA CONSEJO";}
        if($cta==11){$cuenta="PRODISM";}
        if($cta==12){$cuenta="PROGRAMA COMEDORES INFANTILES";}
        if($cta==13){$cuenta="FONDO FEDERAL SOLIDARIO";}
        if($cta==14){$cuenta="CTA.ESP.SUBASTA PUBLICA";}
        if($cta==15){$cuenta="FONDO DE DESARROLLO LOCAL";}
        if($cta==16){$cuenta="OTROS";}
        $tipotramite=$this->input->post("tipotramite");
        $tipopedido=$this->input->post("tipopedido");
        $idsecre=$this->input->post("secret");
        $idssecre=$this->input->post("subsecret");
        $iddg=$this->input->post("dirgral");
        $destmat=$this->input->post("destmat");
        $fechavisado="1900-01-01";$fechaingresado="1900-01-01";
        $fechaautorizado="1900-01-01";
        $resultado=$this->Materiales_model->getNpedido();
        $aniopedido=date("Y");$fechapedido=date("Y-m-d");
        $nropedido=0;
        if($aniopedido > $resultado->anioped)
        {
            $nropedido=1;
        }
        else{
            $nropedido=($resultado->nroped) + 1;
        }
        $estado="CARGADO";
        $actuacion="";
        $programa=$this->input->post("prg");
        $subprograma=$this->input->post("sbprg");
        $cobertura=$this->input->post("cobertura");
        $totalped=$this->Materiales_model->getbuscarTP($dniuser,$secretaria,$aniopedido);
        $data=array(
		 'nropedido'=>$nropedido,
		 'aniopedido'=>$aniopedido,
		 'fechapedido'=>$fechapedido,
		 'estado'=>$estado,
         'totalped'=>$totalped->total,
         'totalletra'=>$totalletra,
         'idsolicitante'=>$dniuser,
         'idpedido'=>$nropedido,
         'isecre'=>$idsecre,
         'isubsecre'=>$idssecre,
         'idg'=>$iddg,
         'destinomat'=>$destmat,
         'cuenta'=>$cuenta,
         'actuacion'=>$actuacion,
         'fechavisado'=>$fechavisado,
         'fechaingresado'=>$fechaingresado,
         'fechaautorizado'=>$fechaautorizado,
         'tipotramite'=>$tipotramite,
         'tipopedido'=>$tipopedido,
         'idprg'=>$programa,
         'idsbprg'=>$subprograma,
         'cobertura'=>$cobertura
        );
        if($this->Materiales_model->saveCab($data))
        {
            $ultimo=$this->Materiales_model->getLastID($dniuser);
            $dataSecuencia=array(
                'nroped'=>$nropedido,
                'anioped'=>$aniopedido
            );
            $actualizarSecuencia=$this->Materiales_model->actualizarSecuencia($dataSecuencia);
            $dataCompra=array(
                'aleatorio'=>$ultimo->idpedidomateriales,
                'secretaria'=>$secretaria,
                'aniop'=>$aniopedido,
                'nrop'=>$nropedido,
                'estimado'=>$totalped->total,
                'pedmat'=>0,
                'aniooc'=>0,
                'nrooc'=>0,
                'asignado'=>0,
                'fecoc'=>"1900-01-01",
                'proveedor'=>"",
                'actuacions'=>"",
                'fecas'=>"1900-01-01",
                'nropv'=>0
            );
            $actualizarCompras=$this->Materiales_model->actualizarCompras($dataCompra);
            $dataAleatorio=array(
                'idpedido'=>$nropedido,
                'idaleatorio'=>$ultimo->idpedidomateriales
            );
            $actualizarAleatorio=$this->Materiales_model->actualizarAleatorio($aniopedido,$idsecre,$dniuser,$dataAleatorio);
            $this->session->unset_userdata("temp");

            echo "<script>alert('PEDIDO Grabado Correctamente');</script>";
            $ruta=base_url().'/mantenimiento/materiales/pedCentral';
            echo "<script>window.location='".$ruta."';</script>";
        }else
        {
            $this->session->set_flashdata("error","No se pudo grabar-Reintente");
            redirect(base_url()."mantenimiento/materiales/addCentral");
        }
    }
    /**Obtiene un pedido por Secretaria ya cargado */
    public function editPedCentral($id)
    {
        $lista=$this->Materiales_model->getPedidoxID($id);
        $secretaria=$lista[0]->isecre;
        $this->session->set_userdata("temp",$secretaria); 
        $dniuser=$this->session->userdata("usuario");
        $dependencia=$this->Materiales_model->getDependencias($secretaria);
        $secretariaN=$dependencia[0]->detsec;
        $programa=$this->Materiales_model->getProgramas($secretaria);
        $nropedido=$lista[0]->nropedido;
        $aniopedido=$lista[0]->aniopedido;
         $data=array(
            'dependencia'=>$dependencia,
            'secretaria'=>$secretaria ,
            'secretariaN'=>$secretariaN ,
            'programa'=>$programa,
            'lista'=>$lista,
            'id'=>$id,
            'nropedido'=>$nropedido,
            'aniopedido'>=$aniopedido
        );     
        $this->load->view("layouts/header");
		$this->load->view("layouts/aside");
		$this->load->view("admin/materiales/addEditCentral",$data);
		$this->load->view("layouts/footer");
    }
    /**Formulario para agregar 1 articulo a un pedido por secretaria ya grabado*/
    public function adddetalleEdCentral($id)
    {
        $rubro=$this->Materiales_model->getRubros();
        $subrubro=$this->Materiales_model->getSubRubros();
        $lista=$this->Materiales_model->getPedidoxID($id);
        $nroped=$lista[0]->nropedido;
        $anioped=$lista[0]->aniopedido;
        $data=array(
            'rubro'=>$rubro,
            'subrubro'=>$subrubro,
            'id'=>$id,
            'nroped'=>$nroped,
            'anioped'=>$anioped

        );
        $this->load->view("layouts/header");
        $this->load->view("layouts/aside");
        $this->load->view("admin/materiales/adddetalleEdCentral",$data);
        $this->load->view("layouts/footer");

    }
    /**Agregar un articulo por secretaria ya grabado */
    public function storeEdCentral($id)
    {
        $secretaria=$this->session->userdata("temp");
        $dniuser=$this->session->userdata("usuario");
        $cantidad=$this->input->post("montof");
        $importeu=$this->input->post("montop");
        $bienes=strtoupper($this->input->post("bienes"));
        $rubro=$this->input->post("rubro");
        $subr=$this->input->post("subrubro");
        $importet=$this->input->post("totalf");
        $aniopedido=$this->input->post("anioped");
        $idpedido=$this->input->post("nroped");
        $idaleatorio=$id;$renglon=0;
     	$data=array(
		 'cantidad'=>$cantidad,
		 'importedetalle'=>$importet,
		 'idrubro'=>$rubro,
		 'idsubr'=>$subr,
         'detallepedido'=>$bienes,
         'idpedido'=>$idpedido,
         'idsol'=>$secretaria,
         'aniopedido'=>$aniopedido,
         'importelinea'=>$importeu,
         'idaleatorio'=>$idaleatorio,
         'renglon'=>$renglon,
         'dniuser'=>$dniuser
        );
        if($this->Materiales_model->save($data))
        {
            echo "<script>alert('Articulo Grabado Correctamente');</script>";
            $ruta=base_url().'/mantenimiento/materiales/editPedCentral/'.$id;
            echo "<script>window.location='".$ruta."';</script>";
        }else
        {
            //ver bien adonde reenviamos, puede ser al list
            $this->session->set_flashdata("error","No se pudo grabar-Reintente");
            redirect(base_url()."mantenimiento/materiales/editPedCentral/".$id);
        }
    }
     /**Formulario de Edicion de un articulo de un pedido por secretaria ya grabado */
     public function editPCentral($id)
    {
        $idPedd=explode("-",$id)[0];
        $idPed=explode("-",$id)[1];
        $rubro=$this->Materiales_model->getRubros();
        $subrubro=$this->Materiales_model->getSubRubros();
        $data= array(
            'rubro'=>$rubro,
            'subrubro'=>$subrubro,
            'lista' =>$this->Materiales_model->getLinea($idPedd),
            'id'=>$idPed 
         );
         $this->load->view("layouts/header");
         $this->load->view("layouts/aside");
         $this->load->view("admin/materiales/viewEdPedPCentral",$data);
         $this->load->view("layouts/footer");

    }
    /**Actualiza un articulo de un pedido por secretaria ya grabado*/
    public function edstorePCentral()
    {
        $id=$this->input->post("idCod");
        $idPed=$this->input->post("idPed");
        $cantidad=$this->input->post("montof");
        $importeu=$this->input->post("montop");
        $bienes=strtoupper($this->input->post("bienes"));
        $rubro=$this->input->post("rubro");
        $subr=$this->input->post("subrubro");
        $importet=$this->input->post("totalf");
        $data=array(
		 'cantidad'=>$cantidad,
		 'importedetalle'=>$importet,
		 'idrubro'=>$rubro,
		 'idsubr'=>$subr,
         'detallepedido'=>$bienes,
         'importelinea'=>$importeu,
        );
        if($this->Materiales_model->edsave($data,$id))
        {
            echo "<script>alert('Articulo Modificado Correctamente');</script>";
            $ruta=base_url().'/mantenimiento/materiales/editPedCentral/'.$idPed;
            echo "<script>window.location='".$ruta."';</script>";
        }else
        {
            $this->session->set_flashdata("error","No se pudo grabar-Reintente");
            redirect(base_url()."mantenimiento/materiales/editPedCentral/".$id);
        }
    }
    /**Elimina un articulo de un pedido por secretaria ya grabado */
    public function deletePCentral($id)
    {
        
        $lista=$this->Materiales_model->getLinea($id);
        $idPed=$lista->idaleatorio;
        if($this->Materiales_model->deleteArt($id))
        {
            echo "<script>alert('Articulo Eliminado Correctamente');</script>";
            $ruta=base_url().'/mantenimiento/materiales/editPedCentral/'.$idPed;
            echo "<script>window.location='".$ruta."';</script>";

        }
        else
        {
            $this->session->set_flashdata("error","No se pudo Eliminar-Reintente");
            redirect(base_url()."mantenimiento/materiales/editPedCentral/".$idPed);

        }
    }
    /** Graba la cabecera de un pedido por secretaria ya grabado */
    public function storecabEdCentral($id)
    {
        $secretaria=$this->session->userdata("temp");
        $dniuser=$this->session->userdata("usuario");
        $totalletra=$this->input->post("totalletra");
        $cta=$this->input->post("cuenta");
        if($cta==1){$cuenta="CAJA CHICA";}
        if($cta==2){$cuenta="CUENTA GENERAL";}
        if($cta==3){$cuenta="FDO.REPARACION MANTENIMIENTO Y CONTROL CALLES";}
        if($cta==4){$cuenta="FDOS.DE TERCEROS Y OTROS EN GARANTIA";}
        if($cta==5){$cuenta="FDOS.REPAROS EN GENERAL";}
        if($cta==6){$cuenta="GAS.IND.UTIL.OF.PAR.MOTOC";}
        if($cta==7){$cuenta="FDO.ESP.OBRAS INFRAESTRUCTURA";}
        if($cta==8){$cuenta="LOTERIA CHAQUEÑA";}
        if($cta==9){$cuenta="CUENTA ESPECIAL APORTE FINANCI";}
        if($cta==10){$cuenta="PRESIDENCIA CONSEJO";}
        if($cta==11){$cuenta="PRODISM";}
        if($cta==12){$cuenta="PROGRAMA COMEDORES INFANTILES";}
        if($cta==13){$cuenta="FONDO FEDERAL SOLIDARIO";}
        if($cta==14){$cuenta="CTA.ESP.SUBASTA PUBLICA";}
        if($cta==15){$cuenta="FONDO DE DESARROLLO LOCAL";}
        if($cta==16){$cuenta="OTROS";}
        $tipotramite=$this->input->post("tipotramite");
        $tipopedido=$this->input->post("tipopedido");
        $idsecre=$this->input->post("secret");
        $idssecre=$this->input->post("subsecret");
        $iddg=$this->input->post("dirgral");
        $destmat=$this->input->post("destmat");
        $estado="CARGADO";
        $programa=$this->input->post("prg");
        $subprograma=$this->input->post("sbprg");
        $cobertura=$this->input->post("cobertura");        
        $totalped=$this->Materiales_model->getbuscarTPEd($id);
        $data=array(
		 'estado'=>$estado,
         'totalped'=>$totalped->total,
         'totalletra'=>$totalletra,
         'isecre'=>$idsecre,
         'isubsecre'=>$idssecre,
         'idg'=>$iddg,
         'destinomat'=>$destmat,
         'cuenta'=>$cuenta,
         'tipotramite'=>$tipotramite,
         'tipopedido'=>$tipopedido,
         'idprg'=>$programa,
         'idsbprg'=>$subprograma,
         'cobertura'=>$cobertura
        );
        if($this->Materiales_model->saveCabEd($data,$id))
        {
            $dataCompra=array(
                'estimado'=>$totalped->total
            );
            $actualizarCompras=$this->Materiales_model->actualizarComprasEd($dataCompra,$id);
            $this->session->unset_userdata("temp");
            echo "<script>alert('PEDIDO Modificado Correctamente');</script>";
            $ruta=base_url().'/mantenimiento/materiales/pedCentral';
            echo "<script>window.location='".$ruta."';</script>";
        }else
        {
            $this->session->set_flashdata("error","No se pudo grabar-Reintente");
            redirect(base_url()."mantenimiento/materiales/pedCentral");
        }
    }
    /**Tabla con los pedidos cargados para que compras gestion los pedidos */
    public function gestionCompras()
    {
        $data=array(
            'pedidos'=>$this->Materiales_model->getGestionCompras(),

        );
        $this->load->view("layouts/header");
        $this->load->view("layouts/aside");
        $this->load->view("admin/materiales/listCompras",$data);
        $this->load->view("layouts/footer");

    }
    /**Trae los datos de un pedido para ser completado por compras */
    public function editPedCompras($id)
     {
         $lista=$this->Materiales_model->getGestionComprasxID($id);
         $data= array(
             'lista' =>$lista,
          );
          $this->load->view("layouts/header");
          $this->load->view("layouts/aside");
          $this->load->view("admin/materiales/viewEdCompras",$data);
          $this->load->view("layouts/footer");
 
     }
     /** Actualiza los datos de la oc del pedido de material*/
     public function edstoreCompras()
    {
        $id=$this->input->post("idPed");
        $pedmatcom=$this->input->post("pedmatcom");
        $aniooc=$this->input->post("anoc");
        if($aniooc==1){$anoc=date("Y");}if($aniooc==2){$anoc=date("Y")-1;}
        if($aniooc==3){$anoc=date("Y")-2;}if($aniooc==4){$anoc=date("Y")-3;}if($aniooc==5){$anoc=date("Y")-4;}
        $nrooc=$this->input->post("noc");$asignado=$this->input->post("asigna");
        $fecoc=$this->input->post("fecoc");
        $proveedor=strtoupper($this->input->post("proveer"));$proven=$this->input->post("provee");$actuacions=strtoupper($this->input->post("actuacions"));
        $feccon=$this->input->post("feccon");
        $data=array(
		 'pedmat'=>$pedmatcom,
		 'aniooc'=>$anoc,
		 'nrooc'=>$nrooc,
		 'asignado'=>$asignado,
         'fecoc'=>$fecoc,
         'proveedor'=>$proveedor,
         'actuacions'=>$actuacions,
         'fecas'=>$feccon,
         'nropv'=>$proven,
        );
        if($this->Materiales_model->updateGestionCompras($id,$data))
        {
            $estado="AUTORIZADO";$actuacion=date("Y-m-d")."-Compras";
            $dataG=array(
                'estado'=>$estado,
                'actuacion'=>$actuacion,

            );
           $actualOC=$this->Materiales_model->updateCompletaOC($id,$dataG);
           echo "<script>alert('Pedido Actualizado Correctamente');</script>";
           $ruta=base_url().'/mantenimiento/materiales/gestionCompras';
           echo "<script>window.location='".$ruta."';</script>";
        }else
        {
            $this->session->set_flashdata("error","No se pudo grabar-Reintente");
            redirect(base_url()."mantenimiento/materiales/gestionCompras");
        }
    }
    /**form para obtener los pedidos por estado */
    public function question()
    {
        $this->load->view("layouts/header");
        $this->load->view("layouts/aside");
        $this->load->view("admin/materiales/questionHead");
        $this->load->view("admin/materiales/questionBody");
        $this->load->view("layouts/footer");
    }
    /**Muestra los pedidos por estado,con fecha y se puede reimprimir */
    public function questionDetalle()
    {
        $est=$this->input->post('estado');
        $rol=$this->session->userdata("rol");
        $secretaria=$this->session->userdata("unidad");

        if($est==1){$estado="CARGADO";}
        if($est==2){$estado="VISADO";}
        if($est==3){$estado="INGRESADO";}
        if($est==4){$estado="AUTORIZADO";}
        if($est==5){$estado="OBSERVADO";}
        $data=$this->Materiales_model->getQuestionDetalle($estado,$rol,$secretaria);
        if($data)
        {
            $datos['pedidos']=$data;
            $this->load->view("layouts/header");
            $this->load->view("layouts/aside");
            $this->load->view("admin/materiales/questionHead");
            $this->load->view("admin/materiales/questionBody",$datos);
            $this->load->view("layouts/footer");

        }
        else
        {
            $this->session->set_flashdata("error","NO EXISTEN DATOS-VERIFIQUE");
            redirect(base_url()."mantenimiento/materiales/question");

        }
    }
    /**Lista los programas y da la posibilidad de agregar programas nuevos por secretaria */
    public function programas()
    {
        $data=array(
            'programas'=>$this->Materiales_model->getProgramasAll(),

        );
        $this->load->view("layouts/header");
        $this->load->view("layouts/aside");
        $this->load->view("admin/materiales/listProgramas",$data);
        $this->load->view("layouts/footer");
    }
    /**Formulario de Alta de Programas */
    public function addProgramas()
    {
        $dependencia=$this->Materiales_model->getSecretarias();
        $programa=$this->Materiales_model->getProgramasAll();
        $data=array(
            'dependencia'=>$dependencia,
            'programa'=>$programa,
        );
        $this->load->view("layouts/header");
		$this->load->view("layouts/aside");
		$this->load->view("admin/materiales/addProgramasDetalle",$data);
		$this->load->view("layouts/footer");

    }
    /**Graba un programa */
    public function storeProgramas()
    {
        $secretaria=$this->input->post("nsec");
        $nroprg=$this->input->post("numprg");
        $descripcion=strtoupper($this->input->post("detprg"));
        $fecini=$this->input->post("bddesde");
        $fechasta=$this->input->post("bdhasta");
        $estado="A";
        $data=array(
            'nroprg' =>$nroprg,
            'descripcion'=>$descripcion,
            'programadesde'=>$fecini,
            'programahasta'=>$fechasta,
            'estado'=>$estado,
            'idsec'=>$secretaria,
         );
        if(!$this->Materiales_model->getConsultaPrograma($nroprg,$secretaria))
        {
            if($this->Materiales_model->addProgramas($data))
        {
            $nrospg=1;$descspg="TODOS";
            $dataspg = array(
                'nrosbprg' =>$nrospg,
                'descripcionsp'=>$descspg,
                'spprogramadesde'=>$fecini,
                'spprogramahasta'=>$fechasta,
                'spestado'=>"A",
                'idpgr'=>$nroprg,
                'idsbsec'=>$secretaria,
             );
             $actual=$this->Materiales_model->addSProgramas($dataspg);
             echo "<script>alert('Programa Cargado Correctamente');</script>";
             $ruta=base_url().'/mantenimiento/materiales/programas';
             echo "<script>window.location='".$ruta."';</script>";
        }
        else
        {
            $this->session->set_flashdata("error","No se pudo grabar-Reintente");
            redirect(base_url()."mantenimiento/materiales/programas");

        }

        }
        else
        {
            echo "<script>alert('Ya Existe ese Programa en la Secretaria');</script>";
             $ruta=base_url().'/mantenimiento/materiales/programas';
             echo "<script>window.location='".$ruta."';</script>";

        }
        
    }
    /**Eliminar un programa-se cambia el estado a Inactivo */
    public function deleteProgramas($id)
    {
        $data=array(
            'estado'=>"I",
        );
        if($this->Materiales_model->deleteProgramas($id,$data))
        {
            echo "<script>alert('Programa Eliminado Correctamente');</script>";
             $ruta=base_url().'/mantenimiento/materiales/programas';
             echo "<script>window.location='".$ruta."';</script>";

        }
        else
        {
            echo "<script>alert('No se pudo eliminar el Programa');</script>";
            $ruta=base_url().'/mantenimiento/materiales/programas';
            echo "<script>window.location='".$ruta."';</script>";
        }
    }
    /**Formulario de edicion de un Programa */
    public function editProgramas($id)
    {
         $lista=$this->Materiales_model->getLineaPrograma($id);
         $dependencia=$this->Materiales_model->getSecretarias();
         $data= array(
             'lista' =>$lista,
             'dependencia'=>$dependencia,
          );
          $this->load->view("layouts/header");
          $this->load->view("layouts/aside");
          $this->load->view("admin/materiales/viewEdProgramas",$data);
          $this->load->view("layouts/footer");

    }
    /**Actualiza info de Programas */
    public function storeEdProgramas($id)
    {
        $secretaria=$this->input->post("nsec");
        $nroprg=$this->input->post("numprg");
        $descripcion=strtoupper($this->input->post("detprg"));
        $fecini=$this->input->post("bddesde");
        $fechasta=$this->input->post("bdhasta");
        $estado="A";
        $data=array(
            'nroprg' =>$nroprg,
            'descripcion'=>$descripcion,
            'programadesde'=>$fecini,
            'programahasta'=>$fechasta,
            'estado'=>$estado,
            'idsec'=>$secretaria,
         );
        if(!$this->Materiales_model->getConsultaPrograma($nroprg,$secretaria))
        {
            if($this->Materiales_model->addProgramas($data))
        {
            $nrospg=1;$descspg="TODOS";
            $dataspg = array(
                'nrosbprg' =>$nrospg,
                'descripcionsp'=>$descspg,
                'spprogramadesde'=>$fecini,
                'spprogramahasta'=>$fechasta,
                'spestado'=>"A",
                'idpgr'=>$nroprg,
                'idsbsec'=>$secretaria,
             );
             $actual=$this->Materiales_model->addSProgramas($dataspg);
             echo "<script>alert('Programa Cargado Correctamente');</script>";
             $ruta=base_url().'/mantenimiento/materiales/programas';
             echo "<script>window.location='".$ruta."';</script>";
        }
        else
        {
            $this->session->set_flashdata("error","No se pudo grabar-Reintente");
            redirect(base_url()."mantenimiento/materiales/programas");

        }

        }
        else
        {
            $edita=$this->Materiales_model->actualizaProg($id,$data);
            echo "<script>alert('Programa Modificado');</script>";
             $ruta=base_url().'/mantenimiento/materiales/programas';
             echo "<script>window.location='".$ruta."';</script>";

        }
        

    }
    /**Lista los programas y da la posibilidad de agregar subprogramas nuevos por secretaria */
    public function subprogramas()
    {
        $data=array(
            'programas'=>$this->Materiales_model->getSProgramasAll(),

        );
        //var_dump($data);
        $this->load->view("layouts/header");
        $this->load->view("layouts/aside");
        $this->load->view("admin/materiales/listSProgramas",$data);
        $this->load->view("layouts/footer");

    }
    /**Seguimiento de Pedidos de Materiales */
    public function seguimientoPed()
    {
        $flag=0;
        $secretaria=$this->session->userdata("unidad");
        $rol=$this->session->userdata("rol");
        $bp=$this->input->post('bp');
        /**Filtro por razon social */
        if(isset($bp))
        {
            $bsprod=$this->input->post('bsprod'); $flag=1;
        }else{$bsprod="";}
        /**Filtro por fecha de pedido electronico */
        $bf=$this->input->post('bf');
        if(isset($bf))
        {
            $bddesde=$this->input->post('bddesde');
            $bdhasta=$this->input->post('bdhasta');$flag=1;
        }else{$bddesde="1900-01-01";$bdhasta="9999-12-31";}
        /**filtro por numero aleatorio de pedido */
        $bfp=$this->input->post('bfp');
        if(isset($bfp))
        {
            $aleatoriod=$this->input->post('aleatoriod');$aleatorioh=$this->input->post('aleatorioh');$flag=1;
        }else{$aleatoriod=0;$aleatorioh=999999;}
        /**filtro por numero de proveedor */
        $bpr=$this->input->post('bpr');
        if(isset($bpr))
        {
            $nroprovd=$this->input->post('nroprovd');$nroprovh=$this->input->post('nroprovh');$flag=1;
        }else{$nroprovd=0;$nroprovh=99999;}
        /**Filtro por Numero de Orden de Compra */
        $bOC=$this->input->post('bOC');
        if(isset($bOC))
        {
            $nocd=$this->input->post('nocd');
            $noch=$this->input->post('noch');$flag=1;
        }else{$nocd=0;$noch=99999;}
        /**Filtro por numero de pedido de material de compras */
        $bPM=$this->input->post('bPM');
        if(isset($bPM))
        {
            $npmd=$this->input->post('npmd');
            $npmh=$this->input->post('npmh');$flag=1;
        }else{$npmd=0;$npmh=99999;}
        if($rol > 2)
            {
            if($flag==0)
            {
                $sql="select a.idpedidomateriales, a.isecre, 
                a.aniopedido, a.nropedido, a.fechapedido,a.totalped, b.pedmat, b.aniooc, 
                b.nrooc, b.asignado, b.fecoc, b.proveedor, b.actuacions, 
                b.fecas, b.nropv from pedidomateriales a, gestioncompra b 
                where (a.idpedidomateriales=b.aleatorio) 
                order by a.idpedidomateriales;";

            }
            else
            {
                if($bsprod!="")
                {
                    $sql="select a.idpedidomateriales, a.isecre,a.aniopedido, a.nropedido, a.totalped, a.fechapedido, b.pedmat, b.aniooc, b.nrooc, b.asignado, b.fecoc, b.proveedor, b.actuacions,b.fecas, b.nropv from pedidomateriales a, gestioncompra b where (b.proveedor like '%$bsprod%') and  (a.fechapedido between '".$bddesde."'and'".$bdhasta."')and (b.nropv between '".$nroprovd."'and '".$nroprovh."') and (b.aleatorio between '".$aleatoriod."'and '".$aleatorioh."')and(b.pedmat between '".$npmd."' and '".$npmh."') and(b.nrooc between '".$nocd."' and '".$noch."')and(a.idpedidomateriales=b.aleatorio)order by a.idpedidomateriales;";
                }
                else
                {
                    $sql="select a.idpedidomateriales, a.isecre, a.aniopedido, a.nropedido, a.totalped, a.fechapedido, b.pedmat, b.aniooc, b.nrooc, b.asignado, b.fecoc, b.proveedor, b.actuacions,b.fecas, b.nropv from pedidomateriales a, gestioncompra b where (a.idpedidomateriales=b.aleatorio) and (a.fechapedido between '".$bddesde."'and'".$bdhasta."')and(b.nropv between '".$nroprovd."'and '".$nroprovh."')    and(b.pedmat between '".$npmd."' and '".$npmh."') and(b.nrooc between '".$nocd."' and '".$noch."')and (b.aleatorio between '".$aleatoriod."'and '".$aleatorioh."') order by a.idpedidomateriales;";  
                }
            }
        } //este se usa para cerrar los permisos mayores a operador
    //aca viene lo de operadores,necesita secretaria 
      else
      
      {
        if($flag==0)
        {
            $sql="select a.idpedidomateriales, a.isecre,a.aniopedido, a.nropedido, a.fechapedido,a.totalped, b.pedmat, b.aniooc, b.nrooc, b.asignado, b.fecoc, b.proveedor, b.actuacions, b.fecas, b.nropv from pedidomateriales a, gestioncompra b where (a.idpedidomateriales=b.aleatorio) and (a.isecre='".$secretaria."') order by a.idpedidomateriales;";
        }else
        {
            if($bsprod!="")
            {
                $sql="select a.idpedidomateriales, a.isecre,a.aniopedido, a.nropedido, a.totalped, a.fechapedido, b.pedmat, b.aniooc,b.nrooc, b.asignado, b.fecoc, b.proveedor, b.actuacions,  b.fecas, b.nropv from pedidomateriales a, gestioncompra b  where (b.proveedor like '%$bsprod%') and (a.fechapedido between '".$bddesde."'and'".$bdhasta."')and (b.nropv between '".$nroprovd."'and '".$nroprovh."') and(b.pedmat between '".$npmd."' and '".$npmh."') and(b.nrooc between '".$nocd."' and '".$noch."') and (b.aleatorio between '".$aleatoriod."'and '".$aleatorioh."')and (a.idpedidomateriales=b.aleatorio) and (a.isecre='".$secretaria."') order by a.idpedidomateriales;";
            }else
             {
                 $sql="select a.idpedidomateriales, a.isecre,  a.aniopedido, a.nropedido, a.totalped, a.fechapedido, b.pedmat, b.aniooc, b.nrooc, b.asignado, b.fecoc, b.proveedor, b.actuacions, b.fecas, b.nropv from pedidomateriales a, gestioncompra b where (a.idpedidomateriales=b.aleatorio) and (a.fechapedido between '".$bddesde."'and'".$bdhasta."')and (b.nropv between '".$nroprovd."'and '".$nroprovh."')and(b.pedmat between '".$npmd."' and '".$npmh."') and(b.nrooc between '".$nocd."' and '".$noch."')and (b.aleatorio between '".$aleatoriod."'and '".$aleatorioh."') and (a.isecre='".$secretaria."') order by a.idpedidomateriales;"; 
            }
        }
  }
    
      if((!isset($bp))&&(!isset($bf))&&(!isset($bfp))&&(!isset($bpr))&&(!isset($bOC))&&(!isset($bPM)))
      {
        $this->load->view("layouts/header");
        $this->load->view("layouts/aside");
        $this->load->view("admin/materiales/seguimientoHead");
        $this->load->view("layouts/footer");

      } 
      else
      {
        $data=$this->Materiales_model->getViewSeguimiento($sql,$secretaria);
       // var_dump($sql);
        if($data)
        {
           
            $datos['pedidos']=$data;
            $this->load->view("layouts/header");
            $this->load->view("layouts/aside");
            $this->load->view("admin/materiales/seguimientoHead");
            $this->load->view("admin/materiales/seguimientoBody",$datos);
            $this->load->view("layouts/footer");

        }
        else
        {
            $this->session->set_flashdata("error","NO EXISTEN DATOS-VERIFIQUE");
            redirect(base_url()."mantenimiento/materiales/seguimientoPed");

        }

      }

       
    }
    /**Formulario para migrar pedido de materiales */
    public function migrarPed()
    {
        $this->load->view("layouts/header");
        $this->load->view("layouts/aside");
        $this->load->view("admin/materiales/migrarPed");
        $this->load->view("layouts/footer");
    }
    /**Migracion del Pedido */
    public function migrarPedDetalle()
    {
        $nroped=$this->input->post("nroped");
        $anio=date("Y")-1;
        $dniusuario=$this->session->userdata("usuario");
        $pedido=$this->Materiales_model->getPedidoaMigrar($nroped,$anio,$dniusuario);
       // var_dump($pedido);
        $id=$pedido->idpedidomateriales;
      //  var_dump($id);
        $detPedido=$this->Materiales_model->getPedidoaMigrarDetalle($id);
      //  var_dump($detPedido);
        /**Primero grabo el detalle */
        $secretaria=$this->session->userdata("unidad");
        $dniuser=$this->session->userdata("usuario");
        foreach($detPedido as $det)
        {
            $cantidad=$det->cantidad;
            $importedetalle=$det->importedetalle;
            $rubro=$det->idrubro;
            $subr=$det->idsubr;
            $bienes=strtoupper($det->detallepedido);
            $idsol=$det->idsol;
            $aniopedido=date("Y");
            $importelinea=$det->importelinea;
            $idaleatorio=0;$renglon=0;$idpedido=0;
            $data=array(
                'cantidad'=>$cantidad,
                'importedetalle'=>$importedetalle,
                'idrubro'=>$rubro,
                'idsubr'=>$subr,
                'detallepedido'=>$bienes,
                'idpedido'=>$idpedido,
                'idsol'=>$idsol,
                'aniopedido'=>$aniopedido,
                'importelinea'=>$importelinea,
                'idaleatorio'=>$idaleatorio,
                'renglon'=>$renglon,
                'dniuser'=>$dniuser
               );
            if($this->Materiales_model->save($data))
            {
               echo "<script>alert('Articulo Migrado Correctamente');</script>";
              /* $ruta=base_url().'/mantenimiento/materiales/add';
               echo "<script>window.location='".$ruta."';</script>";*/
            }
            else
            {
            $this->session->set_flashdata("error","No se pudo grabar-Reintente");
            redirect(base_url()."mantenimiento/materiales/migarPed");
            }
        }//del foreach del detalle
        /*$secretaria=$this->session->userdata("unidad");
        $dniuser=$this->session->userdata("usuario");*/
        $totalped=$pedido->totalped;
        $totalletra=$pedido->totalletra;
        $cuenta=$pedido->cuenta;
        $tipotramite=$pedido->tipotramite;
        $tipopedido=$pedido->tipopedido;
        $idsecre=$pedido->isecre;
        $idssecre=$pedido->isubsecre;
        $iddg=$pedido->idg;
        $destmat=$pedido->destinomat;
        $fechavisado="1900-01-01";$fechaingresado="1900-01-01";
        $fechaautorizado="1900-01-01";
        $resultado=$this->Materiales_model->getNpedido();
        $aniopedido=date("Y");$fechapedido=date("Y-m-d");
        $nropedido=0;
        if($aniopedido > $resultado->anioped)
         {
           $nropedido=1;
         }
        else
        {
            $nropedido=($resultado->nroped) + 1;
        }
        $estado="CARGADO";
        $actuacion="";
        $programa=$pedido->idprg;
        $subprograma=$pedido->idsbprg;
        $cobertura=$pedido->cobertura;
        $totalped=$pedido->totalped;
        $data=array(
            'nropedido'=>$nropedido,
            'aniopedido'=>$aniopedido,
            'fechapedido'=>$fechapedido,
            'estado'=>$estado,
            'totalped'=>$totalped,
            'totalletra'=>$totalletra,
            'idsolicitante'=>$dniuser,
            'idpedido'=>$nropedido,
            'isecre'=>$idsecre,
            'isubsecre'=>$idssecre,
            'idg'=>$iddg,
            'destinomat'=>$destmat,
            'cuenta'=>$cuenta,
            'actuacion'=>$actuacion,
            'fechavisado'=>$fechavisado,
            'fechaingresado'=>$fechaingresado,
            'fechaautorizado'=>$fechaautorizado,
            'tipotramite'=>$tipotramite,
            'tipopedido'=>$tipopedido,
            'idprg'=>$programa,
            'idsbprg'=>$subprograma,
            'cobertura'=>$cobertura
           );
           if($this->Materiales_model->saveCab($data))
         {
             $ultimo=$this->Materiales_model->getLastID($dniuser);
             $dataSecuencia=array(
                 'nroped'=>$nropedido,
                 'anioped'=>$aniopedido
             );
             $actualizarSecuencia=$this->Materiales_model->actualizarSecuencia($dataSecuencia);
             $dataCompra=array(
                 'aleatorio'=>$ultimo->idpedidomateriales,
                 'secretaria'=>$secretaria,
                 'aniop'=>$aniopedido,
                 'nrop'=>$nropedido,
                 'estimado'=>$totalped,
                 'pedmat'=>0,
                 'aniooc'=>0,
                 'nrooc'=>0,
                 'asignado'=>0,
                 'fecoc'=>"1900-01-01",
                 'proveedor'=>"",
                 'actuacions'=>"",
                 'fecas'=>"1900-01-01",
                 'nropv'=>0
             );
             $actualizarCompras=$this->Materiales_model->actualizarCompras($dataCompra);
             $dataAleatorio=array(
                 'idpedido'=>$nropedido,
                 'idaleatorio'=>$ultimo->idpedidomateriales
             );
             $actualizarAleatorio=$this->Materiales_model->actualizarAleatorio($aniopedido,$idsecre,$dniuser,$dataAleatorio);
 
             echo "<script>alert('PEDIDO Grabado Correctamente');</script>";
             $ruta=base_url().'/mantenimiento/materiales';
             echo "<script>window.location='".$ruta."';</script>";
         }else
         {
             $this->session->set_flashdata("error","No se pudo grabar-Reintente");
             redirect(base_url()."mantenimiento/materiales");
         }
    }
     /**Listado de detalle de pedidos por distintos Criterios */
    public function detallePM()
    {
        $flag=0;
        $secretaria=$this->session->userdata("unidad");
        $rol=$this->session->userdata("rol");
        $bp=$this->input->post('bp');
        /**Filtro por Secretaria */
        if(isset($bp))
        {
            $bs=$this->input->post('bsprod'); $flag=1;
        }else{$bs="";}
        /**Filtro por fecha de pedido electronico */
        $bf=$this->input->post('bf');
        if(isset($bf))
        {
            $bddesde=$this->input->post('bddesde');
            $bdhasta=$this->input->post('bdhasta');$flag=1;
        }else{$bddesde="1900-01-01";$bdhasta="9999-12-31";}
        /**Filtro por fecha de autorizacion de pedido */
        $fbf=$this->input->post('fbf');
        if(isset($fbf))
        {
            $fbddesde=$this->input->post('fbddesde');
            $hbdhasta=$this->input->post('fbdhasta');$flag=1;
        }else{$fbddesde="1900-01-01";$hbdhasta="9999-12-31";}
        if($flag==0)
        {
            $sql="select * from detallepedidomateriales a,pedidomateriales b where(a.idsol=b.isecre) order by a.iddetallepm asc";
        }
        else
        {
            if($bs==0)
            {
                $sql="select * from detallepedidomateriales a,pedidomateriales b where(a.idsol=b.isecre)and(a.idaleatorio=b.idpedidomateriales)and(b.fechapedido between '".$bddesde."' and '".$bdhasta."')and (b.fechaautorizado between '".$fbddesde."' and '".$hbdhasta."') order by a.idsol asc ;";
            }
            else
            {
                $sql="select * from detallepedidomateriales a,pedidomateriales b where ((a.idsol=b.isecre)and(a.idsol ='".$bs."'))and(a.idaleatorio=b.idpedidomateriales)and (b.fechapedido between '".$bddesde."' and '".$bdhasta."')and (b.fechaautorizado between '".$fbddesde."' and '".$hbdhasta."')
                     order by a.idsol asc ;";  
            }
        }
        if((!isset($bp))&&(!isset($bf))&&(!isset($fbf)))
        {
            $this->load->view("layouts/header");
            $this->load->view("layouts/aside");
            $this->load->view("admin/materiales/expDetallePMHead");
            $this->load->view("layouts/footer");
        }
        else
        {
            $data=$this->Materiales_model->getViewSeguimiento($sql,$secretaria);
            if($data)
            {
                $datos['pedidos']=$data;
                $this->load->view("layouts/header");
                $this->load->view("layouts/aside");
                $this->load->view("admin/materiales/expDetallePMHead");
                $this->load->view("admin/materiales/expDetallePMBody",$datos);
                $this->load->view("layouts/footer");
            }
            else
            {
                $this->session->set_flashdata("error","NO EXISTEN DATOS-VERIFIQUE");
                redirect(base_url()."mantenimiento/materiales/detallePM");
            }
        }
    }
}