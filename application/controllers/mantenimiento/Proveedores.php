<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Proveedores extends CI_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->model('Proveedores_model');
  
    }
    public function index()
    {
        $data=array(
            'pedidos'=>$this->Proveedores_model->getProveedores(),

        );
        $this->load->view("layouts/header");
        $this->load->view("layouts/aside");
        $this->load->view("admin/proveedores/list",$data);
        $this->load->view("layouts/footer");
    }
    /**Formulario para dar de alta un proveedor */
    public function add()
    {
        $this->load->view("layouts/header");
        $this->load->view("layouts/aside");
        $this->load->view("admin/proveedores/add");
        $this->load->view("layouts/footer");
    }
    /**Agregar un proveedor que no esta en la base de datos */
    public function storeProve()
    {
        $nroprov=$this->input->post("nroprov");
        $res=$this->Proveedores_model->getProveedorxNro($nroprov);
        if($res)
        {
            $this->session->set_flashdata("error","YA EXISTE PROVEEDOR-VERIFIQUE LOS DATOS INGRESADOS");
            redirect(base_url()."mantenimiento/proveedores/add");
        }
        else
        {
            $razonsocial=strtoupper($this->input->post('razonsocial'));$cuit="";$apellido="";$nombre="";$ec="";$dni=0;$domicilio="";$domicilio2="";$localidad="";$provincia="";$tel1="";$tel2="";$email="";$email2="";$conyuge="";$causa="-";$privilegio="-";$usuario="USER-".$nroprov;$clave=md5($nroprov);$estate="P";
            $data = array(
                'usuario' =>$usuario ,
                'clave'=>$clave,
                'nroprov'=>$nroprov,
                'razonsocial'=>$razonsocial,
                'cuit'=>$cuit,
                'dni'=>$dni,
                'apellido'=>$apellido,
                'nombre'=>$nombre,
                'estadocivil'=>$ec,
                'domicilio'=>$domicilio,
                'domicilio2'=>$domicilio2,
                'localidad'=>$localidad,
                'provincia'=>$provincia,
                'tel1'=>$tel1,
                'tel2'=>$tel2,
                'email'=>$email,
                'email2'=>$email2,
                'conyuge'=>$conyuge,
                'estado'=>$estate,
                'causa'=>$causa,
                'privilegio'=>$privilegio

             );
             if($this->Proveedores_model->addProveedor($data))
             {
                 $dataG = array(
                     'usuario' =>$usuario ,
                     'clave'=>$clave,
                     'nroprov'=>$nroprov,
                     'razonsocial'=>$razonsocial,
                     'estado'=>$estate 
                    );
                 if($this->Proveedores_model->addClaveProvedor($dataG))
                 {
                    echo "<script>alert('Proveedor Agregado Correctamente');</script>";
                    $ruta=base_url().'principal';
                    echo "<script>window.location='".$ruta."';</script>";
                 }
                 else
                 {
                    $this->session->set_flashdata("error","No se pudo Agregar Clave de usuario-Reintente");
                    redirect(base_url()."mantenimiento/proveedores");
                 }
                
             }
             else
             {
                $this->session->set_flashdata("error","No se pudo grabar-Reintente");
                redirect(base_url()."mantenimiento/proveedores/add");
             }

            
        }

    }
    /**Formulario para registrar un formulario sin Gestion judicial */
    public function registraFormSG()
    {
        $this->load->view("layouts/header");
        $this->load->view("layouts/aside");
        $this->load->view("admin/proveedores/registraFormSG");
        $this->load->view("layouts/footer");

    }
    /**agregar un formulario de Deuda */
    public function addFormSG()
    {
        $anio=date('Y'); $fecrec=date("Y-m-d");
        $nroprov=$this->input->post('nroprov');
        $razonsocial=strtoupper($this->input->post('razonsocial'));
        $montor=$this->input->post('montor');
        $pagosp=$this->input->post('pagosp');
        $totald=$this->input->post('totald');
        $observa=$this->input->post('observa');
        if(!isset($observa))
        {
            $observa="";
        }   
        $nf=$this->Proveedores_model->getNroFormSG();
        $formulario=$nf->numero+1;//var_dump($formulario);
        $data = array(
            'nroform' =>$formulario ,
            'anioform'=>$anio,
            'nroprov'=>$nroprov,
            'razonsocial'=>$razonsocial,
            'fechapresenta'=>$fecrec,
            'montoreclamado'=>$montor,
            'pagosrecibidos'=>$pagosp,
            'totaldeuda'=>$totald,
            'observaciones'=>$observa
         );
         if($this->Proveedores_model->saveFormSG($data))
         {
            $nrof = array('numero' =>$formulario , );$estad="SI";
            $dataIN = array('imprimio' =>$estad , );
            $actualizar=$this->Proveedores_model->updateFormSG($nrof);
            $inhabilitaSG=$this->Proveedores_model->inhabilitaSG($nroprov,$dataIN);
            echo "<script>alert('Formulario Registrado-Facturas Inhabilitadas Correctamente');</script>";
            $ruta=base_url().'principal';
            echo "<script>window.location='".$ruta."';</script>";

         }
         else
         {
            $this->session->set_flashdata("error","No se pudo grabar-Reintente");
            redirect(base_url()."mantenimiento/proveedores/registraFormSG");

         }

    }
    /**Formulario para registrar un formulario con Gestion judicial */
    public function registraFormCG()
    {
        $this->load->view("layouts/header");
        $this->load->view("layouts/aside");
        $this->load->view("admin/proveedores/registrarFormCG");
        $this->load->view("layouts/footer");

    }
    /**agregar un formulario de Deuda con Gestion Judicial */
    public function addFormCG()
    {
        $anio=date('Y'); $fecrec=date("Y-m-d");
        $dnidem=$this->input->post('dnidem');
        $ayn=strtoupper($this->input->post('ayn'));
        $monto=$this->input->post('montor');
        $honorarios=$this->input->post('hr');
        $costas=$this->input->post('cr');
        $totald=$this->input->post('totald');
        $observa=$this->input->post('observa');
        if(!isset($observa))
        {
            $observa="";
        }   
        $nf=$this->Proveedores_model->getNroFormCG();
        $formulario=$nf->numero+1;//var_dump($formulario);
        $data = array(
            'nroform' =>$formulario ,
            'anioform'=>$anio,
            'nroprov'=>$dnidem,
            'razonsocial'=>$ayn,
            'fechapresenta'=>$fecrec,
            'monto'=>$monto,
            'honorarios'=>$honorarios,
            'costas'=>$costas,
            'totaldeuda'=>$totald,
            'observaciones'=>$observa
         );
         if($this->Proveedores_model->saveFormCG($data))
         {
            $nrof = array('numero' =>$formulario , );$estad="SI";
            $dataIN = array('imprimio' =>$estad , );
            $actualizar=$this->Proveedores_model->updateFormCG($nrof);
            $inhabilitaSG=$this->Proveedores_model->inhabilitaCG($dnidem,$dataIN);
            echo "<script>alert('Formulario Registrado-Facturas Inhabilitadas Correctamente');</script>";
            $ruta=base_url().'principal';
            echo "<script>window.location='".$ruta."';</script>";

         }
         else
         {
            $this->session->set_flashdata("error","No se pudo grabar-Reintente");
            redirect(base_url()."mantenimiento/proveedores/registraFormCG");

         }

    }
    /**Obtiene el listado de los formularios presentados */
    public function listFormularios()
    {
        $formularios=$this->Proveedores_model->getDeudaSG();
        $data = array('pedidos' =>$formularios , );
        $this->load->view("layouts/header");
        $this->load->view("layouts/aside");
        $this->load->view("admin/proveedores/listFormularios",$data);
        $this->load->view("layouts/footer");
        
    }
    /**formulario para generar clave para proveedores desde la mesa de entradas */
    public function genClaveProveedor()
    {
        $this->load->view("layouts/header");
        $this->load->view("layouts/aside");
        $this->load->view("admin/proveedores/generarClaveProv");
        $this->load->view("layouts/footer");

    }
    /**Genera la clave de un proveedor desde la mesa de entrada*/
    public function genClaveProve()
    {
        $nroprov=$this->input->post("nroprov");
        $clave=md5($nroprov);
        $usern="USER-".$nroprov;$estado="P";
        $user = array(
            'usuario' =>$usern ,
            'clave'=>$clave,
            'estado'=>$estado,
        );
        $bd="usuarios";
        $userecono = array(
            'usuario' =>$usern ,
            'clave'=>$clave,
        );
        $bd2="acreedoreconomia";
        if($this->Proveedores_model->updateClaves($nroprov,$user,$bd))
        {
            if($this->Proveedores_model->updateClaves($nroprov,$userecono,$bd2))
            {
                echo "<script>alert('Usuario y Clave Generada Correctamente');</script>";
            $ruta=base_url().'principal';
            echo "<script>window.location='".$ruta."';</script>";

            }
            else
            {
                $this->session->set_flashdata("error","No se pudo grabar-Reintente");
            redirect(base_url()."mantenimiento/proveedores/genClaveProveedor");
            }
        }
        else
        {
            $this->session->set_flashdata("error","No se pudo grabar-Reintente");
            redirect(base_url()."mantenimiento/proveedores/genClaveProveedor");
        }

    }
    /**Listado de Proveedores por distintos Criterios */
    public function proveeList()
    {
        $flag=0;
        $secretaria=$this->session->userdata("unidad");
        $rol=$this->session->userdata("rol");
        $bp=$this->input->post('bp');
        /**Filtro por razon social */
        if(isset($bp))
        {
            $bsprod=$this->input->post('bsprod'); $flag=1;
        }else{$bsprod="";}
        
        /**filtro por numero de proveedor */
        $bpr=$this->input->post('bpr');
        if(isset($bpr))
        {
            $nroprovd=$this->input->post('nroprovd');$nroprovh=$this->input->post('nroprovh');$flag=1;
        }else{$nroprovd=0;$nroprovh=99999;}
        if($flag==0)
        {
            $sql="SELECT * FROM acreedoreconomia where (razonsocial <> '') || (apellido <> '') ORDER BY  razonsocial ASC;";
        }
        else
        {
            if($bsprod!="")
            {
                $sql="select * from acreedoreconomia
                where (razonsocial like '%$bsprod%')
                 ORDER BY  razonsocial ASC ;";
            }
            else
            {
                $sql="select * from acreedoreconomia
                where (nroprov between '".$nroprovd."'and '".$nroprovh."') and (apellido <> '')  ORDER BY  razonsocial ASC;";  
                }
            }
    if((!isset($bp))&&(!isset($bpr)))
      {
        $this->load->view("layouts/header");
        $this->load->view("layouts/aside");
        $this->load->view("admin/proveedores/listProveHead");
        $this->load->view("layouts/footer");

      } 
      else
      {
        $data=$this->Proveedores_model->getListProvee($sql);
        if($data)
        {
           
            $datos['pedidos']=$data;
            $this->load->view("layouts/header");
            $this->load->view("layouts/aside");
            $this->load->view("admin/proveedores/listProveHead");
            $this->load->view("admin/proveedores/listProveBody",$datos);
            $this->load->view("layouts/footer");

        }
        else
        {
            $this->session->set_flashdata("error","NO EXISTEN DATOS-VERIFIQUE");
            redirect(base_url()."mantenimiento/proveedores/proveeList");

        }

      }

       
    }
    /**Obtiene los montos presentados por los proveedores Emergencia Economica */
    public function proveePresenta()
    {
        $flag=0;
        $secretaria=$this->session->userdata("unidad");
        $rol=$this->session->userdata("rol");
        $bp=$this->input->post('bp');
        /**Filtro por razon social */
        if(isset($bp))
        {
            $bsprod=$this->input->post('bsprod'); $flag=1;
        }else{$bsprod="";}
        /**Filtro por fecha de presentacion */
        $bf=$this->input->post('bf');
        if(isset($bf))
        {
            $bddesde=$this->input->post('bddesde');
            $bdhasta=$this->input->post('bdhasta');$flag=1;
        }else{$bddesde="1900-01-01";$bdhasta="9999-12-31";}
        /**filtro por numero de formulario */
        $bfp=$this->input->post('bfp');
        if(isset($bfp))
        {
            $nroformd=$this->input->post('nroformd');$nroformh=$this->input->post('nroformh');$flag=1;
        }else{$nroformd=0;$nroformh=999999;}
        /**filtro por numero de proveedor */
        $bpr=$this->input->post('bpr');
        if(isset($bpr))
        {
            $nroprovd=$this->input->post('nroprovd');$nroprovh=$this->input->post('nroprovh');$flag=1;
        }else{$nroprovd=0;$nroprovh=99999;}
        if($flag==0)
        {
            $sql="select * from resumensg order by totaldeuda desc;";
        }
        else
        {
            if($bsprod!="")
            {
                $sql="select * from resumensg where (razonsocial like '%$bsprod%') and (fechapresenta between '".$bddesde."'and'".$bdhasta."')and(nroprov between '".$nroprovd."'and '".$nroprovh."') and (nroform between '".$nroformd."'and '".$nroformh."')order by totaldeuda desc;";
            }
            else
            {
                $sql="select * from resumensg
                where (fechapresenta between '".$bddesde."'and'".$bdhasta."')and(nroprov between '".$nroprovd."'and '".$nroprovh."')
                and (nroform between '".$nroformd."'and '".$nroformh."')
                 order by totaldeuda desc;";  
                }
        }
        if((!isset($bp))&&(!isset($bf))&&(!isset($bfp))&&(!isset($bpr)))
        {
            $this->load->view("layouts/header");
            $this->load->view("layouts/aside");
            $this->load->view("admin/proveedores/proveePresentaHead");
            $this->load->view("layouts/footer");
        }
        else
        {
            $data=$this->Proveedores_model->getListProvee($sql);
            if($data)
            {
                $datos['pedidos']=$data;
                $this->load->view("layouts/header");
                $this->load->view("layouts/aside");
                $this->load->view("admin/proveedores/proveePresentaHead");
                $this->load->view("admin/proveedores/proveePresentaBody",$datos);
                $this->load->view("layouts/footer");
            }
            else
            {
                $this->session->set_flashdata("error","NO EXISTEN DATOS-VERIFIQUE");
                redirect(base_url()."mantenimiento/proveedores/proveePresenta");
            }
        }
    }
    /**Obtiene los montos no presentados por los proveedores Emergencia Economica */
    public function proveeNoPresenta()
    {
        $flag=0;
        $secretaria=$this->session->userdata("unidad");
        $rol=$this->session->userdata("rol");
        $bp=$this->input->post('bp');
        /**Filtro por razon social */
        if(isset($bp))
        {
            $bsprod=$this->input->post('bsprod'); $flag=1;
        }else{$bsprod="";}
        /**Filtro por fecha de carga */
        $bf=$this->input->post('bf');
        if(isset($bf))
        {
            $bddesde=$this->input->post('bddesde');
            $bdhasta=$this->input->post('bdhasta');$flag=1;
        }else{$bddesde="1900-01-01";$bdhasta="9999-12-31";}
        
        /**filtro por numero de proveedor */
        $bpr=$this->input->post('bpr');
        if(isset($bpr))
        {
            $nroprovd=$this->input->post('nroprovd');$nroprovh=$this->input->post('nroprovh');$flag=1;
        }else{$nroprovd=0;$nroprovh=99999;}
        $prueba=14;
        if($flag==0)
        {
            $sql="select sum(a.montofactura) as mf,sum(a.pagosparciales) as pp,sum(a.totaldeuda) as td, a.nroproveedor, ac.razonsocial, ac.apellido, a.fechacarga, ac.tel1, ac.email from acreenciasingestion a,acreedoreconomia ac where (a.nroproveedor=ac.nroprov)and a.nroproveedor <>'".$prueba."' and (a.nroproveedor not in (select r.nroprov from resumensg r))group by a.nroproveedor order by td desc;";
        }
        else
        {
            if($bsprod!="")
            {
                $sql="select sum(a.montofactura) as mf,sum(a.pagosparciales) as pp,sum(a.totaldeuda) as td, a.nroproveedor, ac.razonsocial, ac.apellido,a.fechacarga, ac.tel1, ac.email from acreenciasingestion a,                acreedoreconomia ac where (ac.razonsocial like '%$bsprod%') and(a.fechacarga between '".$bddesde."'and'".$bdhasta."')and (a.nroproveedor between '".$nroprovd."'and '".$nroprovh."') and (a.nroproveedor=ac.nroprov) and (a.nroproveedor <>'".$prueba."') and (a.nroproveedor not in (select r.nroprov from resumensg r))group by a.nroproveedor order by td desc ;";
            }
            else
            {
                $sql="select sum(a.montofactura) as mf,sum(a.pagosparciales) as pp,sum(a.totaldeuda) as td, a.nroproveedor, ac.razonsocial, ac.apellido,a.fechacarga, ac.tel1, ac.email from acreenciasingestion a,acreedoreconomia ac where (a.fechacarga between '".$bddesde."'and'".$bdhasta."')and (a.nroproveedor between '".$nroprovd."'and '".$nroprovh."')and (a.nroproveedor=ac.nroprov) and (a.nroproveedor <>'".$prueba."') and (a.nroproveedor not in (select r.nroprov from resumensg r))group by a.nroproveedor order by td desc ;";  
                }
        }
        if((!isset($bp))&&(!isset($bf))&&(!isset($bpr)))
        {
            $this->load->view("layouts/header");
            $this->load->view("layouts/aside");
            $this->load->view("admin/proveedores/proveeNoPresentaHead");
            $this->load->view("layouts/footer");
        }
        else
        {
            $data=$this->Proveedores_model->getListProvee($sql);
            if($data)
            {
                $datos['pedidos']=$data;
                $this->load->view("layouts/header");
                $this->load->view("layouts/aside");
                $this->load->view("admin/proveedores/proveeNoPresentaHead");
                $this->load->view("admin/proveedores/proveeNoPresentaBody",$datos);
                $this->load->view("layouts/footer");
            }
            else
            {
                $this->session->set_flashdata("error","NO EXISTEN DATOS-VERIFIQUE");
                redirect(base_url()."mantenimiento/proveedores/proveeNoPresenta");
            }
        }
    }
    /**Obtiene los montos presentados por los proveedores para Compensar */
    public function proveeCompensa()
    {
        $flag=0;
        $secretaria=$this->session->userdata("unidad");
        $rol=$this->session->userdata("rol");
        $bp=$this->input->post('bp');
        /**Filtro por razon social */
        if(isset($bp))
        {
            $bsprod=$this->input->post('bsprod'); $flag=1;
        }else{$bsprod="";}
        /**Filtro por fecha de presentacion */
        $bf=$this->input->post('bf');
        if(isset($bf))
        {
            $bddesde=$this->input->post('bddesde');
            $bdhasta=$this->input->post('bdhasta');$flag=1;
        }else{$bddesde="1900-01-01";$bdhasta="9999-12-31";}
        /**filtro por numero de formulario */
        $bfp=$this->input->post('bfp');
        if(isset($bfp))
        {
            $nroformd=$this->input->post('nroformd');$nroformh=$this->input->post('nroformh');$flag=1;
        }else{$nroformd=0;$nroformh=999999;}
        /**filtro por numero de proveedor */
        $bpr=$this->input->post('bpr');
        if(isset($bpr))
        {
            $nroprovd=$this->input->post('nroprovd');$nroprovh=$this->input->post('nroprovh');$flag=1;
        }else{$nroprovd=0;$nroprovh=99999;}
        if($flag==0)
        {
            $sql="select * from rescompensa order by fechapresenta desc;";
        }
        else
        {
            if($bsprod!="")
            {
                $sql="select * from rescompensa where (razonsocial like '%$bsprod%') and (fechapresenta between '".$bddesde."'and'".$bdhasta."')and (nroprov between '".$nroprovd."'and '".$nroprovh."') and (nroform between '".$nroformd."'and '".$nroformh."') order by fechapresenta desc ;";
            }
            else
            {
                $sql="select * from rescompensa
                where (fechapresenta between '".$bddesde."'and'".$bdhasta."')and (nroprov between '".$nroprovd."'and '".$nroprovh."')
                and (nroform between '".$nroformd."'and '".$nroformh."')
                 order by fechapresenta desc ;";  
                }
        }
        if((!isset($bp))&&(!isset($bf))&&(!isset($bfp))&&(!isset($bpr)))
        {
            $this->load->view("layouts/header");
            $this->load->view("layouts/aside");
            $this->load->view("admin/proveedores/proveeCompensaHead");
            $this->load->view("layouts/footer");
        }
        else
        {
            $data=$this->Proveedores_model->getListProvee($sql);
            if($data)
            {
                $datos['pedidos']=$data;
                $this->load->view("layouts/header");
                $this->load->view("layouts/aside");
                $this->load->view("admin/proveedores/proveeCompensaHead");
                $this->load->view("admin/proveedores/proveeCompensaBody",$datos);
                $this->load->view("layouts/footer");
            }
            else
            {
                $this->session->set_flashdata("error","NO EXISTEN DATOS-VERIFIQUE");
                redirect(base_url()."mantenimiento/proveedores/proveeCompensa");
            }
        }
    }
    /**Obtiene los montos presentados por los proveedores para Compensar */
    public function proveeCompensaDetalle()
    {
        $sql="select b.provcom, b.sujetop, b.tributop, b.importep, b.fechacarga, b.documento,b.nroform, b.anioform, ac.razonsocial from acreenciacompensacion b, acreedoreconomia ac where (b.provcom=ac.nroprov)order by b.provcom asc ;";
        $data=$this->Proveedores_model->getListProvee($sql);
            if($data)
            {
                $datos['pedidos']=$data;
                $this->load->view("layouts/header");
                $this->load->view("layouts/aside");
                $this->load->view("admin/proveedores/proveeCompensaDetalle",$datos);
                $this->load->view("layouts/footer");
            }
            else
            {
                $this->session->set_flashdata("error","NO EXISTEN DATOS-VERIFIQUE");
                redirect(base_url()."mantenimiento/proveedores/proveeCompensaDetalle");
            }
        
    }
    /**Obtiene los montos presentados por los proveedores para Compensar */
    public function proveeCompensaDetalleI()
    {
        $sql="select a.nroproveedor,a.intrumento,a.ordencompra,a.tipofactura,a.nrofactura,a.montofactura, a.pagosparciales,a.totaldeuda,a.imprimio,ae.nroprov,ae.razonsocial from acreenciasingestion a , acreedoreconomia ae where (a.nroproveedor=ae.nroprov) and (a.imprimio='SI') order by a.nroproveedor asc ;";
        $data=$this->Proveedores_model->getListProvee($sql);
            if($data)
            {
                $datos['pedidos']=$data;
                $this->load->view("layouts/header");
                $this->load->view("layouts/aside");
                $this->load->view("admin/proveedores/proveeCompensaDetalleI",$datos);
                $this->load->view("layouts/footer");
            }
            else
            {
                $this->session->set_flashdata("error","NO EXISTEN DATOS-VERIFIQUE");
                redirect(base_url()."mantenimiento/proveedores/proveeCompensaDetalleI");
            }
        
    }
}