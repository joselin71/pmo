<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tesoreria extends CI_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->model('Tesoreria_model');
  
    }
    public function compras()
    {
        $res=$this->Tesoreria_model->getOCompras();
        $data = array(
            'ordenes' =>$res , );
        $this->load->view("layouts/header");
        $this->load->view("layouts/aside");
        $this->load->view("admin/tesoreria/list",$data);
        $this->load->view("layouts/footer");
    }
}