<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// Incluimos el archivo fpdf
require_once APPPATH.'libraries\third_party\fpdf\fpdf.php';
require_once APPPATH.'libraries\third_party\wm\rotation.php';
require_once APPPATH.'libraries\third_party\phpqrcode\qrlib.php';
//Extendemos la clase Pdf de la clase fpdf para que herede todas sus variables y funciones
//class Pdf extends FPDF {
class Pdf extends PDF_Rotate {
public function __construct() {
parent::__construct();
}
// El encabezado del PDF
public function Header(){
$rutalogo1=base_url()."assets/template/img/logoPC.png";
 // Logo
	$this->Image($rutalogo1,20,15,170,50);
  // Salto de línea
    $this->Ln(55);
	$this->SetFont('Arial','B',15);
    $this->SetTextColor(179,255,204);
	    $this->RotatedText(25,190,'P O L I C I A  D E L  C H A C O-P O L I C I A  D E L  C H A C O',45);
	$this->RotatedText(55,190,'P O L I C I A  D E L  C H A C O-P O L I C I A  D E L  C H A C O',45);
	$this->RotatedText(85,190,'P O L I C I A  D E L  C H A C O-P O L I C I A  D E L  C H A C O',45);
}

function RotatedText($x, $y, $txt, $angle)
{
    //Text rotated around its origin
    $this->Rotate($angle,$x,$y);
    $this->Text($x,$y,$txt);
    $this->Rotate(0);
}



// El pie del pdf
public function Footer(){
 // Posición: a 1,5 cm del final
    $this->SetY(-15);
    $this->Image('C:/jr-qrcode.png',120,85,50,50);
}
}
?>