<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// Incluimos el archivo fpdf
require_once APPPATH.'libraries\third_party\fpdf\fpdf.php';
//Extendemos la clase Pdf de la clase fpdf para que herede todas sus variables y funciones
//class Pdf extends FPDF {
class fpdf_manager extends FPDF {
public function __construct() {
parent::__construct();
}
// El encabezado del PDF
public function Header(){
$rutalogo1=base_url()."assets/template/img/FVhead.jpg";
 // Logo
	$this->Image($rutalogo1,0,2,210,25);
  // Salto de línea
    $this->Ln(5);
}
}
?>