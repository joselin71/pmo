<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Catalogo_model extends CI_Model {

    /**Obtiene todos los Rubros */
    public function getRubros()
    {
        $resultado=$this->db->get("rubros");
        return $resultado->result();
    }
    /**Insertar un Rubro */
    public function addRubro($data)
    {
        return $this->db->insert("rubros",$data);
    }
    /**Obtiene los datos del rubro para modificar */
    public function getRubroxID($id)
    {
        $this->db->where("idrubro",$id);
        $resultado=$this->db->get("rubros");
        return $resultado->row();

    }
    /**Actualiza los datos del Rubro */
    public function updateRubro($data,$id)
    {
        $this->db->where("idrubro",$id);
        return $this->db->update("rubros",$data);
    }
    /**Obtiene todos los SubRubros */
    public function getSubRubros()
    {
        $this->db->select("r.idrubro,r.rubro,r.rubrodesc,sr.idsubrubro,sr.subrubro,sr.subrubdesc,sr.estado,sr.fecha_alta,sr.fecha_update");
        $this->db->from("rubros r");
        $this->db->join("subrubros sr","r.idrubro=sr.idrubro");
        $resultado=$this->db->get();
        return $resultado->result();
    }
    /**Actualiza los datos del Sub-Rubro cuando inactiva el rubro */
    public function updateSubRubro($data,$id)
    {
        $this->db->where("idsubrubro",$id);
        return $this->db->update("subrubros",$data);
    }
    /**Insertar un SubRubro */
    public function addSRubro($data)
    {
        return $this->db->insert("subrubros",$data);
    }
    /**Obtiene los datos del subrubro para modificar */
    public function getSubRubroxID($id)
    {
        $this->db->where("idsubrubro",$id);
        $resultado=$this->db->get("subrubros");
        return $resultado->row();

    }
    /**Obtiene el listado de todos los depositos disponibles */
    public function getDepositos()
    {
        $resultados=$this->db->get("depositos");
        return $resultados->result();
    }
    /**Inserta un nuevo Deposito */
    public function addDeposito($data)
    {
        return $this->db->insert("depositos",$data);
    }
    /**Obtiene los datos de un Deposito en Particular */
    public function getDepositoxID($id)
    {
        $this->db->where("id",$id);
        $resultado=$this->db->get("depositos");
        return $resultado->row();
    }
    /**Actualiza la descripcion y el estado del deposito */
    public function updateDeposito($data,$id)
    {
        $this->db->where("id",$id);
        return $this->db->update("depositos",$data);
    }
    /**Obtiene las unidades de medida de los articulos del catalogo */
    public function getUndMedida()
    {
        $resultado=$this->db->get("unidadmedida");
        return $resultado->result();
    }
    /**Inserta una nueva unidad de medida */
    public function addUndMedida($data)
    {
        return $this->db->insert("unidadmedida",$data);
    }
    /**Obtiene los datos de una unidad de medida en Particular */
    public function getUndMedidaxID($id)
    {
        $this->db->where("id",$id);
        $resultado=$this->db->get("unidadmedida");
        return $resultado->row();
    }
    /**Actualiza la descripcion y el estado de la unidad de medida */
    public function updateUndMedida($data,$id)
    {
        $this->db->where("id",$id);
        return $this->db->update("unidadmedida",$data);
    }
    /**Obtiene un listado de Proveedores de Deposito */
    public function getProveedores()
    {
        $resultado=$this->db->get("proveedores");
        return $resultado->result();
    }
    /**Inserta un nuevo Proveedor */
    public function addProveedor($data)
    {
        return $this->db->insert("proveedores",$data);
    }
    /**Obtiene los datos de un proveedor particular segun su id */
    public function getProveedorxID($id)
    {
        $this->db->where("id",$id);
        $resultado=$this->db->get("proveedores");
        return $resultado->row();

    }
    /**Actualiza los datos del Proveedor */
    public function updateProveedor($data,$id)
    {
        $this->db->where("id",$id);
        return $this->db->update("proveedores",$data);
    }
    /**Obtiene el listado de los rubros con su correspondiente subrubro habilitados-Estado A */
    public function getEstructura()
    {
        $resultado=$this->db->query("select r.idrubro,r.rubro,r.rubrodesc,r.estado,sr.id,sr.subrubro,sr.subrubdesc,sr.estado
        from rubro_catalogo r inner join subrubro_catalogo sr on r.idrubro=sr.fk_rubro_id
        where r.estado='A' and sr.estado='A'");
        return $resultado->result();
    }
    /**Obtiene el listado de todos los articulos */
    public function getArticulos()
    {
        $resultado=$this->db->query("select c.id,c.descripcion as descart,c.estado,c.fechaalta,c.fechavto,c.fecha_baja,c.inventariable,c.minimo,c.maximo,c.precio,r.rubro,r.rubrodesc,sr.subrubro,sr.subrubdesc,un.cod,un.descripcion,un.valor
        from catalogos c inner join rubro_catalogo r on c.rubro_id=r.idrubro inner join subrubro_catalogo sr on c.subrubros_id=sr.id
        inner join unidadmedida un on c.unidadmedida_id=un.id
        ");
        return $resultado->result();
    }
    /**Obtiene los datos de un articulo por id */
    public function getArticuloxID($id)
    {
        $resultado=$this->db->query("select c.id,c.descripcion as descart,c.estado,c.fechaalta,c.fechavto,c.fecha_baja,c.inventariable,c.minimo,c.maximo,c.precio,r.rubro,r.rubrodesc,sr.subrubro,sr.subrubdesc,un.cod,un.descripcion,un.valor from catalogos c inner join rubro_catalogo r on c.rubro_id=r.idrubro inner join subrubro_catalogo sr on c.subrubros_id=sr.id inner join unidadmedida un on c.unidadmedida_id=un.id where c.id='".$id."'");
        return $resultado->row();
    }
    /**Obtiene el listado de rubros de articulos */
    public function getRubro()
    {
        $this->db->where("estado","A");
        $resultado=$this->db->get("rubro_catalogo");
        return $resultado->result();
    }
    /**Obtiene listado de subrubros de articulos */
    public function getSubRubro()
    {
        $resultado=$this->db->query("select sr.*,r.rubro
        from subrubro_catalogo sr inner join rubro_catalogo r on sr.fk_rubro_id=r.idrubro where sr.estado='A'");
       
        return $resultado->result();
    }
    /**Lista para solucion de paso */
    public function getIDRubro($rubro,$srubro)
    {
        $resultado=$this->db->query("select r.idrubro,sr.id
        from rubro_catalogo r , subrubro_catalogo sr 
        where  r.rubro='".$rubro."' and sr.subrubro='".$srubro."'");
        return $resultado->row();
    }
    /**Inserta un nuevo articulo */
    public function addArticulo($data)
    {
        return $this->db->insert("catalogos",$data);
    }
    /**Verifico que el articulo ya no tenga cargada esa unidad de medida */
    public function getVerifico($undmed,$art)
    {
        $this->db->where("unidadmedida_id",$undmed);
        $this->db->where("articulos_id",$art);
        $resultado=$this->db->get("unidadxarticulo");
        return $resultado->row();
    }
    /**Inserta un empaque a un articulo siempre y cuando no tenga ese empaque */
    public function saveEmpaque($data)
    {
        return $this->db->insert("unidadxarticulo",$data);
    }
    /**Inserta un nuevo EAN a un articulo */
    public function saveEAN($data)
    {
        return $this->db->insert("codigos",$data);
    }
    /**Obtiene los datos de un articulo por id */
    public function getEANxID($id)
    {
        $resultado=$this->db->query("select c.id, p.nroProv,p.razonsocial,cd.ean from catalogos c inner join codigos cd on cd.articulos_id=c.id inner join proveedores p on cd.proveedores_id=p.id where c.id='".$id."'");
        return $resultado->result();
    }
    /**Obtiene el listado de todos los articulosInventariables */
    public function getArticulosInv()
    {
        $resultado=$this->db->query("select c.id,c.descripcion as descart,c.estado,c.fechaalta,c.fechavto,c.fecha_baja,c.inventariable,c.minimo,c.maximo,c.precio,r.rubro,r.rubrodesc,sr.subrubro,sr.subrubdesc,un.cod,un.descripcion,un.valor
        from catalogos c inner join rubro_catalogo r on c.rubro_id=r.idrubro inner join subrubro_catalogo sr on c.subrubros_id=sr.id
        inner join unidadmedida un on c.unidadmedida_id=un.id
        where c.inventariable='S'");
        return $resultado->result();
    }
    /**Inserta un nuevo codigo de inventario a un articulo */
    public function saveInventario($data)
    {
        return $this->db->insert("inventariable",$data);
    }
    /**Obtiene los datos de una articulo con inventario cargado */
    public function getInventarioxID($id)
    {
        $resultado=$this->db->query("select c.id, p.nroinventario,p.oficina from catalogos c inner join inventariable p on p.catalogos_id=c.id where c.id='".$id."'");
        return $resultado->result();
    }
}