<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Materiales_model extends CI_Model {

    /** Obtiene lista de pedidos grabados segun nivel de Usuarios
     * Nivel 2:Operadores,Nivel 3:Compras,Nivel 11:Secretarios,Nivel 16:Economia,Nivel 99:Administrador
     */
    public function getPedidos($secretaria,$rol)
    {
        $this->db->select("p.*,s.detsec as nomsec");
        $this->db->from("pedidomateriales p");
        $this->db->join("secretarias s","p.isecre=s.sec");
        if(($rol==2)||($rol==3)||($rol==11))
        {
         $this->db->where("s.sec",$secretaria);
         $this->db->where("(p.estado='CARGADO' OR estado='OBSERVADO')");
        }
        if(($rol==15)||($rol==16)||($rol==99))
        {
         $this->db->where("(p.estado='VISADO' OR estado='INGRESADO')");
        }
        $anio=date("Y");
        $this->db->where("p.aniopedido",$anio);
        $this->db->order_by("p.aniopedido","desc");
        $this->db->order_by("p.nropedido","desc");
        $resultado=$this->db->get();
        return $resultado->result();
      
        
    }
    /**Obtiene las lineas de un pedido por usuario/secretaria sin cabecera */
    public function getPedidoDetalle($secretaria,$dniuser)
    {
        $anio=date("Y");
        $this->db->where("idpedido",0);
        $this->db->where("idsol",$secretaria);
        $this->db->where("aniopedido",$anio);
        $this->db->where("dniuser",$dniuser);
        $this->db->order_by("iddetallepm","asc");
        $resultado=$this->db->get("detallepedidomateriales");
        return $resultado->result();        
    }
    /**Obtiene el detalle de un pedido registrado para editar */
    public function getPedidoxID($id)
    {
        $this->db->select("p.idpedidomateriales,p.nropedido,p.aniopedido,p.totalletra,p.isecre,p.destinomat,dp.iddetallepm,dp.cantidad,dp.importelinea,dp.detallepedido,dp.importedetalle");
        $this->db->from("pedidomateriales p");
        $this->db->join("detallepedidomateriales dp"," p.idpedidomateriales=dp.idaleatorio");
        $this->db->where("p.idpedidomateriales",$id);
        $resultado=$this->db->get();
        return $resultado->result();
    }
    /**Obtiene la estructura Secretaria-Subsecretaria-Direccion General
     * segun la secretaria asignada al usuario.
     */
    public function getDependencias($secretaria)
    {
        $this->db->select("s.sec,s.detsec,ss.subsec,ss.detsubsec,dg.dirgral,dg.dirdetalle");
        $this->db->from("secretarias s");
        $this->db->join("subsecretarias ss","s.sec=ss.isec");
        $this->db->join("dirgenerales dg","ss.subsec=dg.issec");
        $this->db->where("s.sec",$secretaria);
        $resultado=$this->db->get();
        return $resultado->result();
    }
    public function getSecretarias()
    {   
        $this->db->where("sec <",20);
        $resultado=$this->db->get("secretarias");
        return $resultado->result();
    }
    /**Obtiene los programas correspondientes a la Secretaria */
    public function getProgramas($secretaria)
    {
        $this->db->select("distinct(pg.id),pg.id,pg.nroprg,pg.descripcion,pg.idsec,spg.nrosbprg,spg.descripcionsp");
        $this->db->from("programa pg");
        $this->db->join("subprograma spg","pg.nroprg=spg.idpgr");
        $this->db->where("pg.idsec",$secretaria);
        $this->db->where("pg.estado","A");
        $resultado=$this->db->get();
        return $resultado->result();
    }
    /**Obtiene el listado de rubros de articulos */
    public function getRubros()
    {
        $resultado=$this->db->get("rubros");
        return $resultado->result();
    }
    /**Obtiene listado de subrubros de articulos */
    public function getSubRubros()
    {
        $resultado=$this->db->get("subrubros");
        return $resultado->result();
    }
    /**Inserta 1 registro en detalle de pedido */
    public function save($data)
    {
        return $this->db->insert("detallepedidomateriales",$data);

    }
    /**Inserta 1 registro en la cabecera del pedido */
    public function saveCab($data)
    {
        return $this->db->insert("pedidomateriales",$data);

    }
    /**Actualiza la cabecera de un pedido que se edita */
    public function saveCabEd($data,$id)
    {
        $this->db->where("idpedidomateriales",$id);
        return $this->db->update("pedidomateriales",$data);

    }
    /**Obtiene el numero de pedido correlativo por año, si es el primero del año pasa a 1 */
    public function getNpedido()
    {
        $this->db->select("nroped, anioped");
        $this->db->order_by("anioped","desc");
        $this->db->order_by("nroped","desc");
        $resultado=$this->db->get("nropedmaterial");
        return $resultado->row();
    }
    /**Obtiene el total valorizado del pedido */
    public function getbuscarTP($dniuser,$secretaria,$aniopedido)
    {
        $this->db->select("sum(importedetalle) as total");
        $this->db->where("idpedido",0);
        $this->db->where("aniopedido",$aniopedido);
        $this->db->where("idsol",$secretaria);
        $this->db->where("dniuser",$dniuser);
        $resultado=$this->db->get("detallepedidomateriales");
        return $resultado->row();
    }
    /**Obtiene el total de un pedido grabado que se edito */
    public function getbuscarTPEd($id)
    {
        $this->db->select("sum(importedetalle) as total");
        $this->db->where("idaleatorio",$id);
        $resultado=$this->db->get("detallepedidomateriales");
        return $resultado->row();
    }
    /**Obtiene el id del ultimo registro insertado en la cabecera para relacionar con el detalle del pedido */

    public function getLastID($dniuser)
    {
        $this->db->where("idsolicitante",$dniuser);
        $this->db->order_by("idpedidomateriales","desc");
        $this->db->limit(1);
        $resultado=$this->db->get("pedidomateriales");
        return $resultado->row();
    }
    /**Actualiza la numeracion de los pedidos por numero/año */
    public function actualizarSecuencia($dataSecuencia)
    {
        return $this->db->update("nropedmaterial",$dataSecuencia);
    }
    /**Inserta 1 registro en la tabla de compras para realizar el seguimiento de los pedidos */
    public function actualizarCompras($data)
    {
        return $this->db->insert("gestioncompra",$data);
    }
    /**Actualiza 1 registro en la tabla de compras para realizar el seguimiento de los pedidos */
    public function actualizarComprasEd($data,$id)
    {
        $this->db->where("idgc",$id);
        return $this->db->update("gestioncompra",$data);
    }
    /**Actualiza el enlace con el pedido cuando se graba la cabecera */
    public function actualizarAleatorio($aniopedido,$idsecre,$dniuser,$dataAleatorio)
    {
        $this->db->where("idsol",$idsecre);
        $this->db->where("idpedido",0);
        $this->db->where("aniopedido",$aniopedido);
        $this->db->where("dniuser",$dniuser);
        return $this->db->update("detallepedidomateriales",$dataAleatorio);
    }
    /**Obtiene los datos de 1 detalle segun el id pasado como parametro */
    public function getLinea($id)
    {
        $this->db->where("iddetallepm",$id);
        $resultados=$this->db->get("detallepedidomateriales");
        return $resultados->row();
    }
    /**Actualiza 1 registro de detalle de pedido */
    public function edsave($data,$id)
    {
        $this->db->where("iddetallepm",$id);
        return $this->db->update("detallepedidomateriales",$data);

    }
    /**Elimina un articulo del detalle de pedido. */
    public function deleteArt($id)
    {
        $this->db->where("iddetallepm",$id);
        return $this->db->delete("detallepedidomateriales");
    }
    public function getPedidoCabecera($id)
    {
        $resultado=$this->db->query("select p.idpedidomateriales,p.nropedido,p.aniopedido,p.cobertura,p.totalped,p.totalletra,p.destinomat,p.cuenta,s.detsec,ss.detsubsec,dg.dirdetalle,us.apynom from pedidomateriales p 
        join secretarias s on p.isecre=s.sec join subsecretarias ss on s.sec=ss.isec join dirgenerales dg on p.idg=dg.dirgral join usersistema us on p.idsolicitante=us.dni 
        where p.idpedidomateriales=".$id." and p.isubsecre=ss.subsec and p.idg=dg.dirgral ");
        return $resultado->row();

    }
    public function getPedidoPrograma($id)
    {
        $resultado=$this->db->query("select p.idpedidomateriales,pg.descripcion,sbpg.descripcionsp from pedidomateriales p 
        join programa pg on p.idprg=pg.nroprg join subprograma sbpg on p.idsbprg=nrosbprg where p.idpedidomateriales=".$id." and p.isecre=pg.idsec ");
        return $resultado->row();
    }
    public function getPedidoDetallado($id)
    {
        $resultado=$this->db->query("select * from detallepedidomateriales where idaleatorio=".$id." ;");
        return $resultado->result();
    }
    /**Obtiene cabecera de un pedido para cambiar el estado */

    public function getPedidoAuthxID($id)
    {
        $this->db->select("idpedidomateriales,nropedido,aniopedido,destinomat,totalped,actuacion");
        $this->db->where("idpedidomateriales",$id);
        $resultado=$this->db->get("pedidomateriales");
        return $resultado->row();
    }
    /**Actualiza el estado del pedido */
    public function updateAuthPed($id,$data)
    {
        $this->db->where("idpedidomateriales",$id);
        return $this->db->update("pedidomateriales",$data);

    }
    /**Obtiene un listado de las secretarias disponibles para pedidos de cualquier secretaria. */
    public function getPedidosCentral($rol)
    {
        $this->db->select("p.*,s.detsec as nomsec");
        $this->db->from("pedidomateriales p");
        $this->db->join("secretarias s","p.isecre=s.sec");
        $this->db->where("(p.estado='CARGADO' OR estado='OBSERVADO')");
        $anio=date("Y");
        $this->db->where("p.aniopedido",$anio);
        $this->db->order_by("p.aniopedido","desc");
        $this->db->order_by("p.nropedido","desc");
        $resultado=$this->db->get();
        return $resultado->result();
      
        
    }
    /**Obtiene un listado con los pedidos grabados */
    public function getGestionCompras()
    {
        $anio=date("Y");
        $resultado=$this->db->query("select a.idpedidomateriales, a.isecre, a.aniopedido, a.nropedido, a.totalped, b.pedmat, b.aniooc, b.nrooc, b.asignado, b.fecoc, b.proveedor, b.actuacions, b.fecas, b.nropv from pedidomateriales a, gestioncompra b where (a.idpedidomateriales=b.aleatorio) and (a.aniopedido=".$anio.") order by a.idpedidomateriales desc ");
        return $resultado->result();
    }
    /**Obtiene un pedido grabado para completar datos por parte de la Direccion de Compras */
    public function getGestionComprasxID($id)
    {
        $this->db->where("idpedidomateriales",$id);
        $resultado=$this->db->get("pedidomateriales");
        return $resultado->row();
    }
    /**Actualiza los datos del pedido con la informacion de compras */
    public function updateGestionCompras($id,$data)
    {
        $this->db->where("aleatorio",$id);
        return $this->db->update("gestioncompra",$data);

    }
    /**Actualiza el Pedido de Materiales con la info que carga compras*/
    public function updateCompletaOC($id,$dataG)
    {
        $this->db->where("idpedidomateriales",$id);
        return $this->db->update("pedidomateriales",$dataG);

    }
    /** */
    public function getQuestionDetalle($estado,$rol,$secretaria)
    {
        $anp=date("Y");
        if($rol < 16 )
        {
            $this->db->where("isecre",$secretaria);
        }
        $this->db->where("estado",$estado);
        $this->db->where("aniopedido",$anp);
        $this->db->order_by("idpedidomateriales","desc");
        $resultado=$this->db->get("pedidomateriales");
        return $resultado->result();
    }
    /**Obtiene los programas correspondientes a las Secretarias */
    public function getProgramasAll()
    {
        $this->db->join("secretarias","programa.idsec=secretarias.sec");
        $this->db->where("estado","A");
        $this->db->order_by("programa.idsec","asc");
        $this->db->order_by("programa.nroprg","asc");
        $resultado=$this->db->get("programa");
        return $resultado->result();
    }
    /**Consulta que no exista el Programa */
    public function getConsultaPrograma($nroprg,$secretaria)
    {
        $this->db->where("nroprg",$nroprg);
        $this->db->where("idsec",$secretaria);
        $this->db->where("estado","A");
        $resultado=$this->db->get("programa");
        return $resultado->row();
    }
    /**Genera el alta de un programa */
    public function addProgramas($data)
    {
        return $this->db->insert("programa",$data);
    }

    /**Genera el alta de un subprograma */
    public function addSProgramas($dataspg)
    {
        return $this->db->insert("subprograma",$dataspg);
    }
    /**Borrar un programa-pasar a estado inactivo */
    public function deleteProgramas($id,$data)
    {
        $this->db->where("id",$id);
        return $this->db->update("programa",$data);
    }
    /**Obtiene el detalle de un programa para modificar */
    public function getLineaPrograma($id)
    {
        $this->db->where("id",$id);
        $resultado=$this->db->get("programa");
        return $resultado->row();
    }
    /**Actualiza los programas */
    public function actualizaProg($id,$data)
    {
        $this->db->where("id",$id);
        return $this->db->update("programa",$data);
    }

    /**Obtiene los subprogramas correspondientes a las Secretarias */
    public function getSProgramasAll()
    {
        /*$this->db->select("sp.id,sp.nrosbprg,sp.descripcionsp,sp.idpgr,sp.idsbsec,p.id,p.nroprg,p.descripcion,p.idsec,s.sec,s.detsec");
        $this->db->from("subprograma sp");
        $this->db->join("programa p","sp.idpgr=p.nroprg");
        $this->db->join("secretarias s","sp.idsbsec=s.sec");
        $this->db->where("sp.idsbsec","p.idsec");
        $this->db->where("p.estado","A");
        $this->db->order_by("p.idsec","asc");
        $this->db->order_by("p.nroprg","asc");
        $this->db->order_by("sp.nrosbprg","asc");
        $resultado=$this->db->get();*/
        $resultado=$this->db->query("SELECT sp.id as idsp,sp.nrosbprg,sp.descripcionsp,sp.idpgr,sp.idsbsec,p.id,p.nroprg,p.descripcion,p.idsec,s.sec,s.detsec FROM subprograma sp join programa p
        on sp.idpgr=p.nroprg  join secretarias s on sp.idsbsec=s.sec
        where sp.idsbsec=p.idsec and p.estado='A'
        order by p.idsec asc,p.nroprg asc,sp.nrosbprg asc");
        return $resultado->result();
    }
    /**Obtiene el detalle del seguimiento de los pedidos */
    public function getViewSeguimiento($sql,$secretaria)
    {
        $resultado=$this->db->query($sql);
        return $resultado->result();
    }
    /**Obtiene datos del pedido a migrar */
    public function getPedidoaMigrar($nroped,$anio,$dniusuario)
    {
        $resultado=$this->db->query("SELECT * FROM pedidomateriales  p
         where (p.nropedido=".$nroped.")and(p.aniopedido=".$anio.") and (p.idsolicitante=".$dniusuario.")");
        return $resultado->row();
    }
    /**Obtiene detalle del pedido a Migrar */
    public function getPedidoaMigrarDetalle($id)
    {
        $this->db->where("idaleatorio",$id);
        $resultado=$this->db->get("detallepedidomateriales");
        return $resultado->result();
    }

}