<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Proveedores_model extends CI_Model {

    /**Obtiene el listado de todos los proveedores */
    public function getProveedores()
    {
        return $this->db->get("acreedoreconomia")->result();
    }
    /**Obtiene datos del proveedor para saber si existe o no */
    public function getProveedorxNro($nroprov)
    {
        $this->db->where("nroprov",$nroprov);
        $resultado=$this->db->get("acreedoreconomia");
        return $resultado->result();
    }
    /**Agrega Proveedor */
    public function addProveedor($data)
    {
        return $this->db->insert("acreedoreconomia",$data);
    }
    /**Actualiza las claves de un proveedor */
    public function addClaveProvedor($dataG)
    {
        return $this->db->insert("usuarios",$dataG);

    }
    /**Obtiene el ultimo numero de formulario de proveedores sin gestion judicial */
    public function getNroFormSG()
    {
        $resultado=$this->db->query("SELECT numero FROM nroform order by numero desc limit 1");
        return $resultado->row();
    }
    /**Ingresa un formulario de reclamo de proveedor sin gestion judicial */
    public function saveFormSG($data)
    {
        return $this->db->insert("resumensg",$data);
    }
    /**Actualiza la numeracion de los formularios */
    public function updateFormSG($nrof)
    {
        return $this->db->update("nroform",$nrof);
    }
    /**Inhabilitar comprobantes presentados */
    public function inhabilitaSG($nroprov,$dataIN)
    {
        $this->db->where("nroproveedor",$nroprov);
        return $this->db->update("acreenciasingestion",$dataIN);
    }
    /**Obtiene el ultimo numero de formulario de proveedores con gestion judicial */
    public function getNroFormCG()
    {
        $resultado=$this->db->query("SELECT numero FROM nroformcg order by numero desc limit 1");
        return $resultado->row();
    }
    /**Ingresa un formulario de reclamo de proveedor con gestion judicial */
    public function saveFormCG($data)
    {
        return $this->db->insert("resumencg",$data);
    }
    /**Actualiza la numeracion de los formularios con gestion judicial*/
    public function updateFormCG($nrof)
    {
        return $this->db->update("nroformcg",$nrof);
    }
    /**Inhabilitar comprobantes presentados con gestion judicial*/
    public function inhabilitaCG($nroprov,$dataIN)
    {
        $this->db->where("dnidem",$nroprov);
        return $this->db->update("acreenciacongestion",$dataIN);
    }
    /**Obtiene los formularios registrados sin gestion judicial */
    public function getDeudaSG()
    {
        $this->db->order_by("idresumensg","desc");
        $resultado=$this->db->get("resumensg");
        return $resultado->result();
    }
    /**Genera las claves para los proveedores y actualiza las tablas de usuarios */
    public function updateClaves($nroprov,$data,$bd)
    {
        $this->db->where("nroprov",$nroprov);
        return $this->db->update($bd,$data);
    }
    /**Obtiene el listado de proveedores por distintos criterios */
    public function getListProvee($data)
    {
        $resultado=$this->db->query($data);
        return $resultado->result();
    }
}