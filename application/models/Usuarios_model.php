<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuarios_model extends CI_Model {
    //logueo a bd sececonomiarcia

	public function login($username,$password)
	{
        $this->db->select("u.*,s.detsec as nomsec");
        $this->db->from("usersistema u");
        $this->db->join("secretarias s","u.secretaria=s.sec");
        $this->db->where("u.dni",$username);
        $this->db->where("u.clave",md5($password));
        $this->db->where("u.estado","A");
        $resultados=$this->db->get();
        if($resultados->num_rows() > 0){
            return $resultados->row();
        }
        else{
            return false;
        }
	
    }
    /**Obtiene los usuarios activos */
    public function getUsuarios()
    {
        $resultados=$this->db->get("usersistema");
        return $resultados->result();
    }
    /**Obtiene el listado de secretarias para el alta de los usuarios */
    public function getSecretarias()
    {   
        $this->db->where("sec <",20);
        $resultado=$this->db->get("secretarias");
        return $resultado->result();
    }
    /**Consulta si existe el usuario para no darlo de alta nuevamente */
    public function getUsuarioxDNI($dni)
    {
        $this->db->where("dni",$dni);
        $resultado=$this->db->get("usersistema");
        return $resultado->row();
    }
    /**inserta un nuevo usuario */
    public function saveUsuarios($data)
    {
        return $this->db->insert("usersistema",$data);
    }
    /**Obtiene usuario x id */
    public function getUsuarioxID($id)
    {
        $this->db->where("idusersistema",$id);
        $resultado=$this->db->get("usersistema");
        return $resultado->row();
    }
    /**Actualiza los datos del usuario menos el dni, si necesita cambiar el dni se da de baja y se carga de nuevo */
    public function saveEdUsuarios($id,$data)
    {
        $this->db->where("idusersistema",$id);
        return $this->db->update("usersistema",$data);
    }
    /**Baja logica de un usuario */
    public function deleteUsuario($id,$data)
    {
        $this->db->where("idusersistema",$id);
        return $this->db->update("usersistema",$data);
        
    }
    /**Blanquea la clave del usuario a 12345 */
    public function blanqueaUsuario($id,$data)
    {
        $this->db->where("idusersistema",$id);
        return $this->db->update("usersistema",$data);
    }
    /**Obtiene la clave del usuario antes de modificarla */
    public function getClave($dni,$clave)
    {
        $this->db->where("dni",$dni);
        $this->db->where("clave",$clave);
        return $this->db->get("usersistema")->row();
    }
    /**Actualiza la clave */
    public function updateClave($dni,$clave)
    {
        $this->db->where("dni",$dni);
        return $this->db->update("usersistema",$clave);
    }
    
}
