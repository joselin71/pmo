<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<section class="content">
<div class="box box-solid">
   <div class="box-body">
    <div style="float:left;">
     <p class="font-weight-bold">
     <img id="" src="<?php echo base_url();?>assets/template/img/LCRe.png" alt="LCRe"/>
    </div>
    <div><h2 style="text-align:center"><b>API PLATAFORMA P.M.O RESISTENCIA</b></h2></p>
    </div>
  </div>
</div>
<!--/section>
 <!-- Content Header (Page header) -->
 <!-- Main content -->
 <!--section class="content"-->
 <!-- Default box -->
  <div class="box box-solid">
    <div class="box-body">   

     <div class="row">
     
       <div class="col-md-12"><h4><b>Uso de la API REST para la plataforma.</b></h4>
       <p>Las respuestas se podran dar en formatos diferentes:</p>
		<ul>
			<li>JSON - útil para JavaScript e incrementalmente en aplicaciones PHP y BigData.</li>
			<li>XML - casi cualquier lenguaje de programación puede leer xml.-NO DISPONIBLE AUN</li>
			<li>CSV - se abre con programas de hojas de cálculo Excel.-NO DISPONIBLE AUN</li>
			<li>HTML - una tabla simple de HTML.-NO DISPONIBLE AUN</li>
		</ul>
        <p>Métodos HTTP</p>
		<u>
			<li>GET: Para consultar y leer recursos.</li>
			<li>POST: Para crear recurso.-NO DISPONIBLE AUN</li>
			<li>PUT: Para editar recursos.-NO DISPONIBLE AUN</li>
			<li>DELETE: Para eliminar recursos.-NO DISPONIBLE AUN</li>
		</u>
		<p>Ejemplo de URL como recurso:</p>
		<code>../mantenimiento/apirest/apiCatalogo</code>
		<p>Luego de la palabra "/apirest/" sigue "apiCatalogo" este es el recurso que se va a traer desde la base de datos.</p>
		<p>Para indicar el formato se debe llamar a la URL en el navegador de la siguiente manera:</p>
        <code>../mantenimiento/apirest/apiCatalogo/format/csv</code>
		<p>Como se observa se agrega la /format/csv donde CSV es el formato para Excel, pero se puede usar cualquiera de los formatos antes mencionado.</p>
		<br>
		<p><b>ACCEDER A LOS RECURSOS:(en desarrollo)</b></p>
		<ul>
			<li><p>Pedir y obtener el listado de <b>Articulos</b>:</p></li>
			<code>../mantenimiento/apirest/apiCatalogo</code>
            <p>Respuesta predeterminada en Json:</p>
			<code>[{"id":"2","descart":"BIROME AZUL TRAZO FINO","estado":"A","fechaalta":"2021-03-30","fechavto":"1900-01-01","fecha_baja":"1900-01-01","inventariable":"N","minimo":"0.00","maximo":"0.00","precio":"45.00","rubro":"5800","rubrodesc":"LIBRERIA-PAPELERIA-UTILES OFICINA","subrubro":"5870","subrubdesc":"UTILES DE OFICINA EN GRAL.","cod":"UND","descripcion":"UNIDAD","valor":"1"},{"id":"3","descart":"PAVA ACERO INOXIDABLE 2 LTS.","estado":"A","fechaalta":"2021-05-04","fechavto":"1900-01-01","fecha_baja":"1900-01-01","inventariable":"S","minimo":"0.00","maximo":"0.00","precio":"800.60","rubro":"1200","rubrodesc":"ARTICULOS DE BAZAR Y MENAJES","subrubro":"1214","subrubdesc":"ACERO INOXIDABLE","cod":"UND","descripcion":"UNIDAD","valor":"1"}]</code>
			<p>Pedir un Articulos por <b>ID</b>:</p>
			<code>../mantenimiento/apirest/apiCatalogoID/id/{idArticulo}</code>
			<p>Donde <b>{idArticulo}</b> es el numero entero del id del Articulo.</p>
			
		</ul>
		
       </div>
     </div>
    </div>
    <!-- /.box-body -->
   </div>
  <!-- /.box -->
 </section>
 <!-- /.content -->
 </div>
<!-- /.content-wrapper -->