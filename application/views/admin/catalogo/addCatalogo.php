<script type="text/javascript">
    function cargarsub(idrubro){
    var select = document.getElementById(idrubro);
    var seleccionado = select.options[select.selectedIndex].value; 
    var selectEst = document.getElementById("subrubro");
    selectEst.disabled=false;
    selectEst.value=0;
   for (var i = 0; i < selectEst.length; i++) {
      var opt = selectEst[i];
        if (seleccionado == opt.id)
     {
     document.getElementById("subrubro").options[i].style.display ="";
      }else{
    document.getElementById("subrubro").options[i].style.display ="none";
  }
}
} 
</script>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
 <!-- Content Header (Page header) -->
 <section class="content-header">
 <h1>Alta de un nuevo Articulo</h1>
 </section>
 <!-- Main content -->
 <section class="content">
 <!-- Default box -->
  <div class="box box-solid">
    <div class="box-body">
     <div class="row">
     <div class="col-md-12">
       <?php if($this->session->flashdata("error")):?>
        <div class="alert alert-danger">
          <p><?php echo $this->session->flashdata("error")?></p>
        </div>
    </div></div>
       <?php endif ?>
    <div class="class row">
    <div class="class col-md-8">
    <label for="cod">RUBRO</label>
    <form id="formulario" action="<?php echo base_url();?>mantenimiento/catalogo/storecatalogo" method="POST">
    <?php if(!empty($rubro)):?>
      <select class="form-control"required="required" name="rubro" id="rubro" size=1 onchange="cargarsub(id)">
      <option value="">Seleccione</option>
      <?php foreach($rubro as $ped):?>
      <option value="<?php echo $ped->rubro;?>"><?php echo $ped->rubrodesc;?></option>
      <?php endforeach;?>
    <?php endif;?>
      </select>
      
    </div>
    </div>
    <div class="class row">
    <div class="class col-md-8">
    <label for="cod">SUB-RUBRO</label>
     <?php if(!empty($subrubro)):?>
      <select class="form-control" name="subrubro" size=1 id="subrubro"  required="required" disabled>
      <option value="">Seleccione</option>
      <?php foreach($subrubro as $ped):?>
      <option id="<?php echo $ped->rubro?>" value="<?php echo $ped->subrubro;?>"><?php echo $ped->subrubdesc;?></option>
      <?php endforeach;?>
    <?php endif;?>
      </select>
      </span>
    </div>
    </div>
    
    <div class="row">
      <div class="col-md-4">
       <label for="rubro">Nombre</label>
       <input type="text" class="form-control" name="descart" id="descart"  onkeyup="javascript:this.value=this.value.toUpperCase();" maxlength="300" required="required" placeholder="Nombre del Articulo">
       </div>
       <div class="col-md-2">
       <label for="rubro">Vencimiento(si corresponde)</label>
       <input type="date" class="form-control" name="fecvto" id="fecvto"   required="required" value="1900-01-01"> 
       </div> 
       <div class="col-md-2">
       <label for="">Inventariable</label>
       <select class="form-control" name="inventario" size=1 required="required">
       <option value=''>Seleccione</option>
       <option value='S'>SI</option>   
       <option value='N'>NO</option></select>
      </div>
       </div>
       <div class="row"> 
       <div class="col-md-2">
       <label for="">Medida Principal</label>
       <select class="form-control" name="undmedida" size=1 required="required">
       <?php if(!empty($undmedida)):?>
       <option value=''>Seleccione</option>
       <?php foreach($undmedida as $ped):?>
      <option value="<?php echo $ped->id;?>"><?php echo $ped->cod."-".$ped->valor;?></option>
      <?php endforeach;?>
    <?php endif;?></select>
      </div>
      <div class="col-md-2">
       <label for="">Minimo(con 2 decimales)</label>
       <input type="text" class="form-control" name="minval" id="minval" maxlength="10" onkeypress="return numeros(event)" pattern="[0-9]{1,10}[.][0-9]{2}" required="required" placeholder="Si no lleva colocar cero (0)">
      </div>    
      <div class="col-md-2">
       <label for="">Maximo(con 2 decimales)</label>
       <input type="text" class="form-control" name="maxval" id="maxval" maxlength="10" onkeypress="return numeros(event)" pattern="[0-9]{1,10}[.][0-9]{2}" required="required" placeholder="Si no lleva colocar cero (0)">
      </div>    
      <div class="col-md-2">
       <label for="">Precio(con 2 decimales)</label>
       <input type="text" class="form-control" name="pvval" id="pvval" maxlength="10" onkeypress="return numeros(event)" required="required" placeholder="Solo numeros" pattern="[0-9]{1,10}[.][0-9]{2}">
      </div>     
    </div><hr>
    <div class="row">
      <div class="col-md-4">
         <button type="submit" class="btn btn-block btn-success btn-flat" >GRABAR ARTICULO</button>
      </div>
    
    </form>
    <form action="<?php echo base_url()?>mantenimiento/catalogo/catalogo" method="post">
    
     <div class="col-md-4">
       <button type="submit" class="btn btn-danger btn-block btn-flat" name="volver">VOLVER SIN GRABAR ARTICULO</button>
     </div>
    </form><br>
   </div>
       
   </div>
    <!-- /.box-body -->
   </div>
  <!-- /.box -->
 </section>
 <!-- /.content -->
 </div>
<!-- /.content-wrapper -->