<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
 <!-- Content Header (Page header) -->
 <section class="content-header">
 <h1>Alta de unidades de Empaque por Articulo</h1>
 </section>
 <!-- Main content -->
 <section class="content">
 <!-- Default box -->
  <div class="box box-solid">
    <div class="box-body">
     <div class="row">
     <div class="col-md-12">
       <?php if($this->session->flashdata("error")):?>
        <div class="alert alert-danger">
          <p><?php echo $this->session->flashdata("error")?></p>
        </div>
    </div></div>
       <?php endif ?>
    <div class="class row">
    <div class="class col-md-8">
    <label for="cod">Articulo</label>
    <form id="formulario" action="<?php echo base_url();?>mantenimiento/catalogo/storeunempaque" method="POST">
    <?php if(!empty($articulo)):?>
      <select class="form-control"required="required" name="art" id="art" size=1 >
      <option value="">Seleccione</option>
      <?php foreach($articulo as $art):?>
      <option value="<?php echo $art->id;?>"><?php echo $art->descart;?></option>
      <?php endforeach;?>
    <?php endif;?>
      </select>
      
    </div>
    </div>
    <div class="class row">
    <div class="class col-md-8">
    <label for="cod">Unidad de Empaque</label>
     <?php if(!empty($medida)):?>
      <select class="form-control" name="emp" size=1 id="emp"  required="required" >
      <option value="">Seleccione</option>
      <?php foreach($medida as $ped):?>
      <option value="<?php echo $ped->id;?>"><?php echo $ped->cod."-".$ped->descripcion."-".$ped->valor;?></option>
      <?php endforeach;?>
    <?php endif;?>
      </select>
      </span>
    </div>
    </div>
    
   
     <hr>
    <div class="row">
      <div class="col-md-4">
         <button type="submit" class="btn btn-block btn-success btn-flat" >GRABAR UNIDAD DE EMPAQUE</button>
      </div>
    
    </form>
    <form action="<?php echo base_url()?>mantenimiento/catalogo/catalogo" method="post">
    
     <div class="col-md-4">
       <button type="submit" class="btn btn-danger btn-block btn-flat" name="volver">VOLVER SIN GRABAR UNIDAD DE EMPAQUE</button>
     </div>
    </form><br>
   </div>
       
   </div>
    <!-- /.box-body -->
   </div>
  <!-- /.box -->
 </section>
 <!-- /.content -->
 </div>
<!-- /.content-wrapper -->