<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
 <!-- Content Header (Page header) -->
 <section class="content-header">
 <h1>Alta de Numero de Inventario por Articulo</h1>
 </section>
 <!-- Main content -->
 <section class="content">
 <!-- Default box -->
  <div class="box box-solid">
    <div class="box-body">
     <div class="row">
     <div class="col-md-12">
       <?php if($this->session->flashdata("error")):?>
        <div class="alert alert-danger">
          <p><?php echo $this->session->flashdata("error")?></p>
        </div>
    </div></div>
       <?php endif ?>
    <form id="formulario" action="<?php echo base_url();?>mantenimiento/catalogo/storeInventario" method="POST">   
    <div class="class row">
    <div class="class col-md-8">
    <label for="cod">Articulo</label>
    
    <?php if(!empty($articulo)):?>
      <select class="form-control"required="required" name="art" id="art" size=1 >
      <option value="">Seleccione</option>
      <?php foreach($articulo as $art):?>
      <option value="<?php echo $art->id;?>"><?php echo $art->descart;?></option>
      <?php endforeach;?>
    <?php endif;?>
      </select>
      
    </div>
    </div>
    <div class="class row">
    <div class="class col-md-8">
    <label for="cod">CODIGO DE INVENTARIO</label>
    <input type="text" class="form-control" name="codinv" id="codinv"  onkeyup="javascript:this.value=this.value.toUpperCase();" maxlength="45" required="required" placeholder="Codigo grabado en el bien">
    </div>
    </div>
    <div class="row">
      <div class="col-md-6">
       <label for="">Oficina</label>
       <input type="text" class="form-control" name="ofinv" id="ofinv" maxlength="100" onkeyup="javascript:this.value=this.value.toUpperCase();" required="required" placeholder="Donde se encuentra el bien" >
      </div>
    </div>
    <div class="row">
      <div class="col-md-6">
       <label for="">Observacion</label>
       <input type="text" class="form-control" name="obseinv" id="obseinv" maxlength="200" onkeyup="javascript:this.value=this.value.toUpperCase();" placeholder="Solo si es necesario" >
      </div>
    </div> <hr>
    <div class="row">
      <div class="col-md-4">
         <button type="submit" class="btn btn-block btn-success btn-flat" >GRABAR INVENTARIO DE  ARTICULO</button>
      </div>
    
    </form>
    <form action="<?php echo base_url()?>mantenimiento/catalogo/catalogo" method="post">
    
     <div class="col-md-4">
       <button type="submit" class="btn btn-danger btn-block btn-flat" name="volver">VOLVER SIN GRABAR INVENTARIO DE ARTICULO</button>
     </div>
    </form><br>
   </div>
       
   </div>
    <!-- /.box-body -->
   </div>
  <!-- /.box -->
 </section>
 <!-- /.content -->
 </div>
<!-- /.content-wrapper -->