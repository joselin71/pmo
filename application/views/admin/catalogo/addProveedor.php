<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
 <!-- Content Header (Page header) -->
 <section class="content-header">
 <h1>Alta de un nuevo Proveedor</h1>
 </section>
 <!-- Main content -->
 <section class="content">
 <!-- Default box -->
  <div class="box box-solid">
    <div class="box-body">
     <div class="row">
     <div class="col-md-12">
       <?php if($this->session->flashdata("error")):?>
        <div class="alert alert-danger">
          <p><?php echo $this->session->flashdata("error")?></p>
        </div>
    </div></div>
       <?php endif ?>
    <form id="formulario" action="<?php echo base_url();?>mantenimiento/catalogo/storeproveedor" method="POST">
    <div class="row <?php echo !empty(form_error('nroProv'))? 'has-error' : ''; ?>">
      <div class="col-md-3">
       <label for="rubro">Numero de Proveedor</label>
       <input type="text" class="form-control" name="nroProv" id="nroProv" value="<?php echo set_value('nroProv') ?>"  onkeypress="return numeros(event)" maxlength="4" required="required" placeholder="Solo numeros"><?php echo form_error('nroProv','<span class="help-block">','</span>')?> 
       </div>
       <div class="col-md-3">
       <label for="rubro">Numero de C.U.I.T</label>
       <input type="text" class="form-control" name="numct" id="numct"   onkeypress="return numeros(event)" maxlength="13" required="required" placeholder="Solo numeros" > 
       </div>  
       <div class="col-md-2">
       <label for="">Convenio Multilateral</label>
       <input type="text" class="form-control" name="convmt" id="convmt" maxlength="20" onkeypress="return numeros(event)" placeholder="Cero (0) si no tiene" required="required">
      </div>
      <div class="col-md-2">
       <label for="">Codigo Postal</label>
       <input type="text" class="form-control" name="cp" id="cp" maxlength="10" onkeypress="return numeros(event)" required="required" placeholder="Solo numeros">
      </div>     
      
    </div>
    <div class="row">
    <div class="col-md-5">
       <label for="">Razon Social</label>
       <input type="text" class="form-control" name="razon" id="razon" maxlength="100" onkeyup="javascript:this.value=this.value.toUpperCase();" required="required">
      </div>
      <div class="col-md-5">
       <label for="">Nombre Fantasia</label>
       <input type="text" class="form-control" name="fanta" id="fanta" maxlength="100" onkeyup="javascript:this.value=this.value.toUpperCase();" required="required">
      </div>
    </div>
    <div class="row">
    <div class="col-md-5">
       <label for="">Domicilio Fiscal</label>
       <input type="text" class="form-control" name="fiscal" id="fiscal" maxlength="100" onkeyup="javascript:this.value=this.value.toUpperCase();" required="required">
      </div>
      <div class="col-md-5">
       <label for="">Domicilio Real</label>
       <input type="text" class="form-control" name="real" id="real" maxlength="100" onkeyup="javascript:this.value=this.value.toUpperCase();" required="required">
      </div>
    </div>
    <div class="row">
    <div class="col-md-5">
       <label for="">Localidad</label>
       <input type="text" class="form-control" name="local" id="local" maxlength="50" onkeyup="javascript:this.value=this.value.toUpperCase();" required="required">
      </div>
      <div class="col-md-3">
       <label for="">Correo</label>
       <input type="email" class="form-control" name="email" id="email" maxlength="150" required="required">
      </div>
      <div class="col-md-2">
       <label for="">Contacto</label>
       <input type="text" class="form-control" name="contacto" id="contacto" maxlength="20" placeholder="Solo numeros" onkeypress="return numeros(event)" required="required">
      </div>
    </div>


    <hr>
    <div class="row">
      <div class="col-md-4">
         <button type="submit" class="btn btn-block btn-success btn-flat" >GRABAR PROVEEDOR</button>
      </div>
    
    </form>
    <form action="<?php echo base_url()?>mantenimiento/catalogo/proveedores" method="post">
    
     <div class="col-md-4">
       <button type="submit" class="btn btn-danger btn-block btn-flat" name="volver">VOLVER SIN GRABAR PROVEEDOR</button>
     </div>
    </form><br>
   </div>
       
   </div>
    <!-- /.box-body -->
   </div>
  <!-- /.box -->
 </section>
 <!-- /.content -->
 </div>
<!-- /.content-wrapper -->