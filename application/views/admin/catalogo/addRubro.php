<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
 <!-- Content Header (Page header) -->
 <section class="content-header">
 <h1>Alta de un nuevo Rubro</h1>
 </section>
 <!-- Main content -->
 <section class="content">
 <!-- Default box -->
  <div class="box box-solid">
    <div class="box-body">
     <div class="row">
     <div class="col-md-12">
       <?php if($this->session->flashdata("error")):?>
        <div class="alert alert-danger">
          <p><?php echo $this->session->flashdata("error")?></p>
        </div>
    </div></div>
       <?php endif ?>
    <form id="formulario" action="<?php echo base_url();?>mantenimiento/catalogo/storerubro" method="POST">
    <div class="row <?php echo !empty(form_error('numrub'))? 'has-error' : ''; ?>">
      <div class="col-md-2">
       <label for="rubro">Numero de Rubro</label>
       <input type="text" class="form-control" name="numrub" id="numrub" value="<?php echo set_value('numrub') ?>"  onkeypress="return numeros(event)" maxlength="4" required="required"><?php echo form_error('numrub','<span class="help-block">','</span>')?> 
       </div>       
      <div class="col-md-6">
       <label for="">Descripcion Rubro</label>
       <input type="text" class="form-control" name="detrub" id="detrub" maxlength="100" onkeyup="javascript:this.value=this.value.toUpperCase();" required="required">
      </div>
    </div>
    <hr>
    <div class="row">
      <div class="col-md-4">
         <button type="submit" class="btn btn-block btn-success btn-flat" >GRABAR RUBRO</button>
      </div>
    
    </form>
    <form action="<?php echo base_url()?>mantenimiento/catalogo/rubros" method="post">
    
     <div class="col-md-4">
       <button type="submit" class="btn btn-danger btn-block btn-flat" name="volver">VOLVER SIN GRABAR RUBRO</button>
     </div>
    </form><br>
   </div>
       
   </div>
    <!-- /.box-body -->
   </div>
  <!-- /.box -->
 </section>
 <!-- /.content -->
 </div>
<!-- /.content-wrapper -->