<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
 <!-- Content Header (Page header) -->
 <section class="content-header">
 <h1>Alta de un nuevo Sub-Rubro</h1>
 </section>
 <!-- Main content -->
 <section class="content">
 <!-- Default box -->
  <div class="box box-solid">
    <div class="box-body">
    <div class="class row">
    <div class="class col-md-8">
    <label for="cod">RUBRO</label>
    <form id="formulario" action="<?php echo base_url();?>mantenimiento/catalogo/storesubrubro" method="POST">
    <?php if(!empty($rubros)):?>
      <select required="required" name="rubro" id="rubro" >
      <option value=''>Seleccione</option>
      <?php foreach($rubros as $ped):?>
      <option value="<?php echo $ped->idrubro;?>"><?php echo $ped->rubro."-".$ped->rubrodesc;?></option>
      <?php endforeach;?>
    <?php endif;?>
      </select>
      </span>
    </div>
    </div>
     <div class="row">
     <div class="col-md-12">
       <?php if($this->session->flashdata("error")):?>
        <div class="alert alert-danger">
          <p><?php echo $this->session->flashdata("error")?></p>
        </div>
    </div></div>
       <?php endif ?>
    
    <div class="row <?php echo !empty(form_error('numsrub'))? 'has-error' : ''; ?>">
      <div class="col-md-2">
       <label for="srubro">Numero de Sub-Rubro</label>
       <input type="text" class="form-control" name="numsrub" id="numsrub" value="<?php echo set_value('numsrub') ?>"  onkeypress="return numeros(event)" maxlength="4" required="required"><?php echo form_error('numsrub','<span class="help-block">','</span>')?> 
       </div>       
      <div class="col-md-6">
       <label for="">Descripcion Sub-Rubro</label>
       <input type="text" class="form-control" name="detsrub" id="detsrub" maxlength="100" onkeyup="javascript:this.value=this.value.toUpperCase();" required="required">
      </div>
    </div>
    <hr>
    <div class="row">
      <div class="col-md-4">
         <button type="submit" class="btn btn-block btn-success btn-flat" >GRABAR SUB-RUBRO</button>
      </div>
    
    </form>
    <form action="<?php echo base_url()?>mantenimiento/catalogo/subrubros" method="post">
    
     <div class="col-md-4">
       <button type="submit" class="btn btn-danger btn-block btn-flat" name="volver">VOLVER SIN GRABAR SUB-RUBRO</button>
     </div>
    </form><br>
   </div>
       
   </div>
    <!-- /.box-body -->
   </div>
  <!-- /.box -->
 </section>
 <!-- /.content -->
 </div>
<!-- /.content-wrapper -->