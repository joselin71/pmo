<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
 <!-- Content Header (Page header) -->
 <section class="content-header">
 <h1>Alta de una nueva Unidad de Medida</h1>
 </section>
 <!-- Main content -->
 <section class="content">
 <!-- Default box -->
  <div class="box box-solid">
    <div class="box-body">
     <div class="row">
     <div class="col-md-12">
       <?php if($this->session->flashdata("error")):?>
        <div class="alert alert-danger">
          <p><?php echo $this->session->flashdata("error")?></p>
        </div>
    </div></div>
       <?php endif ?>
    <form id="formulario" action="<?php echo base_url();?>mantenimiento/catalogo/storeundmedida" method="POST">
    <div class="row <?php echo !empty(form_error('codund'))? 'has-error' : ''; ?>">
      <div class="col-md-2">
       <label for="rubro">Codigo Unidad</label>
       <input type="text" class="form-control" name="codund" id="codund" value="<?php echo set_value('codund') ?>"  onkeyup="javascript:this.value=this.value.toUpperCase();" maxlength="3" placeholder="Codigo unico de 3 letras" required="required"><?php echo form_error('codund','<span class="help-block">','</span>')?> 
       </div>       
      <div class="col-md-6">
       <label for="">Descripcion Unidad de Medida</label>
       <input type="text" class="form-control" name="detcod" id="detcod" maxlength="100" onkeyup="javascript:this.value=this.value.toUpperCase();" required="required">
      </div>
      <div class="col-md-1">
       <label for="">Valor</label>
       <input type="text" class="form-control" name="valor" id="valor" maxlength="4" onkeypress="return numeros(event)" required="required">
      </div>
    </div>
    <hr>
    <div class="row">
      <div class="col-md-4">
         <button type="submit" class="btn btn-block btn-success btn-flat" >GRABAR UNIDAD</button>
      </div>
    
    </form>
    <form action="<?php echo base_url()?>mantenimiento/catalogo/unidadesMedida" method="post">
    
     <div class="col-md-4">
       <button type="submit" class="btn btn-danger btn-block btn-flat" name="volver">VOLVER SIN GRABAR UNIDAD</button>
     </div>
    </form><br>
   </div>
       
   </div>
    <!-- /.box-body -->
   </div>
  <!-- /.box -->
 </section>
 <!-- /.content -->
 </div>
<!-- /.content-wrapper -->