<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
 <!-- Content Header (Page header) -->
 <section class="content-header">
 <h1>Modificar un Deposito</h1>
 </section>
 <!-- Main content -->
 <section class="content">
 <!-- Default box -->
  <div class="box box-solid">
    <div class="box-body">
     <div class="row">
     <div class="col-md-12">
       <?php if($this->session->flashdata("error")):?>
        <div class="alert alert-danger">
          <p><?php echo $this->session->flashdata("error")?></p>
        </div>
    </div></div>
       <?php endif ?>
    <form id="formulario" action="<?php echo base_url();?>mantenimiento/catalogo/edstoreDeposito" method="POST">
    <div class="row">
      <input type="hidden" value="<?php echo $lista->id; ?>" name="idr" id="idr">
      <div class="col-md-6">
       <label for="">Descripcion Deposito</label>
       <input type="text" class="form-control" name="detdep" id="detdep" maxlength="60" onkeyup="javascript:this.value=this.value.toUpperCase();" required="required" value="<?php echo $lista->nombre; ?>">
      </div>
    </div><br>
    <div class="class row">
    <div class="class col-md-8">
    <label for="cod">ESTADO:</label>
       <select required="required" name="estad" id="estad" >
       <option value='B'>INACTIVO</option><option value='A'>ACTIVO</option>
       </select></span>
    </div>
    </div>
    <hr>
    <div class="row">
      <div class="col-md-4">
         <button type="submit" class="btn btn-block btn-success btn-flat" >MODIFICAR DEPOSITO</button>
      </div>
    
    </form>
    <form action="<?php echo base_url()?>mantenimiento/catalogo/depositos" method="post">
    
     <div class="col-md-4">
       <button type="submit" class="btn btn-danger btn-block btn-flat" name="volver">VOLVER SIN MODIFICAR DEPOSITO</button>
     </div>
    </form><br>
   </div>
       
   </div>
    <!-- /.box-body -->
   </div>
  <!-- /.box -->
 </section>
 <!-- /.content -->
 </div>
<!-- /.content-wrapper -->