<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
 <!-- Content Header (Page header) -->
 <section class="content-header">
 <h1>Modificacion de Datos de un Proveedor</h1>
 </section>
 <!-- Main content -->
 <section class="content">
 <!-- Default box -->
  <div class="box box-solid">
    <div class="box-body">
     <div class="row">
     <div class="col-md-12">
       <?php if($this->session->flashdata("error")):?>
        <div class="alert alert-danger">
          <p><?php echo $this->session->flashdata("error")?></p>
        </div>
    </div></div>
       <?php endif ?>
    <form id="formulario" action="<?php echo base_url();?>mantenimiento/catalogo/edstoreProveedor" method="POST">
    <div class="row <?php echo !empty(form_error('nroProv'))? 'has-error' : ''; ?>">
    <input type="hidden" value="<?php echo $lista->id; ?>" name="idr" id="idr">
      <div class="col-md-3">
       <label for="rubro">Numero de Proveedor</label>
       <input type="text" class="form-control" name="nroProv" id="nroProv" value="<?php echo $lista->nroProv; ?>"  onkeypress="return numeros(event)" maxlength="4" required="required" ><?php echo form_error('nroProv','<span class="help-block">','</span>')?> 
       </div>
       <div class="col-md-3">
       <label for="rubro">Numero de C.U.I.T</label>
       <input type="text" class="form-control" name="numct" id="numct"   onkeypress="return numeros(event)" maxlength="13" required="required" value="<?php echo $lista->cuit; ?>" > 
       </div>  
       <div class="col-md-2">
       <label for="">Convenio Multilateral</label>
       <input type="text" class="form-control" name="convmt" id="convmt" maxlength="20" onkeypress="return numeros(event)" value="<?php echo $lista->conv_multi; ?>" required="required">
      </div>
      <div class="col-md-2">
       <label for="">Codigo Postal</label>
       <input type="text" class="form-control" name="cp" id="cp" maxlength="10" onkeypress="return numeros(event)" required="required" value="<?php echo $lista->cp; ?>">
      </div>     
      
    </div>
    <div class="row">
    <div class="col-md-5">
       <label for="">Razon Social</label>
       <input type="text" class="form-control" name="razon" id="razon" maxlength="100" onkeyup="javascript:this.value=this.value.toUpperCase();" required="required" value="<?php echo $lista->razonsocial; ?>">
      </div>
      <div class="col-md-5">
       <label for="">Nombre Fantasia</label>
       <input type="text" class="form-control" name="fanta" id="fanta" maxlength="100" onkeyup="javascript:this.value=this.value.toUpperCase();" required="required" value="<?php echo $lista->razonfantasia; ?>">
      </div>
    </div>
    <div class="row">
    <div class="col-md-5">
       <label for="">Domicilio Fiscal</label>
       <input type="text" class="form-control" name="fiscal" id="fiscal" maxlength="100" onkeyup="javascript:this.value=this.value.toUpperCase();" required="required" value="<?php echo $lista->domiciliofiscal; ?>">
      </div>
      <div class="col-md-5">
       <label for="">Domicilio Real</label>
       <input type="text" class="form-control" name="real" id="real" maxlength="100" onkeyup="javascript:this.value=this.value.toUpperCase();" required="required" value="<?php echo $lista->domicilioreal; ?>">
      </div>
    </div>
    <div class="row">
    <div class="col-md-5">
       <label for="">Localidad</label>
       <input type="text" class="form-control" name="local" id="local" maxlength="50" onkeyup="javascript:this.value=this.value.toUpperCase();" required="required" value="<?php echo $lista->localidad; ?>">
      </div>
      <div class="col-md-3">
       <label for="">Correo</label>
       <input type="email" class="form-control" name="email" id="email" maxlength="150" required="required" value="<?php echo $lista->email; ?>">
      </div>
      <div class="col-md-2">
       <label for="">Contacto</label>
       <input type="text" class="form-control" name="contacto" id="contacto" maxlength="20" value="<?php echo $lista->tel; ?>" onkeypress="return numeros(event)" required="required">
      </div>
    </div>
    <br>
    <div class="class row">
    <div class="class col-md-8">
    <label for="cod">ESTADO:</label>
       <select required="required" name="estad" id="estad" >
       <option value='B'>INACTIVO</option><option value='A'>ACTIVO</option>
       </select></span>
    </div>
    </div>


    <hr>
    <div class="row">
      <div class="col-md-4">
         <button type="submit" class="btn btn-block btn-success btn-flat" >GRABAR PROVEEDOR</button>
      </div>
    
    </form>
    <form action="<?php echo base_url()?>mantenimiento/catalogo/proveedores" method="post">
    
     <div class="col-md-4">
       <button type="submit" class="btn btn-danger btn-block btn-flat" name="volver">VOLVER SIN GRABAR PROVEEDOR</button>
     </div>
    </form><br>
   </div>
       
   </div>
    <!-- /.box-body -->
   </div>
  <!-- /.box -->
 </section>
 <!-- /.content -->
 </div>
<!-- /.content-wrapper -->