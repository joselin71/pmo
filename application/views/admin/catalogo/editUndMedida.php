<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
 <!-- Content Header (Page header) -->
 <section class="content-header">
 <h1>Modificar una Unidad de Medida</h1>
 </section>
 <!-- Main content -->
 <section class="content">
 <!-- Default box -->
  <div class="box box-solid">
    <div class="box-body">
     <div class="row">
     <div class="col-md-12">
       <?php if($this->session->flashdata("error")):?>
        <div class="alert alert-danger">
          <p><?php echo $this->session->flashdata("error")?></p>
        </div>
    </div></div>
       <?php endif ?>
    <form id="formulario" action="<?php echo base_url();?>mantenimiento/catalogo/edstoreundmedida" method="POST">
    <div class="row <?php echo !empty(form_error('codund'))? 'has-error' : ''; ?>">
      <input type="hidden" value="<?php echo $lista->id; ?>" name="idr" id="idr">
      <div class="col-md-2">
       <label for="rubro">Codigo Unidad</label>
       <input type="text" class="form-control" name="codund" id="codund" value="<?php echo $lista->cod; ?>" onkeyup="javascript:this.value=this.value.toUpperCase();" maxlength="3" required="required"><?php echo form_error('codund','<span class="help-block">','</span>')?> 
       </div>       
      <div class="col-md-6">
       <label for="">Descripcion Unidad de Medida</label>
       <input type="text" class="form-control" name="detcod" id="detcod" maxlength="45" onkeyup="javascript:this.value=this.value.toUpperCase();" value="<?php echo $lista->descripcion; ?>" required="required" >
      </div>
      <div class="col-md-1">
       <label for="">Valor</label>
       <input type="text" class="form-control" name="valor" id="valor" maxlength="4" value="<?php echo $lista->valor; ?>" onkeypress="return numeros(event)" required="required">
      </div>
    </div><br>
    <div class="class row">
    <div class="class col-md-8">
    <label for="cod">ESTADO:</label>
       <select required="required" name="estad" id="estad" >
       <option value='B'>INACTIVO</option><option value='A'>ACTIVO</option>
       </select></span>
    </div>
    </div>
    <hr>
    <div class="row">
      <div class="col-md-4">
         <button type="submit" class="btn btn-block btn-success btn-flat" >MODIFICAR UNIDAD MEDIDA</button>
      </div>
    
    </form>
    <form action="<?php echo base_url()?>mantenimiento/catalogo/unidadesmedidas" method="post">
    
     <div class="col-md-4">
       <button type="submit" class="btn btn-danger btn-block btn-flat" name="volver">VOLVER SIN MODIFICAR UNIDAD MEDIDA</button>
     </div>
    </form><br>
   </div>
       
   </div>
    <!-- /.box-body -->
   </div>
  <!-- /.box -->
 </section>
 <!-- /.content -->
 </div>
<!-- /.content-wrapper -->