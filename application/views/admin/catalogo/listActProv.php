<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
 <!-- Content Header (Page header) -->
 <section class="content-header">
 <h1>Actividades de Proveedor<small>Listado</small></h1>
 </section>
 <!-- Main content -->
 <section class="content">
 <!-- Default box -->
  <div class="box box-solid">
    <div class="box-body">
     <div class="row">
      <div class="col-md-12">
        <a href="<?php echo base_url();?>mantenimiento/catalogo/addActProveedor" class="btn btn-primary btn-flat"><span class="fa fa-plus"></span>Nueva Actividad</a>
      </div>
     </div><hr>     
     <div class="row">
       <div class="col-md-12">
       <table id="compras" class="table-bordered btn-hover" style="width:100%">
        <thead>
        <tr><th>#</th><th>Codigo</th><th>Descripcion</th><th>Valor</th><th>Estado</th><th>Opciones</th>
        </tr>
        </thead>
        <tbody> 
        <?php if(!empty($lista)):?>
        <?php foreach($lista as $ped):?>
        <tr><td><?php echo $ped->id; ?></td><td ><?php echo $ped->cod; ?></td><td ><?php echo $ped->descripcion; ?></td><td ><?php echo $ped->valor; ?></td><td><?php echo $ped->estado; ?></td>
        <td>
        <div class="btn-group">
        <a href="<?php echo base_url();?>mantenimiento/catalogo/editUndMedida/<?php echo $ped->id;?>" ><span class="material-icons" style="color: #FB8C00;">create</span></a>
        </div>        
        </td></tr>
        <?php endforeach;?>
        <?php endif;?>
         </tbody>
       </table>
       </div>
     </div>
    </div>
    <!-- /.box-body -->
   </div>
  <!-- /.box -->
 </section>
 <!-- /.content -->
 </div>
<!-- /.content-wrapper -->