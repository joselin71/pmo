<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
 <!-- Content Header (Page header) -->
 <section class="content-header">
 <h1>Catologo de Articulos<small>Listado</small></h1>
 </section>
 <!-- Main content -->
 <section class="content">
 <!-- Default box -->
  <div class="box box-solid">
    <div class="box-body">
     <div class="row">
      <div class="col-md-2">
        <a href="<?php echo base_url();?>mantenimiento/catalogo/addCatalogo" class="btn btn-primary btn-flat"><span class="fa fa-plus"></span>Nuevo Articulo al Catalogo</a>
      </div>
      <div class="col-md-2">
        <a href="<?php echo base_url();?>mantenimiento/catalogo/addEmpaque" class="btn btn-danger btn-flat"><span class="fa fa-plus"></span>Nuevo Unidad de Empaque</a>
      </div>
      <div class="col-md-2">
        <a href="<?php echo base_url();?>mantenimiento/catalogo/addEan" class="btn btn-success btn-flat"><span class="fa fa-plus"></span>Nuevo EAN por Articulo</a>
      </div>
      <div class="col-md-2">
        <a href="<?php echo base_url();?>mantenimiento/catalogo/addInventario" class="btn btn-warning btn-flat"><span class="fa fa-plus"></span>Nuevo Numero de Inventario</a>
      </div>
     </div><hr>     
     <div class="row">
       <!--div class="col-md-12"-->
       <div class="col-md-12 table-responsive">
       <table id="articulos" class="table-bordered btn-hover" style="width:90%">
        <thead>
        <tr><th>Codigo</th><th>Descripcion</th><th>Rubro</th><th>Descripcion</th><th>Subrubro</th><th>Descripcion</th><th>Inventariable</th><th>Unidad Base</th><th>Precio</th><th>Estado</th></th><th>Opciones</th>
        </tr>
        </thead>
        <tbody> 
        <?php if(!empty($lista)):?>
        <?php foreach($lista as $ped):?>
        <tr><td align="right"><?php echo $ped->id; ?></td><td ><?php echo $ped->descart; ?></td><td align="right"><?php echo $ped->rubro; ?></td><td ><?php echo $ped->rubrodesc; ?></td><td align="right"><?php echo $ped->subrubro; ?></td><td ><?php echo $ped->subrubdesc; ?></td><td ><?php echo $ped->inventariable; ?></td><td ><?php echo $ped->cod."-".$ped->descripcion."-".$ped->valor; ?></td><td align="right"><?php echo $ped->precio; ?></td><td align="center"><?php echo $ped->estado; ?></td>
        <td>
        <div class="btn-group" style="font-size:75%">
        <button type="button" class="btn btn-info btn-view-Catalogo btn-xs" value="<?php echo $ped->id;?>" data-toggle="modal" data-target="#modal-default" >
        <span class="fa fa-search"></span>
      </button>
        <div class="btn-group">
        <a href="<?php echo base_url();?>mantenimiento/catalogo/editCatalogo/<?php echo $ped->id;?>" ><span class="material-icons" style="color: #FB8C00;">create</span></a>
        </div>        
        </td></tr>
        <?php endforeach;?>
        <?php endif;?>
         </tbody>
       </table>
       </div>
     </div>
    </div>
    <!-- /.box-body -->
   </div>
  <!-- /.box -->
 </section>
 <!-- /.content -->
 </div>
<!-- /.content-wrapper -->
<div class="modal fade" id="modal-default">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
              </button>
              <h4 class="modal-title">Datos del Articulo</h4>
            </div>
            <div class="modal-body">
              
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Cerrar</button>
              <button type="button" class="btn btn-primary btn-print"><span class="fa fa-pencil"> </span>Imprimir</button>
              
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->

      