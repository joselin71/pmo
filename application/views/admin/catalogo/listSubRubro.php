<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
 <!-- Content Header (Page header) -->
 <section class="content-header">
 <h1>Sub-Rubros para Pedidos de Materiales<small>Listado</small></h1>
 </section>
 <!-- Main content -->
 <section class="content">
 <!-- Default box -->
  <div class="box box-solid">
    <div class="box-body">
     <div class="row">
      <div class="col-md-12">
        <a href="<?php echo base_url();?>mantenimiento/catalogo/addSubRubro" class="btn btn-primary btn-flat"><span class="fa fa-plus"></span>Nuevo Sub-Rubro</a>
      </div>
     </div><hr>     
     <div class="row">
       <div class="col-md-12">
       <table id="subrubros" class="table-bordered btn-hover" style="width:100%">
        <thead>
        <tr><th>#</th><th>Rubro</th>
        <th>Descripcion</th><th>Sub-Rubro</th>
        <th>Descripcion</th><th>Estado</th><th>Alta</th><th>Actualizado</th><th>Opciones</th>
        </tr>
        </thead>
        <tbody> 
        <?php if(!empty($subrubros)):?>
        <?php foreach($subrubros as $ped):?>
        <tr><td><?php echo $ped->idsubrubro; ?><td><?php echo $ped->rubro; ?></td> <td ><?php echo $ped->rubrodesc; ?></td><td><?php echo $ped->subrubro; ?></td> <td ><?php echo $ped->subrubdesc; ?></td><td><?php echo $ped->estado; ?><td><?php echo $ped->fecha_alta; ?></td><td><?php echo $ped->fecha_update; ?></td></td>
        <td>
        <div class="btn-group">
        <a href="<?php echo base_url();?>mantenimiento/catalogo/editSubRubro/<?php echo $ped->idsubrubro;?>" ><span class="material-icons" style="color: #FB8C00;">create</span></a>
        </div>        
        </td></tr>
        <?php endforeach;?>
        <?php endif;?>
         </tbody>
       </table>
       </div>
     </div>
    </div>
    <!-- /.box-body -->
   </div>
  <!-- /.box -->
 </section>
 <!-- /.content -->
 </div>
<!-- /.content-wrapper -->