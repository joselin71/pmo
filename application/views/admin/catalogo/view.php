<div class="row">
<div class="col-xs-12">
<table class="table table-bordered">
<caption>DETALLE DE ARTICULOS</caption>
<tbody>
<tr>
<td>Codigo Articulo</td><td><?php echo $lista->id;?></td>
</tr>
<tr><td>Descripcion</td><td><?php echo $lista->descart;?></td></tr>
<tr><td>Estado</td><td><?php echo $lista->estado;?></td></tr>
<tr><td>Fecha Alta</td><td><?php echo $lista->fechaalta;?></td></tr>
<tr><td>Fecha Vencimiento</td><td><?php echo $lista->fechavto;?></td></tr>
<tr><td>Fecha Baja</td><td><?php echo $lista->fecha_baja;?></td></tr>
<tr><td>Inventariable</td><td><?php echo $lista->inventariable;?></td></tr>
<tr><td>Empaque</td><td><?php echo $lista->cod."-".$lista->descripcion."-".$lista->valor;?></td></tr>
<tr><td>Minimo</td><td><?php echo $lista->minimo;?></td></tr>
<tr><td>Maximo</td><td><?php echo $lista->maximo;?></td></tr>
<tr><td>Precio</td><td><?php echo $lista->precio;?></td></tr>
<tr><td>Rubro</td><td><?php echo $lista->rubro."-".$lista->rubrodesc;?></td></tr>
<tr><td>Subrubro</td><td><?php echo $lista->subrubro."-".$lista->subrubdesc;?></td></tr>
</tbody>
</table>
</div>

<div class="col-xs-12">
<table class="table table-bordered">
<caption>EAN ASOCIADOS</caption>
<tbody>
<?php if(!empty($codigos)):?>
<?php foreach($codigos as $ean):?>
<tr><td>Nro.Prov</td><td><?php echo $ean->nroProv;?></td></tr>
<tr><td>Razon Social</td><td><?php echo $ean->razonsocial;?></td></tr>
<tr><td>EAN</td><td><?php echo $ean->ean;?></td></tr>
<?php endforeach;?>
<?php endif;?>
<div class="col-xs-12">
<table class="table table-bordered">
<caption>NUMERO DE INVENTARIO</caption>
<tbody>
<?php if(!empty($inventario)):?>
<?php foreach($inventario as $inv):?>
<tr><td>Nro.Inventario</td><td><?php echo $inv->nroinventario;?></td></tr>
<tr><td>Oficina</td><td><?php echo $inv->oficina;?></td></tr>
<?php endforeach;?>
<?php endif;?>
</tbody>
</table>
</div>

</div>

