<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
 <!-- Content Header (Page header) -->
 <section class="content-header">
 <h1>Formulario de Solicitud</h1>
 </section>
 <!-- Main content -->
 <section class="content">
 <!-- Default box -->
  <div class="box box-solid">
    <div class="box-body">
     
     <div class="row">
       <div class="col-md-12">
       <?php if($this->session->flashdata("error")):?>
            <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <p><i class="icon fa fa-ban"></i><?php echo $this->session->flashdata("error")?></p>
            </div>
            <?php endif ?>
       <form action="<?php echo base_url();?>mantenimiento/certificado/store" method="POST">
       <div class="form-group">
       <label for="">DOCUMENTO-Ingrese el numero sin puntos</label><h6></h6>
       <input type="text" class="form-control" name="dni" type="text" id="dni" placeholder="Solo numeros"  onkeypress="return numeros(event)"  required="required" onPaste="return false" maxlength="8" >
       </div>
       <div class="form-group">
       <label for="">TRAMITE-Ingrese los 11 digitos(ceros incluidos)</label>
       <input type="text" class="form-control" name="tramdni" type="text" id="tramdni" placeholder="El numero completo que figura en el DNI"  onkeypress="return numeros(event)"  required="required" onPaste="return false" minlength="11"  maxlength="11" pattern="[0-9]*">
       <div class="col-xs-12" align="center"><img src="<?php echo base_url()."assets/template/img/nrotram.jpg"?>" alt="" /></div>
       </div>
     
       <div class="form-group">
       <label for="">APELLIDOS</label>
       <input type="text" class="form-control" name="apellido" type="text" id="apellido" placeholder="Segun DNI"  onkeyup="javascript:this.value=this.value.toUpperCase();" required="required" maxlength="58" onPaste="return false" >
       </div>
       <div class="form-group">
       <label for="">NOMBRES</label>
       <input type="text" class="form-control" name="nombre" type="text" id="nombre" placeholder="Segun DNI"  onkeyup="javascript:this.value=this.value.toUpperCase();" required="required" maxlength="44" onPaste="return false">
       </div>
       <div class="form-group">
       <label for="">FECHA NACIMIENTO</label>
       <input type="date" class="form-control" id="fecnac" name="fecnac" required="required"  >
       </div>
       <div class="form-group">
       <label for="">NACIONALIDAD</label>
       <input type="text" class="form-control" name="nacion" type="text" id="nacion" placeholder="Segun Documento"  onkeyup="javascript:this.value=this.value.toUpperCase();" required="required" maxlength="44" onPaste="return false" >
       </div>

       <div class="form-group">
       <label for="">DOMICILIO ACTUAL</label>
       <input type="text" class="form-control" name="domi" type="text" id="domi" placeholder="Domicilio Actual"  onkeyup="javascript:this.value=this.value.toUpperCase();"  required="required" onPaste="return false" maxlength="78">
       </div>
       <div class="form-group">
       <label for="">LOCALIDAD</label>
       <input type="text" class="form-control" name="localidad" type="text" id="localidad" placeholder="Localidad del domicilio actual" required="required" maxlength="98" onPaste="return false" onkeyup="javascript:this.value=this.value.toUpperCase();">
       </div>
       <div class="form-group">
       <label for="">LUGAR DE PRESENTACION</label>
       <input type="text" class="form-control" name="presenta" type="text" id="presenta" placeholder="Lugar de Presentacion" required="required" maxlength="60" onPaste="return false" onkeyup="javascript:this.value=this.value.toUpperCase();">
       </div>
       <div class="form-group">
       <label for="">CELULAR / FIJO</label>
       <input type="text" class="form-control" name="cel" type="text" id="cel" placeholder="Solo numeros"  onkeypress="return numeros(event)"  required="required" onPaste="return false" maxlength="20">
       </div>
       <div class="form-group">
       <label for="">CORREO ELECTRONICO</label>
       <input type="text" class="form-control" name="correo" type="email" id="correo" placeholder="Correo Electronico Valido" required="required" maxlength="255" onPaste="return false">
       </div>
       <div class="form-group">
       <button type="submit" class="btn btn-block btn-success btn-flat" >GENERAR FORMULARIO</button>
       </div>
       </form>
       <form action="<?php echo base_url()?>mantenimiento/certificado" method="post">
        <div class="row">
        <div class="col-xs-12">
         <button type="submit" class="btn btn-danger btn-block btn-flat" name="volver">VOLVER</button>
       </div>
       </form><br>
       </div>
       
       </div>
       
     </div>
    </div>
    <!-- /.box-body -->
   </div>
  <!-- /.box -->
 </section>
 <!-- /.content -->
 </div>
<!-- /.content-wrapper -->