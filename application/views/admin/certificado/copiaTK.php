<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
 <!-- Content Header (Page header) -->
 <section class="content-header">
 <h1>COPIA DE TICKETS DE CERTIFICADOS DE DOMICILIO ON-LINE ESTADO TERMINADO </h1><br>
 <br>
 </section>
<!-- Main content -->
<section class="content">
 <!-- Default box -->
  <div class="box box-solid">
    <div class="box-body">
    <div class="row">
    <div class="col-md-12">
       <?php if($this->session->flashdata("error")):?>
            <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <p><i class="icon fa fa-ban"></i><?php echo $this->session->flashdata("error")?></p>
            </div>
            <?php endif ?>
    </div>


    <div class="col-md-5">  
    <div class="form-inline" >
    <form action="<?php echo base_url();?>mantenimiento/certificado/copiaTK" method="POST" >
    <div class="form-group">
      <label for="">FECHA</label>
       <input type="date" class="form-control" id="fechainicio" name="fechainicio" required="required" value="<?php echo date("Y-m-d"); ?>" >
       <input type="date" class="form-control" id="fechafin" name="fechafin" required="required" value="<?php echo date("Y-m-d"); ?>" >

    </div>
    </div>
    </div>
    <br>
    </div>
    <br>
    <div class="row">
      <div class="col-md-4">
      <button type="submit" class="btn btn-success btn-block btn-flat" name="volver">COPIAR</button>
      </div>
    </form>
    <form action="<?php echo base_url()?>mantenimiento/certificado/principalDom" method="post">
      <div class="col-md-4">
         <button type="submit" class="btn btn-danger btn-block btn-flat" name="volver">VOLVER</button>
      </div>
    </form>
   </div><br>
   <!--?php if(!empty($data)){ ?-->
<h4>TICKETS COPIADOS:<?php echo $copiado;?></h4><br>
<h4>TICKETS NO COPIADOS:<?php echo $nocopiado;?></h4><br>
<!--?php }?-->




    </div>
    <!-- /.box-body -->
   </div>
  <!-- /.box -->
 </section>
 <!-- /.content -->
 </div>
<!-- /.content-wrapper -->