<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
 <!-- Content Header (Page header) -->
 <section class="content-header">
 <h1>Descarga de documentos anexos</h1>
 </section>
 <!-- Main content -->
 <section class="content">
 <!-- Default box -->
  <div class="box box-solid">
    <div class="box-body">
     
     <div class="row">
       <div class="col-md-12">
       <?php if($this->session->flashdata("error")):?>
            <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <p><i class="icon fa fa-ban"></i><?php echo $this->session->flashdata("error")?></p>
            </div>
            <?php endif ?>
       <form action="<?php echo base_url();?>mantenimiento/certificado/downarcFCert" method="POST" enctype="multipart/form-data">
       <div class="form-group">
       <label for="">DOCUMENTO</label>
       <input type="text" class="form-control" name="dniS" type="text" id="dniS" placeholder="Solo numeros"  onkeypress="return numeros(event)"  required="required" onPaste="return false" maxlength="10" >
       </div>
       <div class="form-group">
       <button type="submit" class="btn btn-block btn-success btn-flat" >VISUALIZAR DOCUMENTACION</button>
       </div>
       </form><br>
       <form action="<?php echo base_url()?>mantenimiento/certificado/principalDom" method="post">
        <div class="row">
        <div class="col-xs-12">
         <button type="submit" class="btn btn-danger btn-block btn-flat" name="volver">VOLVER</button>
       </div><!-- /.col -->
       </div>
       </form>
        <?php if(!empty($ticket)){ ?>
        <img src="data:image/jpg; base64,<?php echo base64_encode($ticket);?>" width="350" height="300" >
        <?php }
        else{ ?>
        <h6>NO EXISTE ARCHIVO DE TICKET</h6>
        <?php  }?>

        <?php if(!empty($frente)){ ?>
        <img src="data:image/jpg; base64,<?php echo base64_encode($frente);?>" width="350" height="300" >
        <?php }
        else{ ?>
        <h6>NO EXISTE ARCHIVO FRENTE DNI</h6>
        <?php  }?>

        <?php if(!empty($dorso)){ ?>
        <img src="data:image/jpg; base64,<?php echo base64_encode($dorso);?>" width="350" height="300" >
        <?php }
        else{ ?>
        <h6>NO EXISTE ARCHIVO DORSO DNI</h6>
        <?php  }?>

        <?php if(!empty($bs)){ ?>
        <img src="data:image/jpg; base64,<?php echo base64_encode($bs);?>" width="350" height="300" >
        <?php }
        else{ ?>
        <h6>NO EXISTE ARCHIVO BOLETA</h6>
        <?php  }?>
       <br>
     </div>
    </div>
    <!-- /.box-body -->
   </div>
  <!-- /.box -->
 </section>
 <!-- /.content -->
       
 </div>
<!-- /.content-wrapper -->