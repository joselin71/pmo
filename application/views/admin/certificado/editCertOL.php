<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
 <!-- Content Header (Page header) -->
 <section class="content-header">
 <h1>Edicion de Solicitud</h1>
 </section>
 <!-- Main content -->
 <section class="content">
 <!-- Default box -->
  <div class="box box-solid">
    <div class="box-body">
     
     <div class="row">
       <div class="col-md-12">
       <?php if($this->session->flashdata("error")):?>
            <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <p><i class="icon fa fa-ban"></i><?php echo $this->session->flashdata("error")?></p>
            </div>
            <?php endif ?>
       <form action="<?php echo base_url();?>mantenimiento/certificado/confirmstore" method="POST">
       <div class="form-group">
       <label for="">FORMULARIO</label>
       <input type="text" class="form-control" name="formulario" id="formulario" type="text" value="<?php echo $id;?>" readonly="readonly" >
       </div>

       <div class="form-group">
       <label for="">DOCUMENTO-Ingrese el numero sin puntos</label><h6></h6>
       <input type="text" class="form-control" name="dni" type="text" id="dni" value="<?php echo $dni;?>"  onkeypress="return numeros(event)"  required="required" onPaste="return false" maxlength="8" >
       </div>
       <div class="form-group">
       <label for="">TRAMITE-Ingrese los 11 digitos(ceros incluidos)</label>
       <input type="text" class="form-control" name="tramdni" type="text" id="tramdni" value="<?php echo $tramdni;?>"  onkeypress="return numeros(event)"  required="required" onPaste="return false" maxlength="11" >
       </div>
       
       <div class="form-group">
       <label for="">APELLIDOS</label>
       <input type="text" class="form-control" name="apellido" type="text" id="apellido" value="<?php echo $apellido;?>"  onkeyup="javascript:this.value=this.value.toUpperCase();" required="required" maxlength="58" onPaste="return false" >
       </div>
       <div class="form-group">
       <label for="">NOMBRES</label>
       <input type="text" class="form-control" name="nombre" type="text" id="nombre" value="<?php echo $nombre;?>"  onkeyup="javascript:this.value=this.value.toUpperCase();" required="required" maxlength="44" onPaste="return false">
       </div>
       <div class="form-group">
       <label for="">FECHA NACIMIENTO</label>
       <input type="date" class="form-control" id="fecnac" name="fecnac" required="required" max="2004-01-01" value="<?php echo $nacio;?>" >
       </div>
       <div class="form-group">
       <label for="">NACIONALIDAD</label>
       <input type="text" class="form-control" name="nacion" type="text" id="nacion" value="<?php echo $nacion;?>"  onkeyup="javascript:this.value=this.value.toUpperCase();" required="required" maxlength="44" onPaste="return false">
       </div>
       <div class="form-group">
       <label for="">DOMICILIO ACTUAL</label>
       <input type="text" class="form-control" name="domi" type="text" id="domi" value="<?php echo $domicilio;?>"  onkeyup="javascript:this.value=this.value.toUpperCase();"  required="required" onPaste="return false" maxlength="78">
       </div>
       <div class="form-group">
       <label for="">LOCALIDAD</label>
       <input type="text" class="form-control" name="localidad" type="text" id="localidad" value="<?php echo $localidad;?>" required="required" maxlength="98" onPaste="return false" onkeyup="javascript:this.value=this.value.toUpperCase();">
       </div>
       <div class="form-group">
       <label for="">PROVINCIA</label>
       <input type="text" class="form-control" name="provincia" type="text" id="provincia" value="<?php echo $provincia;?>" required="required" maxlength="98" onPaste="return false" onkeyup="javascript:this.value=this.value.toUpperCase();">
       </div>
       <div class="form-group">
       <label for="">PAIS</label>
       <input type="text" class="form-control" name="pais" type="text" id="pais" value="<?php echo $pais;?>" required="required" maxlength="98" onPaste="return false" onkeyup="javascript:this.value=this.value.toUpperCase();">
       </div>
       <div class="form-group">
       <label for="">PRESENTA EN</label>
       <input type="text" class="form-control" name="presenta" type="text" id="presenta" value="<?php echo $presenta;?>" required="required" maxlength="60" onPaste="return false" onkeyup="javascript:this.value=this.value.toUpperCase();">
       </div>
       <div class="form-group">
       <label for="">CELULAR / FIJO</label>
       <input type="text" class="form-control" name="cel" type="text" id="cel" value="<?php echo $celular;?>"  onkeypress="return numeros(event)"  required="required" onPaste="return false" maxlength="20">
       </div>
       <div class="form-group">
       <label for="">CORREO ELECTRONICO</label>
       <input type="text" class="form-control" name="correo" type="email" id="correo" value="<?php echo $correo;?>" required="required" maxlength="255" onPaste="return false">
       </div>
       <div class="form-group">
       <label for="">COMPROBANTE</label>
       <input type="text" class="form-control" name="transaccion" type="text" id="transaccion" value="<?php echo $transaccion;?>"  onkeypress="return numeros(event)"  required="required" onPaste="return false" maxlength="8">
       </div>
       <div class="form-group">
       <label for="">FECHA COMPROBANTE</label>
       <input type="date" class="form-control" id="fectransac" name="fectransac" required="required" value="<?php echo $fectransac;?>" >
       </div>
       <div class="form-group">
       <label for="">HORA COMPROBANTE</label>
       <input type="text" class="form-control" name="horatransac" type="text" id="horatransac" value="<?php echo $horatransac;?>"  onkeypress="return numeros(event)"  required="required" onPaste="return false" maxlength="2">
       </div>
       <div class="form-group">
       <label for="">MINUTOS COMPROBANTE</label>
       <input type="text" class="form-control" name="minutransac" type="text" id="minutransac" value="<?php echo $minutransac;?>"  onkeypress="return numeros(event)"  required="required" onPaste="return false" maxlength="2">
       </div>
       <div class="form-group">
       <label for="">IMPORTE ABONADO(2 decimales)</label>
       <input type="text" class="form-control" name="abono" type="text" id="abono" value="<?php echo $abono;?>"   pattern="[0-9]{1,3}[.][0-9]{2}"  required="required" onPaste="return false" maxlength="7">
       </div>
       <div class="form-group">
       <label for="">CANTIDAD DE TICKETS ABONADOS</label>
       <input type="text" class="form-control" name="tk" type="text" id="tk" value="<?php echo $tk;?>"  onkeypress="return numeros(event)"  required="required" onPaste="return false" maxlength="1">
       </div>



       <div class="form-group">
       <button type="submit" class="btn btn-block btn-success btn-flat" >CONFIRMAR Y GENERAR</button>
       </div>
       </form>
       <form action="<?php echo base_url()?>mantenimiento/certificado/generaCertOL" method="post">
        <div class="row">
        <div class="col-xs-12">
         <button type="submit" class="btn btn-danger btn-block btn-flat" name="volver">VOLVER</button>
       </div>
       </form><br>
       </div>
       
       </div>
       
     </div>
    </div>
    <!-- /.box-body -->
   </div>
  <!-- /.box -->
 </section>
 <!-- /.content -->
 </div>
<!-- /.content-wrapper -->