<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
 <!-- Content Header (Page header) -->
 <section class="content-header">
 <h1>Certificado de Domicilio Provincial</h1>
 <form action="<?php echo base_url()?>" method="post">
        <div class="row">
        <div class="col-xs-12">
         <button type="submit" class="btn btn-danger btn-block btn-flat" name="volver">VOLVER</button>
       </div>
       </form><br>
 </section>
 <!-- Main content -->
 <section class="content">
 <!-- Default box -->
  <div class="box box-solid">
    <div class="box-body">
    <b><h5>VIGENCIA DE LOS CERTIFICADOS:10 DIAS DESDE LA FECHA DE EMISION</h5></b>
    <h5>PASO 1</h5><h6>Complete con los datos solicitados</h6>
     <div class="row">
      <div class="col-md-12">
        <a href="<?php echo base_url();?>mantenimiento/certificado/add" class="btn btn-flat" style="background-color:#7bbbe8;color:white;"><span class="fa fa-plus"></span>GENERAR FORMULARIO</a><br>
        </div>
     </div><br>
     <h5>PASO 2</h5><h6>Imprima el formulario generado en el PASO 1</h6>
     <div class="row">
      <div class="col-md-12">
        <a href="<?php echo base_url();?>mantenimiento/certificado/printform" class="btn btn-flat" style="background-color:#7bbbe8;color:white;"><span class="fa fa-plus"></span>IMPRIMIR FORMULARIO</a><br>
        </div>
     </div>
    
     <hr>
    
     <h5>PASO 3</h5><h6>Adjunte Ticket,DNI-FRENTE,DNI-DORSO,Boleta de Servicio ó Contrato de Alquiler</h6>
     <div class="row">
      <div class="col-md-12">
         <a href="<?php echo base_url();?>mantenimiento/certificado/load" class="btn btn-flat" style="background-color:#BCD46C;color:white;"><span class="fa fa-plus"></span>AGREGAR DOCUMENTOS</a><br>
      </div>
       <hr>
     <h5>PASO 4 </h5><h6>Imprima su Certificado a las 24 horas habiles</h6>
     <div class="row">
     
      <div class="col-md-12">
        
        <a href="<?php echo base_url();?>mantenimiento/certificado/printcertificado" class="btn btn-flat" style="background-color:#edc73f;color:white;"><span class="fa fa-plus"></span>IMPRIMIR CERTIFICADO</a>
      </div>
     </div>
    </div>
    <!-- /.box-body -->
   </div>
  <!-- /.box -->
 </section>
 <!-- /.content -->
 </div>
<!-- /.content-wrapper -->