 <!-- Content Wrapper. Contains page content -->
 <div class="content-wrapper">
 <!-- Content Header (Page header) -->
 <section class="content-header">
 <h1>REPORTE DE CERTIFICADOS DE DOMICILIO GENERADOS</h1><br>
 <br>
 </section>
<!-- Main content -->
<section class="content">
 <!-- Default box -->
  <div class="box box-solid">
    <div class="box-body">
    <div class="row">
    <div class="col-md-5">  
    <div class="form-inline" >
    <form action="<?php echo base_url();?>mantenimiento/certificado/listaCertReal" method="POST" >
    <div class="form-group">
      <label for="">FECHA</label>
       <input type="date" class="form-control" id="fechainicio" name="fechainicio" required="required" value="<?php echo date("Y-m-d"); ?>" >
       <input type="date" class="form-control" id="fechafin" name="fechafin" required="required" value="<?php echo date("Y-m-d"); ?>" >

    </div>
    </div>
    </div>
    <br>
    </div>
    <br>
    <div class="row">
      <div class="col-md-4">
      <button type="submit" class="btn btn-success btn-block btn-flat" name="volver">CONSULTAR</button>
      </div>
    </form>
    <form action="<?php echo base_url()?>mantenimiento/certificado/principalDom" method="post">
      <div class="col-md-4">
         <button type="submit" class="btn btn-danger btn-block btn-flat" name="volver">VOLVER</button>
      </div>
    </form>
   </div><br>
   <div class="row">
    <div class="col-md-12">
    <table id="cndend" class="table table-bordered btn-hover">
    <caption><b>LISTADO DE CERTIFICADOS ON-LINE ESTADO TERMINADO desde el:  <?php if(!empty($desde)){echo $desde;} ?> al : <?php if(!empty($hasta)){echo $hasta;} ?></b></caption>
     <thead>
      <tr>
        <th style="font-size:75%">FORMULARIO</th><th style="font-size:75%" >DNI</th> <th style="font-size:75%">APELLIDO</th><th style="font-size:75%">NOMBRE</th><th style="font-size:75%">CONTACTO</th><th style="font-size:75%">CORREO</th><th style="font-size:75%">COMPROBANTE</th><th style="font-size:75%">FECHA</th><th style="font-size:75%">HORA</th><th style="font-size:75%">MINUTO</th><th style="font-size:75%">TIPO</th><th style="font-size:75%">REALIZADO</th><th style="font-size:75%">ABONO</th><th style="font-size:75%">TICKETS</th>
        </tr>
        </thead>
        <tbody> 
        <?php if(!empty($listado)):?>
        <?php foreach($listado as $lista):?>
        <tr> <td style="font-size:75%"><?php echo $lista->id; ?></td>
        <td style="font-size:75%"><?php echo $lista->dni;?></td>
        <td style="font-size:75%"><?php echo $lista->apellido;?></td>
        <td style="font-size:75%"><?php echo $lista->nombre;?></td>
        <td style="font-size:75%"><?php echo $lista->celular;?></td>
        <td style="font-size:75%"><?php echo $lista->correo;?></td>
        <td style="font-size:75%"><?php echo $lista->transaccion; ?></td><td style="font-size:75%"><?php echo $lista->fectransac; ?></td><td style="font-size:75%"><?php echo $lista->horatransac; ?></td><td style="font-size:75%"><?php echo $lista->minutransac; ?></td><td style="font-size:75%"><?php echo $lista->tipo; ?></td><td style="font-size:75%"><?php echo $lista->realizo; ?></td><td style="font-size:75%"><?php echo $lista->abono;?></td><td style="font-size:75%"><?php echo $lista->tk;?></td> 
        </tr>
        <?php endforeach;?>
        <?php endif;?>
         </tbody>
       </table>
       </div>
     </div>
    </div>
    <!-- /.box-body -->
   </div>
  <!-- /.box -->
 </section>
 <!-- /.content -->
 </div>
<!-- /.content-wrapper -->