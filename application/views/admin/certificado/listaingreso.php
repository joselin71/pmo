<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
 <!-- Content Header (Page header) -->
 <section class="content-header">
 <h1>SOLICITUD DE CERTIFICADOS DE DOMICLIO POR ESTADO</h1>
 <br>
 </section>
 <!-- Main content -->
 <section class="content">
 <!-- Default box -->
 <div class="box box-solid">
  <div class="box-body">
   <div class="row">
    <div class="col-md-3">  
    <div class="form-inline" >
     <form action="<?php echo base_url();?>mantenimiento/certificado/listaingreso" method="POST">
     <div class="form-group">
      <label for="">ESTADO</label><select class="form-control" name="estado" size=1 required="required"><option value='CARGADO'>CARGADO</option><option value='INGRESADO'>INGRESADO</option><option value='TRATADO'>TRATADO</option></select>
     </div>
    </div> 
    </div>
    <div class="col-md-5">  
    <div class="form-inline" >
      <div class="form-group">
       <label for="">FECHA</label>
       <input type="date" class="form-control" id="bddesde" name="bddesde" required="required" value="<?php echo date("Y-m-d"); ?>" >
       <input type="date" class="form-control" id="bdhasta" name="bdhasta" required="required" value="<?php echo date("Y-m-d"); ?>" >
      </div>
    </div>
    </div>
</div>
    <br>
    <div class="row">
       <div class="col-md-4">
        <button type="submit" class="btn btn-success btn-block btn-flat" name="volver">CONSULTAR</button>
       </div>
      </form>
      <form action="<?php echo base_url()?>mantenimiento/certificado/principalDom" method="post">
     <div class="col-md-4">
      <button type="submit" class="btn btn-danger btn-block btn-flat" name="volver">VOLVER</button>
     </div>
     </form>
    </div>
  <br>
    <div class="row">
       <div class="col-md-12">
       <table id="example" class="table table-bordered btn-hover">
        <thead>
        <tr>
        <th>FORMULARIO</th><th>FECHA</th><th>DNI</th> <th>APELLIDO</th><th>NOMBRE</th> <th>ESTADO</th><th>F.NACIMIENTO</th><th>CONTACTO</th><th>CORREO</th>
        </tr>
        </thead>
        <tbody> 
        <?php if(!empty($listado)):?>
        <?php foreach($listado as $lista):?>
        <tr> <td><?php echo $lista->id; ?></td><td><?php echo $lista->saco;?></td><td><?php echo $lista->dni;?></td><td><?php echo $lista->apellido;?></td><td><?php echo $lista->nombre;?></td><td><?php echo $lista->estado;?></td><td><?php echo $lista->nacido;?></td><td><?php echo $lista->celular;?></td><td><?php echo $lista->correo;?></td> 
        </tr>
        <?php endforeach;?>
        <?php endif;?>
         </tbody>
       </table>
       </div>
     </div>
    </div>
    <!-- /.box-body -->
   </div>
  <!-- /.box -->
 </section>
 <!-- /.content -->
 </div>
<!-- /.content-wrapper -->