<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
 <!-- Content Header (Page header) -->
 <section class="content-header">
 <h1>Carga de documentos anexos</h1>
 <h4>Los archivos son 4(cuatro):TICKET(con numero de comprobante,fecha y hora),DNI-FRENTE,DNI-DORSO.Si el Domicilio del DNI y el de la Solicitud coinciden NO DEBE CARGAR :Boleta de Servicio ó Contrato de Alquiler ó Información Sumaria</h4>
 <h5>Deberá cargar de a 1 por vez los archivos solicitados</h5>
 </section>
 <!-- Main content -->
 <section class="content">
 <!-- Default box -->
  <div class="box box-solid">
    <div class="box-body">
     
     <div class="row">
       <div class="col-md-12">
       <?php if($this->session->flashdata("error")):?>
            <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <p><i class="icon fa fa-ban"></i><?php echo $this->session->flashdata("error")?></p>
            </div>
            <?php endif ?>
       <form action="<?php echo base_url();?>mantenimiento/certificado/loadf" method="POST" enctype="multipart/form-data">
       <h6 align="center" class="title_header" style="color:blue;"><strong>INGRESE EL DNI Y EL TIPO DE DOCUMENTACION A ENVIAR-LOS ARCHIVOS NO DEBEN SUPERAR LOS 2 MB DE TAMAÑO</strong></h6>
       <div class="form-group">
        <label for="">INGRESE TIPO DE ARCHIVO</label>
        <select class="form-control" name="tarc" size=1 required="required">
         <option value=''>Seleccione</option>
         <option value='1'>TICKET ABONADO</option>
         <option value='2'>DNI-FRENTE</option>
         <option value='3'>DNI-DORSO</option>
         <option value='4'>BOLETA DE SERVICIO ó CONTRATO ó INFORMACION SUMARIA</option>
        </select>
       </div>

       <div class="form-group">
       <label for="">DOCUMENTO</label>
       <input type="text" class="form-control" name="dniS" type="text" id="dniS" placeholder="Solo numeros"  onkeypress="return numeros(event)"  required="required" onPaste="return false" maxlength="8" >
       </div>
       <div class="form-group">
       <label for="">SELECCIONE ARCHIVO</label>
       <input class="form-control" name="userfile" type="file"  size="10" required="required">
       </div>
              
       <div class="form-group">
       <button type="submit" class="btn btn-block btn-success btn-flat" >CARGAR DOCUMENTOS</button>
       </div>
       </form>
       <form action="<?php echo base_url()?>mantenimiento/certificado" method="post">
        <div class="row">
        <div class="col-xs-12">
         <button type="submit" class="btn btn-danger btn-block btn-flat" name="volver">VOLVER</button>
       </div><!-- /.col -->
       </div>
       </form><br>
     </div>
    </div>
    <!-- /.box-body -->
   </div>
  <!-- /.box -->
 </section>
 <!-- /.content -->
 </div>
<!-- /.content-wrapper -->