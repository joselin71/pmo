<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
 <!-- Content Header (Page header) -->
 <section class="content-header">
 <h2>Boleta de Servicio ó Contrato de Alquiler ó Información Sumaria</h2>
 <h4>Adjunte una imagen .Si el Domicilio del DNI y el de la Solicitud coinciden NO DEBE CARGAR</h4>
 <!--h5>Deberá cargar de a 1 por vez los archivos solicitados</h5-->
 </section>
 <!-- Main content -->
 <section class="content">
 <!-- Default box -->
  <div class="box box-solid">
    <div class="box-body">
     
     <div class="row">
       <div class="col-md-12">
       <?php if($this->session->flashdata("error")):?>
            <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <p><i class="icon fa fa-ban"></i><?php echo $this->session->flashdata("error")?></p>
            </div>
            <?php endif ?>
       <form action="<?php echo base_url();?>mantenimiento/certificado/loadbt" method="POST" enctype="multipart/form-data">
       <h6 align="center" class="title_header" style="color:blue;"><strong>EL ARCHIVO NO DEBE SUPERAR LOS 2 MB DE TAMAÑO</strong></h6>
       <div class="form-group">
       <label for="">DNI SOLICITANTE</label>
       <input type="text" class="form-control" name="dniS" type="text" id="dniS" value="<?php echo $this->session->userdata("tramite"); ?>"  required="required" readonly="readonly">
       </div>
       <div class="form-group">
       <label for="">CARGUE IMAGEN DE LA BOLETA</label>
       <input class="form-control" name="image" type="file" id="image" size="10" required="required" >
       <input type="hidden" id="image-temp" name="image">
       </div>
              
       <div class="form-group">
       <button type="submit" class="btn btn-block btn-success btn-flat" >CARGAR BOLETA DE SERVICIO Y TERMINAR</button>
       </div>
       </form>
       <form action="<?php echo base_url()?>mantenimiento/certificado" method="post">
        <div class="row">
        <div class="col-xs-12">
         <button type="submit" class="btn btn-danger btn-block btn-flat" name="volver">SALIR SIN CARGAR BOLETA DE SERVICIO</button>
       </div>
       </div>
       </form><br>
     </div>
    </div>
    <!-- /.box-body -->
   </div>
  <!-- /.box -->
 </section>
 <!-- /.content -->
 </div>
<!-- /.content-wrapper -->