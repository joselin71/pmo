<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
 <!-- Content Header (Page header) -->
 <section class="content-header">
 <h2>FRENTE D.N.I.</h2>
 <h4>Adjunte una imagen del FRENTE de su D.N.I en formato .JPG</h4>
 <!--h5>Deberá cargar de a 1 por vez los archivos solicitados</h5-->
 </section>
 <!-- Main content -->
 <section class="content">
 <!-- Default box -->
  <div class="box box-solid">
    <div class="box-body">
     
     <div class="row">
       <div class="col-md-12">
       <?php if($this->session->flashdata("error")):?>
            <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <p><i class="icon fa fa-ban"></i><?php echo $this->session->flashdata("error")?></p>
            </div>
            <?php endif ?>
       <form action="<?php echo base_url();?>mantenimiento/certificado/loadfft" method="POST" enctype="multipart/form-data">
       <h6 align="center" class="title_header" style="color:blue;"><strong>EL ARCHIVO NO DEBE SUPERAR LOS 2 MB DE TAMAÑO</strong></h6>
       <div class="form-group">
       <label for="">DNI SOLICITANTE</label>
       <input type="text" class="form-control" name="dniS" type="text" id="dniS" value="<?php echo $this->session->userdata("tramite"); ?>"  required="required" readonly="readonly">
       </div>
       <div class="form-group">
       <label for="">CARGUE IMAGEN DEL FRENTE DEL D.N.I</label>
       <input class="form-control" name="image" type="file" id="image" size="10" required="required" >
       <input type="hidden" id="image-temp" name="image">
       </div>              
       <div class="form-group">
       <button type="submit" class="btn btn-block btn-success btn-flat" >SIGUIENTE</button>
       </div>
       </form>
       <br>
     </div>
    </div>
    <!-- /.box-body -->
   </div>
  <!-- /.box -->
 </section>
 <!-- /.content -->
 </div>
<!-- /.content-wrapper -->