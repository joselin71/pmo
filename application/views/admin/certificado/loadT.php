<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
 <!-- Content Header (Page header) -->
 <section class="content-header">
 <h2>TICKET</h2>
 <h4>Adjunte una imagen del TICKET(con numero de comprobante,fecha y hora) en formato .JPG</h4>
 <h4 style="color:red;">INGRESAR EN FORMA CORRECTA EL DNI DEL SOLICITANTE UNA SOLA VEZ</h4>
 </section>
 <!-- Main content -->
 <section class="content">
 <!-- Default box -->
  <div class="box box-solid">
    <div class="box-body">
     
     <div class="row">
       <div class="col-md-12">
       <?php if($this->session->flashdata("error")):?>
            <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <p><i class="icon fa fa-ban"></i><?php echo $this->session->flashdata("error")?></p>
            </div>
            <?php endif ?>
       <form action="<?php echo base_url();?>mantenimiento/certificado/loadf" method="POST" enctype="multipart/form-data" >
       <h6 align="center" class="title_header" style="color:blue;"><strong>EL ARCHIVO NO DEBE SUPERAR LOS 2 MB DE TAMAÑO</strong></h6>
       <div class="form-group">
       <label for="">INGRESE NUMERO DOCUMENTO</label>
       <input type="text" class="form-control" name="dniS" type="text" id="dniS" placeholder="Solo numeros"  onkeypress="return numeros(event)"  required="required" onPaste="return false" maxlength="8" >
       </div>
       <div class="form-group">
       <label for="">CARGUE IMAGEN DEL TICKET</label>
       <input class="form-control" name="image" type="file" id="image" size="10" required="required" >
       <input type="hidden" id="image-temp" name="image">
      </div>
      </div>
      <div class="form-group">
       <button type="submit" class="btn btn-block btn-success btn-flat" >SIGUIENTE</button>
       </div>
       </form>
       <br>
     </div>
    </div>
    <!-- /.box-body -->
   </div>
  <!-- /.box -->
 </section>
 <!-- /.content -->
 </div>
<!-- /.content-wrapper -->