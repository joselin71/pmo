<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
 <!-- Content Header (Page header) -->
 <section class="content-header">
 <h1>Impresion de Certificado de Domicilio</h1>
  </section>
 <!-- Main content -->
 <section class="content">
 <!-- Default box -->
  <div class="box box-solid">
    <div class="box-body">
     
     <div class="row">
       <div class="col-md-12">
       <?php if($this->session->flashdata("error")):?>
            <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <p><i class="icon fa fa-ban"></i><?php echo $this->session->flashdata("error")?></p>
            </div>
            <?php endif ?>
       <form action="<?php echo base_url();?>mantenimiento/certificado/printc" method="POST">
       <h6 align="center" class="title_header" style="color:blue;"><strong>INGRESE EL DNI Y EL NUMERO DE TRAMITE CARGADO EN LA SOLICITUD</strong></h6>
       <div class="form-group">
       <label for="">DOCUMENTO</label>
       <input type="text" class="form-control" name="dniS" type="text" id="dniS" placeholder="Solo numeros"  onkeypress="return numeros(event)"  required="required" onPaste="return false" maxlength="8" >
       </div>
       <div class="form-group">
       <label for="">TRAMITE</label>
       <input type="text" class="form-control" name="tramdniS" type="text" id="tramdniS" placeholder="El numero completo que figura en el DNI"  onkeypress="return numeros(event)"  required="required" onPaste="return false" maxlength="11" >
       </div>
       
       <div class="form-group">
       <button type="submit" class="btn btn-block btn-success btn-flat" >BUSCAR CERTIFICADO</button>
       </div>
       </form>
       <form action="<?php echo base_url()?>mantenimiento/certificado" method="post">
        <div class="row">
        <div class="col-xs-12">
         <button type="submit" class="btn btn-danger btn-block btn-flat" name="volver">VOLVER</button>
       </div><!-- /.col -->
       </div>
       </form><br>
     </div>
    </div>
    <!-- /.box-body -->
   </div>
  <!-- /.box -->
 </section>
 <!-- /.content -->
 </div>
<!-- /.content-wrapper -->