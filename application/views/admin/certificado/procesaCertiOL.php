 <!-- Content Wrapper. Contains page content -->
 <div class="content-wrapper">
 <!-- Content Header (Page header) -->
 <section class="content-header">
 <h1>SOLICITUD DE CERTIFICADOS DE DOMICILIOS INGRESADOS</h1>
 <form action="<?php echo base_url()?>mantenimiento/certificado/principalDom" method="post">
 <div class="row">
 <div class="col-xs-2">
 <button type="submit" class="btn btn-danger btn-block btn-flat" name="volver">VOLVER</button>
 </div>
 </form><br>
 </section>
 <!-- Main content -->
 <section class="content">
 <!-- Default box -->
  <div class="box box-solid">
    <div class="box-body">
     <div class="row">
       <div class="col-md-12">
       <table id="example" class="table table-condensed table-bordered btn-hover">
        <thead>
        <tr>
        <th style="font-size:75%">FORMULARIO</th><th style="font-size:75%">DNI</th> <th style="font-size:75%">APELLIDO</th><th style="font-size:75%">NOMBRE</th><th style="font-size:75%">F.NACIMIENTO</th><th style="font-size:75%">CONTACTO</th><th style="font-size:75%">CORREO</th><th style="font-size:75%">SACO</th><th style="font-size:75%">TK</th><th style="font-size:75%">DNIF</th><th style="font-size:75%">DNID</th><th style="font-size:75%">BS</th><th style="font-size:75%">ESTADO</th><th style="font-size:75%">Observa</th><th style="font-size:75%">Opciones</th>
        </tr>
        </thead>
        <tbody> 
        <?php if(!empty($listado)):?>
        <?php foreach($listado as $lista):?>
          <?php 
          $tk="NO";$df="NO";$dd="NO";$bs="NO";
          $dni=$lista->dni;$ruta=base_url().'/uploads/domicilio/';
          $nombre_fichero=$ruta.'T-'.$dni.'.jpg';
          if (file_exists($nombre_fichero)){$tk="SI";}
          $nombre_fichero=$ruta.'F-'.$dni.'.jpg';
          if (file_exists($nombre_fichero)){$df="SI";}
          $nombre_fichero=$ruta.'D-'.$dni.'.jpg';
          if (file_exists($nombre_fichero)){$dd="SI";}
          $nombre_fichero=$ruta.'B-'.$dni.'.jpg';
          if (file_exists($nombre_fichero)){$bs="SI";}
            ?>
        <tr> <td style="font-size:75%"><?php echo $lista->id; ?></td><td style="font-size:75%"><?php echo $lista->dni;?></td><td style="font-size:75%"><?php echo $lista->apellido;?></td><td style="font-size:75%"><?php echo $lista->nombre;?></td><td style="font-size:75%"><?php echo $lista->nacido;?></td><td style="font-size:75%"><?php echo $lista->celular;?></td><td style="font-size:75%"><?php echo $lista->correo;?></td><td style="font-size:75%"><?php echo $lista->saco;?></td>
        <td style="font-size:75%"><?php echo $tk;?></td>
        <td style="font-size:75%"><?php echo $df;?></td>
        <td style="font-size:75%"><?php echo $dd;?></td>
        <td style="font-size:75%"><?php echo $bs;?></td>
        <td style="font-size:75%"><?php echo $lista->estado;?></td>
        <td style="font-size:75%"><?php echo $lista->observaciones ?></td>
        <td style="font-size:75%">
        <div class="btn-group" style="font-size:75%">
        <button type="button" class="btn btn-info btn-view-dataDom btn-xs" value="<?php echo $lista->id;?>" data-toggle="modal" data-target="#modal-default" >
        <span class="fa fa-print"></span>
      </button>
        
        <a href="<?php echo base_url();?>mantenimiento/certificado/editCert/<?php echo $lista->id;?>" class="btn btn-warning btn-xs"><span class="fa fa-pencil"></span></a>
        
        </div>
        
        </td>




        </tr>
        <?php endforeach;?>
        <?php endif;?>
         </tbody>
       </table>
       </div>
     </div>
    </div>
    <!-- /.box-body -->
   </div>
  <!-- /.box -->
 </section>
 <!-- /.content -->
 </div>
<!-- /.content-wrapper -->
<div class="modal fade" id="modal-default">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
              </button>
              <h4 class="modal-title">Datos del Solicitante</h4>
            </div>
            <div class="modal-body">
              
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Cerrar</button>
              <button type="button" class="btn btn-primary btn-print"><span class="fa fa-pencil"> </span>Imprimir</button-->
              
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->

      