<div class="box-body">
<form action="<?php echo base_url();?>mantenimiento/certificado/updatestore" method="POST" autocomplete="off">
 <div class="row">
  <div class="col-md-4">
   <?php if($this->session->flashdata("error")):?>
   <div class="alert alert-danger alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <p><i class="icon fa fa-ban"></i><?php echo $this->session->flashdata("error")?></p>
   </div>
   <?php endif ?>
   <div class="form-group">
    <label for="">FORMULARIO DE SOLICITUD</label>
     <input type="text" class="form-control" name="formu" type="text" id="formu" value="<?php if(!empty($data)){echo $data->id;} ?>" readonly="readonly" required="required">
   </div>
   </div>
   </div>
   <div class="row">
   <div class="col-md-4">
   <div class="form-group">
    <label for="">DOCUMENTO-Ingrese el numero sin puntos</label>
     <input type="text" class="form-control" name="dni" type="text" id="dni" value="<?php if(!empty($data)){echo $data->dni;} ?>" onkeypress="return numeros(event)"  required="required" onPaste="return false" maxlength="8" >
   </div>
   </div>
  <div class="col-md-4">
    <div class="form-group">
     <label for="">TRAMITE-Ingrese los 11 digitos(ceros incluidos)</label>
     <input type="text" class="form-control" name="tramdni" type="text" id="tramdni"  value="<?php if(!empty($data)){echo $data->tramdni;} ?>"  onkeypress="return numeros(event)"  required="required" onPaste="return false"  minlength="11"  maxlength="11" pattern="[0-9]*">
    </div>
   </div>
</div>
<div class="row">
  <div class="col-md-4">
   <div class="form-group">
    <label for="">APELLIDOS</label>
    <input type="text" class="form-control" name="apellido" type="text" id="apellido"  value="<?php if(!empty($data)){echo $data->apellido;
       } ?>"  onkeyup="javascript:this.value=this.value.toUpperCase();" required="required" maxlength="58" onPaste="return false" >
   </div>
  </div>
  <div class="col-md-4">
   <div class="form-group">
    <label for="">NOMBRES</label>
     <input type="text" class="form-control" name="nombre" type="text" id="nombre"  value="<?php if(!empty($data)){echo $data->nombre;}?>"  onkeyup="javascript:this.value=this.value.toUpperCase();" required="required" maxlength="44" onPaste="return false">
   </div>
  </div>
  </div>
  <div class="row">
      <div class="col-md-8">    
       <div class="form-group">
       <button type="submit" class="btn btn-block btn-success btn-flat" >CORREGIR DATOS</button>
       </div>
       </form>
      <br>
       </div>
  </div>
     </div>
    </div>
    <!-- /.box-body -->
   </div>
  <!-- /.box -->
 </section>
 <!-- /.content -->
 </div>
<!-- /.content-wrapper -->