<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
 <!-- Content Header (Page header) -->
 <section class="content-header">
 <h1>CAMBIAR ESTADO CERTIFICADOS DE DOMICILIO ESTADO TERMINADO/ANULADO</h1>
 <form action="<?php echo base_url()?>mantenimiento/certificado/principalDom" method="post">
 <div class="row">
 <div class="col-xs-2">
 <button type="submit" class="btn btn-danger btn-block btn-flat" name="volver">VOLVER</button>
 </div>
 </form><br>
 </section>
 <!-- Main content -->
 <section class="content">
 <!-- Default box -->
  <div class="box box-solid">
    <div class="box-body">
     <div class="row">
       <div class="col-md-12">
       <table id="terminado" class="table table-condensed table-bordered btn-hover">
        <thead>
        <tr>
        <th style="font-size:75%">FORMULARIO</th>
        <th style="font-size:75%">REALIZADO</th>
        <th style="font-size:75%">DNI</th> <th style="font-size:75%">APELLIDO</th><th style="font-size:75%">NOMBRE</th>
        <th style="font-size:75%">ESTADO</th>
        <th style="font-size:75%">Opciones</th>
        </tr>
        </thead>
        <tbody> 
        <?php if(!empty($listado)):?>
        <?php foreach($listado as $lista):?>
          
        <tr> <td style="font-size:75%"><?php echo $lista->id; ?></td>
        <td style="font-size:75%"><?php echo $lista->realizo;?></td>
        <td style="font-size:75%"><?php echo $lista->dni;?></td><td style="font-size:75%"><?php echo $lista->apellido;?></td><td style="font-size:75%"><?php echo $lista->nombre;?></td>
        <td style="font-size:75%"><?php echo $lista->estado;?></td>
        <td style="font-size:75%">
        <a href="<?php echo base_url();?>mantenimiento/certificado/editRCert/<?php echo $lista->id;?>" class="btn btn-warning btn-xs"><span class="fa fa-pencil"></span></a>
      </div>
      </td>
      </tr>
        <?php endforeach;?>
        <?php endif;?>
         </tbody>
       </table>
       </div>
     </div>
    </div>
    <!-- /.box-body -->
   </div>
  <!-- /.box -->
 </section>
 <!-- /.content -->
 </div>
<!-- /.content-wrapper -->
<div class="modal fade" id="modal-default">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
              </button>
              <h4 class="modal-title">Datos del Solicitante</h4>
            </div>
            <div class="modal-body">
              
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Cerrar</button>
              <button type="button" class="btn btn-primary btn-print"><span class="fa fa-pencil"> </span>Imprimir</button-->
              
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->
