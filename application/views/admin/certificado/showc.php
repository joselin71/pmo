<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
 <!-- Content Header (Page header) -->
 <section class="content-header">
 <h1>Impresion de Certificado de Domicilio</h1>
 <form action="<?php echo base_url()?>mantenimiento/certificado/printcertificado" method="post">
 <div class="row">
  <div class="col-xs-12">
  <button type="submit" class="btn btn-danger btn-block btn-flat" name="volver">VOLVER</button>
  </div><!-- /.col -->
  </div>
  </form><br>
 </section>
 <!-- Main content -->
 <section class="content">
 <!-- Default box -->
  <div class="box box-solid">
    <div class="box-body">
     
     <div class="row">
       <div class="col-md-12">
       <?php if($this->session->flashdata("error")):?>
            <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <p><i class="icon fa fa-ban"></i><?php echo $this->session->flashdata("error")?></p>
            </div>
            <?php endif ?>
            <iframe src="<?php echo $ff;?>" width="60%" height="500px"></iframe>
      
       
     </div>
    </div>
    <!-- /.box-body -->
   </div>
  <!-- /.box -->
 </section>
 <!-- /.content -->
 </div>
<!-- /.content-wrapper -->