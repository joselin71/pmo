<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
 <h1>Carga de Certificado Generado</h1><br>
 </section>
 <!-- Main content -->
 <section class="content">
 <!-- Default box -->
  <div class="box box-solid">
    <div class="box-body">
     
     <div class="row">
       <div class="col-md-12">
       <?php if($this->session->flashdata("error")):?>
            <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <p><i class="icon fa fa-ban"></i><?php echo $this->session->flashdata("error")?></p>
            </div>
            <?php endif ?>
       <form action="<?php echo base_url();?>mantenimiento/certificado/uparcFCert" method="POST" enctype="multipart/form-data">
       <div class="form-group">
       <label for="">DOCUMENTO</label>
       <input type="text" class="form-control" name="dniS" type="text" id="dniS" placeholder="Solo numeros"  onkeypress="return numeros(event)"  required="required" onPaste="return false" maxlength="10" >
       </div>
       <div class="form-group">
       <label for="">SELECCIONE ARCHIVO PDF</label>
       <input class="form-control" name="userfile" type="file"  size="10" required="required">
       </div>
                     
       <div class="form-group">
       <button type="submit" class="btn btn-block btn-success btn-flat" >CARGAR CERTIFICADO</button>
       </div>
       </form>
       <form action="<?php echo base_url()?>mantenimiento/certificado/principalDom" method="post">
        <div class="row">
        <div class="col-xs-12">
         <button type="submit" class="btn btn-danger btn-block btn-flat" name="volver">VOLVER</button>
       </div><!-- /.col -->
       </div>
       </form><br>
     </div>
    </div>
    <!-- /.box-body -->
   </div>
  <!-- /.box -->
 </section>
 <!-- /.content -->
 </div>
<!-- /.content-wrapper -->