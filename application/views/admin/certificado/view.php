<?php
 list($ano,$mes,$dia) = explode("-",$info->nacido);
 $edad  = date("Y") - $ano;
 $mes_diferencia = date("m") - $mes;
 $dia_diferencia   = date("d") - $dia;
 if ($dia_diferencia < 0 || $mes_diferencia < 0)
  $edad--;
?>

<div class="row">
 <div class="col-xs-12 text-center">
 <b>POLICIA DEL CHACO-CERTIFICADO DE DOMICILIO</b><br>
 <b>FECHA IMPRESION:</b>
 <?php echo date("j/n/Y");?>
</div>
</div> <br>
<div class="row">
<div class="col-xs-12">
<table class="table table-bordered">
<caption>DATOS SOLICITANTE</caption>
<tbody>
<tr>
<td>Formulario.</td><td><?php echo $info->id;?></td>
</tr>
<tr>
<td>Fecha Solicitud.</td><td><?php echo $info->saco;?></td>
</tr>
<tr><td>DNI</td><td><?php echo $info->dni;?></td></tr>
<tr><td>APELLIDOS</td><td><?php echo $info->apellido;?></td></tr>
<tr><td>NOMBRES</td><td><?php echo $info->nombre;?></td></tr>
<tr><td>FECHA NACIMIENTO</td><td><?php echo $info->nacido;?></td></tr>
<tr><td>EDAD</td><td><?php echo $edad;?></td></tr>
<tr><td>DOMICILIO</td><td><?php echo $info->domicilio;?></td></tr>
<tr><td>LOCALIDAD</td><td><?php echo $info->localidad;?></td></tr>
<tr><td>PROVINCIA/PAIS</td><td><?php echo $info->provincia.",".$info->pais;?></td></tr>
<tr><td>LUGAR</td><td><?php echo $info->presenta;?></td></tr>
<tr><td>CEL/TEL</td><td><?php echo $info->celular;?></td></tr>
<tr><td>CORREO</td><td><?php echo $info->correo;?></td></tr>
</tbody>
</table>
</div>
</div>

