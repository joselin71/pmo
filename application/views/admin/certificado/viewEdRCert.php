 <!-- Content Wrapper. Contains page content -->
 <div class="content-wrapper">
 <!-- Content Header (Page header) -->
 <section class="content-header">
 <h1>DATOS SOLICITANTE<small>Actualizar Estado</small></h1>
 <form action="<?php echo base_url()?>mantenimiento/certificado/reprocesaCertificadoOL" method="post">
 <div class="row">
 <div class="col-xs-2">
 <button type="submit" class="btn btn-danger btn-block btn-flat" name="volver">VOLVER</button>
 </div>
 </form><br>
 </section>
 <!-- Main content -->
 <section class="content">
 <!-- Default box -->
  <div class="box box-solid">
    <div class="box-body">
     <div class="row">
       <div class="col-md-12">
       <?php if($this->session->flashdata("error")):?>
            <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <p><i class="icon fa fa-ban"></i><?php echo $this->session->flashdata("error")?></p>
            </div>
            <?php endif ?>
       <form action="<?php echo base_url();?>mantenimiento/certificado/updateEdRCert" method="POST">
       <input type="hidden" value="<?php echo $lista->id; ?>" name="idCod">
       <div class="form-group">
       <label for="cod">Formulario:</label>
       <input type="text" class="form-control" readonly="readonly" style="width:50%;" value="<?php echo $lista->id;?>">
       </div>
       <div class="form-group">
       <label for="cod">Fecha Solicitud:</label>
       <input type="text" class="form-control" readonly="readonly" style="width:50%;" value="<?php echo $lista->saco;?>">
       </div>
       <div class="form-group">
       <label for="cod">DNI:</label>
       <input type="text" class="form-control" readonly="readonly" style="width:50%;" value="<?php echo $lista->dni;?>">
       </div>
       <div class="form-group">
       <label for="cod">APELLIDOS:</label>
       <input type="text" class="form-control" readonly="readonly" style="width:50%;" value="<?php echo $lista->apellido;?>">
       </div>
       <div class="form-group">
       <span class=""><label for="cod">NOMBRES:</label>
       <input type="text" class="form-control" readonly="readonly" style="width:50%;" value="<?php echo $lista->nombre;?>">
       </div>
       <div class="form-group">
       <label for="cod">ESTADO:</label>
       <select required="required" name="estad" id="estad" >
       <option value='1'>INGRESADO</option>
       </select></span>
       </div>
       <div class="form-group">
       <label for="cod">Observaciones:</label>
       <textarea name="observa" id="observa" cols="60" rows="4"   maxlength="85"  onkeyup="javascript:this.value.toUpperCase()"><?php echo $lista->observaciones;?></textarea>
       </div>
       <div class="row">
        <div class="col-xs-2">
        <button type="submit" class="btn btn-success btn-block btn-flat" name="volver">ACTUALIZAR</button>
        </div>
       </form>
       </div>
     </div>
    </div>
    <!-- /.box-body -->
   </div>
  <!-- /.box -->
 </section>
 <!-- /.content -->
 </div>
<!-- /.content-wrapper -->
