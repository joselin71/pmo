<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
 <!-- Content Header (Page header) -->
 <section class="content-header">
 <h1>Formulario de Carga de Pedido</h1>
 </section>
 <!-- Main content -->
 <section class="content">
 <!-- Default box -->
  <div class="box box-solid">
    <div class="box-body">
    <div class="row">
      <div class="col-md-12">
        <a href="<?php echo base_url();?>mantenimiento/materiales/adddetalle" class="btn btn-primary btn-flat"><span class="fa fa-plus"></span>Agregar Bien / Servicio</a>
      </div>
     </div><hr>
     <div class="row">
     <div class="col-md-12">
     <table id="detalleped" class="table table-bordered btn-hover" style="width:100%">
     <thead>
        <tr align="center"><th>CANTIDAD</th><th>P.V. UNITARIO $</th>
        <th>DESCRIPCION BIEN / SERVICIO</th><th>IMPORTE ESTIMADO $</th><th>Opciones</th>
        </tr>
        </thead>
        <tbody class="t-body"> 
        <?php  $tmontot=0;
        if(!empty($pedido)):
         ?>
        <?php foreach($pedido as $ped):?>
        <tr> 
        <td align="right"><?php echo $ped->cantidad; ?></td>
        <td align="right"><?php echo $ped->importelinea;?></td>
        <td ><?php echo $ped->detallepedido;?></td>
        <td align="right"><?php echo $ped->importedetalle;?></td>
        <td>
        <div class="btn-group">
        <a href="<?php echo base_url();?>mantenimiento/materiales/edit/<?php echo $ped->iddetallepm;?>" class="btn btn-warning"><span class="fa fa-pencil"></span></a>
        <a href="<?php echo base_url();?>mantenimiento/materiales/delete/<?php  echo $ped->iddetallepm;?>" class="btn btn-danger btn-remove"><span class="fa fa-remove"></span></a>
        </div>
        </td>
        </tr>
        <?php 
        $tmontot=$tmontot+$ped->importedetalle;    
        endforeach;?>
        <?php endif;?>
        <tr><td colspan="5" class="total" style="text-align: left; padding: .8em .8em .8em 2em;"><h3>TOTAL $:<?php echo number_format($tmontot,2,'.','');?></h3></td></tr>
         </tbody></table>
     </div>
     </div>
     <hr>
     <div class="row">
       <div class="col-md-12">
       <form action="<?php echo base_url();?>mantenimiento/materiales/storecab" method="POST">
       <div class="col-md-4"><label for="dni">SECRETARIA</label>
       <select name="secret" id="secret" size=1 class="form-control" required="required" >
       <option value="">Seleccione</option><option value="<?php echo $secretaria;?>"><?php echo $secretariaN;?></option>
       </select>
       </div>
       <div class="col-md-7">
       <label for="">SUBSECRETARIA</label>
       <select name="subsecret"  id="subsecret" size=1 class="form-control" required="required" >
       <option value="">Seleccione</option>
       <?php if(!empty($dependencia)):?>
        <?php foreach($dependencia as $dep):?>
         <option value="<?php echo $dep->subsec;?>"><?php echo $dep->detsubsec;?></option>
         <?php endforeach;?>
        <?php endif;?>
       </select>
      </div>
      <div class="col-md-11">
       <label for="">DIRECCION GENERAL</label>
       <select name="dirgral"  id="dirgral" size=1 class="form-control" required="required" >
       <option value="">Seleccione</option><?php if(!empty($dependencia)):?><?php foreach($dependencia as $dep):?>
         <option value="<?php echo $dep->dirgral;?>"><?php echo $dep->dirdetalle;?></option>
         <?php endforeach;?>
        <?php endif;?>
       </select>
       </div>
       <div class="col-md-11">
       <label for="">PROGRAMA</label>
       <select name="prg"  id="prg" size=1 class="form-control" required="required" onchange="mostrar(this)" >
       <option value="">Seleccione</option>
       <?php if(!empty($programa)):?><?php foreach($programa as $pg):?>
         <option value="<?php echo $pg->nroprg;?>"><?php echo $pg->descripcion;?></option>
         <?php endforeach;?>
        <?php endif;?>
       </select>
       </div>
       <div class="col-md-11">
       <label for="">SUBPROGRAMA</label>
       <select name="sbprg"  id="sbprg" size=1 class="form-control" required="required" onchange="mostrar(this)" >
       <option value="">Seleccione</option><?php if(!empty($programa)):?><?php foreach($programa as $pg):?>
         <option value="<?php echo $pg->nrosbprg;?>"><?php echo $pg->descripcionsp;?></option>
         <?php endforeach;?>
        <?php endif;?>
       </select>
       </div>
       <div class="col-md-11">
       <label for="">USO DEL MATERIAL/SERVICIO</label>
       <input type="text" class="form-control" name="destmat" type="text" id="destmat" placeholder=""  onkeyup="javascript:this.value=this.value.toUpperCase();" required="required" maxlength="300" onPaste="return false" >
       </div>
      <div class="col-md-11">
       <label for="">CUENTA DESTINO</label>
       <select name="cuenta"  id="cuenta" size=1 class="form-control" required="required" onchange="mostrar(this)" >
       <option value="">Seleccione</option><option value='1'>CAJA CHICA</option><option value='2'>CUENTA GENERAL</option><option value='3'>FDO.REPARACION MANTENIMIENTO Y CONTROL CALLES</option>
       <option value='4'>FDOS.DE TERCEROS Y OTROS EN GARANTIA</option>
       <option value='5'>FDOS.REPAROS EN GENERAL</option>
       <option value='6'>GAS.IND.UTIL.OF.PAR.MOTOC.</option>
       <option value='7'>FDO.ESP.OBRAS INFRAESTRUCTURA</option>
       <option value='8'>LOTERIA CHAQUEÑA</option>
       <option value='9'>CUENTA ESPECIAL APORTE FINANCI</option>
       <option value='10'>PRESIDENCIA CONCEJO</option>
       <option value='11'>PRODISM</option>
       <option value='12'>PROGRAMA COMEDORES INFANTILES</option>
       <option value='13'>FONDO FEDERAL SOLIDARIO</option>
       <option value='14'>CTA.ESP.SUBASTA PUBLICA</option>
       <option value='15'>FONDO DE DESARROLLO LOCAL</option>
       <option value='16'>OTROS</option>
       </select>
       </div>
       <div class="col-md-11">
       <label for="">TOTAL EN LETRAS</label>
       <input type="text" class="form-control" name="totalletra" type="text" id="totalletra" placeholder="" required="required" maxlength="300" onPaste="return false" onkeyup="javascript:this.value=this.value.toUpperCase();">
       </div>
       <div class="col-md-4">
       <label for="">TRAMITE DEL PEDIDO</label>
       <select name="tipotramite"  id="tipotramite" size=1 class="form-control" required="required" ><option value="">Seleccione</option><option value='N'>NORMAL</option>
       <option value='U'>URGENTE</option>
       <option value='M'>MUY URGENTE</option>
       </select>
       </div>
       <div class="col-md-4">
       <label for="">TIPO DE PEDIDO</label>
       <select name="tipopedido"  id="tipopedido" size=1 class="form-control" required="required">
       <option value="">Seleccione</option><option value='S'>SERVICIO</option>
       <option value='N'>BIENES DE CONSUMO</option><option value='C'>BIENES DE CAPITAL</option>
       <option value='T'>TRANSFERENCIAS</option>
       </select>
       </div>
       <div class="col-md-3">
       <label for="">COBERTURA EN MESES DEL PEDIDO</label>
       <select name="cobertura"  id="cobertura" size=1 class="form-control" required="required" >
       <option value="">Seleccione</option><option value='1'>1</option><option value='2'>2</option><option value='3'>3</option><option value='4'>4</option><option value='5'>5</option><option value='6'>6</option><option value='7'>7</option><option value='8'>8</option><option value='9'>9</option><option value='10'>10</option><option value='11'>11</option><option value='12'>12</option>
       </select>
       </div>
       </div></div>
       <hr>
       <div class="row">
       <div class="col-md-12">
       <div class="col-md-11">
       <button type="submit" class="btn btn-block btn-success btn-flat" >GRABAR PEDIDO</button>
       </div>
       </form><br>
       </div>
      </div>
     </div>
    </div>
    <!-- /.box-body -->
   </div>
  <!-- /.box -->
 </section>
 <!-- /.content -->
 </div>
<!-- /.content-wrapper -->