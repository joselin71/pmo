<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
 <!-- Content Header (Page header) -->
 <section class="content-header">
 <h1>Formulario de Seleccion de Secretaria</h1>
 </section>
 <!-- Main content -->
 <section class="content">
 <!-- Default box -->
  <div class="box box-solid">
    <div class="box-body">
      <div class="row">
       <div class="col-md-12">
       <form action="<?php echo base_url();?>mantenimiento/materiales/addCentral" method="POST">
       <div class="col-md-4"><label for="dni">SECRETARIA</label>
       <select name="secret" id="secret" size=1 class="form-control" required="required" >
       <option value="">Seleccione</option>
       <?php if(!empty($dependencia)):?>
        <?php foreach($dependencia as $ped):?>
       
       <option value="<?php echo $ped->sec;?>"><?php echo $ped->detsec;?></option>
       <?php endforeach;?>
        <?php endif;?>
       </select>
       </div>
       </div></div>
       <br>
       <div class="row">
       <div class="col-md-4">
        <button type="submit" class="btn btn-block btn-success btn-flat" >GENERAR PEDIDO</button>
       </div>
       </form>
       <div class="col-md-4">
       <form action="<?php echo base_url()?>mantenimiento/materiales/pedCentral" method="post">
        <div class="row">
        <div class="col-xs-12">
         <button type="submit" class="btn btn-danger btn-block btn-flat" name="volver">VOLVER SIN GENERAR</button>
       </div>
       </form><br>
       </div>
       
       </div>
       
     </div>
    </div>
    <!-- /.box-body -->
   </div>
  <!-- /.box -->
 </section>
 <!-- /.content -->
 </div>
<!-- /.content-wrapper -->