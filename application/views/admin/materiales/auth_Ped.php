<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
 <!-- Content Header (Page header) -->
 <section class="content-header">
 <h1>DATOS PEDIDO<small>Actualizar Estado</small></h1>
 <form action="<?php echo base_url()?>mantenimiento/materiales" method="post">
 <div class="row">
 <div class="col-xs-2">
 <button type="submit" class="btn btn-danger btn-block btn-flat" name="volver">VOLVER</button>
 </div>
 </form><br>
 </section>
 <!-- Main content -->
 <section class="content">
 <!-- Default box -->
  <div class="box box-solid">
    <div class="box-body">
     <div class="row">
       <div class="col-md-12">
       <?php if($this->session->flashdata("error")):?>
            <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <p><i class="icon fa fa-ban"></i><?php echo $this->session->flashdata("error")?></p>
            </div>
            <?php endif ?>
       <form action="<?php echo base_url();?>mantenimiento/materiales/updateAuthPed" method="POST">
       <input type="hidden" value="<?php echo $lista->idpedidomateriales; ?>" name="idCod">
       <div class="form-group">
       <label for="cod">Nro.Pedido:</label>
       <input type="text" class="form-control" readonly="readonly" style="width:50%;" value="<?php echo $lista->nropedido;?>">
       </div>
       <div class="form-group">
       <label for="cod">Año Pedido:</label>
       <input type="text" class="form-control" readonly="readonly" style="width:50%;" value="<?php echo $lista->aniopedido;?>">
       </div>
       <div class="form-group">
       <label for="cod">Destino:</label>
       <input type="text" class="form-control" readonly="readonly" style="width:50%;" value="<?php echo $lista->destinomat;?>">
       </div>
       <div class="form-group">
       <label for="cod">Total $:</label>
       <input type="text" class="form-control" readonly="readonly" style="width:50%;" value="<?php echo $lista->totalped;?>">
       </div>
       <div class="form-group">
       <label for="cod">ESTADO:</label>
       <select required="required" name="estad" id="estad" >
       <?php if(($rol==2)||($rol==3)||($rol==11)||($rol==15)) {?>
       <option value='1'>VISADO</option>
       <option value='2'>CARGADO</option>
       <?php }?>
       <?php if(($rol==16)||($rol==99)) {?>
        <option value='1'>VISADO</option>
        <option value='2'>CARGADO</option>
        <option value='3'>INGRESADO</option> 
        <option value='4'>AUTORIZADO</option>       
	      <option value='5'>OBSERVADO</option>
        <option value='6'>ANULADO</option>
       <?php }?>
       </select></span>
       </div>
       <div class="form-group">
       <label for="cod">Observaciones:</label>
       <textarea name="observa" id="observa" cols="60" rows="4"   maxlength="85"  onkeyup="javascript:this.value.toUpperCase()"><?php echo $lista->actuacion;?></textarea>
       </div>
       <div class="row">
        <div class="col-xs-2">
        <button type="submit" class="btn btn-success btn-block btn-flat" name="volver">ACTUALIZAR</button>
        </div>
       </form>
       </div>
     </div>
    </div>
    <!-- /.box-body -->
   </div>
  <!-- /.box -->
 </section>
 <!-- /.content -->
 </div>
<!-- /.content-wrapper -->
