<div class="box-body">
    <div class="row">
       <div class="col-md-12">
       <table id="example2" class="table-bordered btn-hover" style="width:100%">
        <thead>
        <tr><th width="100" style="font-family:verdana;font-size:60%;">SECRETARIA</th><th width="100" style="font-family:verdana;font-size:60%;">ALEATORIO</th> <th width="50" style="font-family:verdana;font-size:60%;">FECHA PEDIDO</th>
        <th width="50" style="font-family:verdana;font-size:60%;">PEDIDO</th><th width="50" style="font-family:verdana;font-size:60%;">AÑO</th><th width="100" style="font-family:verdana;font-size:60%;">TOTAL$</th>
        <th width="50" style="font-family:verdana;font-size:60%;">RUBRO</th>
        <th width="50" style="font-family:verdana;font-size:60%;">SUBRUBRO</th>
        <th width="50" style="font-family:verdana;font-size:60%;">CANTIDAD</th>
        <th width="50" style="font-family:verdana;font-size:60%;">DETALLE</th>
        <th width="50" style="font-family:verdana;font-size:60%;">PRECIO UNIT.</th>
        <th width="50" style="font-family:verdana;font-size:60%;">ESTADO</th>
        <th width="50" style="font-family:verdana;font-size:60%;">VISADO</th>
        <th width="50" style="font-family:verdana;font-size:60%;">AUTORIZADO</th>
        <th width="50" style="font-family:verdana;font-size:60%;">TIPO PEDIDO</th><th width="50" style="font-family:verdana;font-size:60%;">PROGRAMA</th><th width="50" style="font-family:verdana;font-size:60%;">SUBPROGRAMA</th><th width="50" style="font-family:verdana;font-size:60%;">COBERTURA</th>
        </tr>
        </thead>
        <tbody> 
        <?php if(!empty($pedidos)):?>
        <?php foreach($pedidos as $ped):?>
        <tr style="font-family:verdana;font-size:65%;"><td><?php echo $ped->idsol; ?></td><td><?php echo $ped->idaleatorio; ?></td> <td ><?php echo $ped->fechapedido; ?></td><td><?php echo $ped->nropedido; ?></td><td><?php echo $ped->aniopedido;?></td><td><?php echo $ped->totalped;?></td><td><?php echo $ped->idrubro;?></td><td><?php echo $ped->idsubr;?></td><td><?php echo $ped->cantidad;?></td><td><?php echo $ped->detallepedido;?></td>
        <td><?php echo $ped->importelinea;?></td><td><?php echo $ped->estado;?></td> <td><?php echo $ped->fechavisado;?></td><td><?php echo $ped->fechaautorizado;?></td>
        <td><?php echo $ped->tipopedido;?></td>
        <td><?php echo $ped->idprg;?></td>
        <td><?php echo $ped->idsbprg;?></td>
        <td><?php echo $ped->cobertura;?></td>
        </tr>
        <?php endforeach;?>
        <?php endif;?>
         </tbody>
       </table>
       </div>
     </div>
    </div>
    <!-- /.box-body -->
   </div>
  <!-- /.box -->
 </section>
 <!-- /.content -->
 </div>
<!-- /.content-wrapper -->