<script> 
function criterios(){ 
    if(document.form1.bp.checked == true){ 
        document.form1.bsprod.disabled = false; 
    } 
    else{ 
        document.form1.bsprod.disabled = true; 
    } 
     if(document.form1.bf.checked == true){ 
        document.form1.bddesde.disabled = false;
        document.form1.bdhasta.disabled = false; 
    } 
    else{ 
        document.form1.bddesde.disabled = true; 
        document.form1.bdhasta.disabled = true;
    } 
	 if(document.form1.fbf.checked == true){ 
        document.form1.fbddesde.disabled = false;
        document.form1.hbdhasta.disabled = false; 
    } 
    else{ 
        document.form1.fbddesde.disabled = true; 
        document.form1.hbdhasta.disabled = true;
    } 

} 
</script> 
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
<h1>Detalle de Pedidos de Materiales por Secretaria</h1>
</section>
<!-- Main content -->
<section class="content">
<!-- Default box -->
<div class="box box-solid">
 <div class="box-body">
 <div class="row">
  <div class="col-md-8">  
  <p class="">Seleccione Criterio de Consulta</p>
   <form id="form1" name="form1" action="<?php echo base_url();?>mantenimiento/materiales/detallePM" method="POST">
   <fieldset class="fieldset">
   <label>POR SECRETARIA</label>
   <input class="form-check-input" type="checkbox" name="bp" onclick="criterios()">
   <span class="">
   <select class="form-control" type="" name="bsprod"  id="bsprod" required="required" disabled="true">
   <option value='0'>TODOS</option>
   <option value='1'>HACIENDA Y FINANZAS</option>
   <option value='4'>PLANIFICACION INFRAESTRUCTURA Y AMBIENTE</option>
   <option value='5'>SERVICIOS PUBLICOS</option>
   <option value='7'>GOBIERNO</option>
   <option value='8'>DESARROLLO HUMANO E IDENTIDAD</option>
   <option value='9'>CONCEJO DELIBERANTE</option>
   <option value='10'>INTENDENCIA</option>
   <option value='11'>JUZGADO</option>
   <option value='12'>JUZGADO DE FALTAS 1</option>
   <option value='13'>JUZGADO DE FALTAS 2</option>
   </select>
   </span></fieldset><br>   
   <fieldset class="fieldset"><label>Por Fecha de Pedido Electrónico</label><input type="checkbox" name="bf" onclick="criterios()">
   <span class="contenedor-input"><input type="date" placeholder="dd/mm/aa" id="bddesde" name="bddesde" disabled="true" /><span>hasta</span><input type="date" placeholder="dd/mm/aa" id="bdhasta" name="bdhasta" disabled="true" /></span></fieldset><br>
   <fieldset class="fieldset"><label>Por Fecha de Autorizacion de Pedido</label><input type="checkbox" name="fbf" onclick="criterios()">
   <span class="contenedor-input"><input class="form-control fecha" type="date" placeholder="dd/mm/aaaa" id="fbddesde" name="fbddesde" disabled="true" /><span>hasta</span><input class="form-control fecha" type="date" placeholder="dd/mm/aaaa" id="hbdhasta" name="hbdhasta" disabled="true"/></span></fieldset><br>
 </div></div><hr>
 <div class="row">
  <div class="col-md-3">
   <div class="form-group"><label for=""></label>
    <button type="submit" class="btn btn-block btn-success btn-flat" >CONSULTAR PEDIDOS</button>
   </div>
   </form>
  </div>
  <div class="col-md-2">
   <form action="<?php echo base_url()?>" method="post">
   <!--div class="row">
   <div class="col-xs-12"-->
   <div class="form-group"><label for=""></label>
    <button type="submit" class="btn btn-danger btn-block btn-flat" name="volver">VOLVER</button>
    </div><!-- /.col -->
    </div>
    </form><br>
   </div>  
 </div>
 <!--/div>
</section>
<div-->
