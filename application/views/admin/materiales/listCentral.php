<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
 <!-- Content Header (Page header) -->
 <section class="content-header">
 <h1>Pedidos de Materiales por Secretaria<small>Listado</small></h1>
 </section>
 <!-- Main content -->
 <section class="content">
 <!-- Default box -->
  <div class="box box-solid">
    <div class="box-body">
     <div class="row">
      <div class="col-md-12">
        <a href="<?php echo base_url();?>mantenimiento/materiales/addSCentral" class="btn btn-primary btn-flat"><span class="fa fa-plus"></span>Nuevo Pedido</a>
      </div>
     </div>
     <hr>
     <div class="row">
       <!--div class="col-md-12"-->
       <div class="table-responsive">
       <table id="example1" class="table-bordered btn-hover" style="width:70%">
        <thead>
        <tr><th>Aleatorio</th><th>Fecha</th>
        <th>Sec</th><th>Operador</th><th>Nro</th><th>Año</th> <th>Uso Material/Servicio</th><th>Total $</th> <th>Estado</th><th>Fecha Autorizacion</th><th>Observaciones</th> <th>Opciones</th>
        </tr>
        </thead>
        <tbody> 
        <?php if(!empty($pedidos)):?>
        <?php foreach($pedidos as $ped):?>
        <tr style="font-family:verdana;font-size:65%;"><td><?php echo $ped->idpedidomateriales; ?><td><?php echo $ped->fechapedido; ?></td> <td ><?php echo $ped->nomsec; ?></td><td><?php echo $ped->idsolicitante; ?></td><td><?php echo $ped->nropedido;?></td><td><?php echo $ped->aniopedido;?></td><td><?php echo $ped->destinomat;?></td><td><?php echo $ped->totalped;?></td><td><?php echo $ped->estado;?></td><td><?php echo $ped->fechaautorizado; ?></td><td><?php echo $ped->actuacion;?></td> 
        <?php $datarevista=$ped->nropedido."*".$ped->aniopedido."*".$ped->destinomat."*".$ped->totalped ;?>
        <td>
        <div class="btn-group">
        <a href="<?php echo base_url();?>mantenimiento/materiales/printPed/<?php echo $ped->idpedidomateriales;?>" target="_blank;"><span class="material-icons">print</span></a>
        <a href="<?php echo base_url();?>mantenimiento/materiales/editPedCentral/<?php echo $ped->idpedidomateriales;?>" ><span class="material-icons" style="color: #FB8C00;">create</span></a>
        <a href="<?php echo base_url();?>mantenimiento/materiales/auth_ped/<?php echo $ped->idpedidomateriales;?>"> <span class="material-icons" style="color:#33cc33;">assignment_turned_in</span></a>
        </div>        
        </td></tr>
        <?php endforeach;?>
        <?php endif;?>
         </tbody>
       </table>
       </div>
     </div>
    </div>
    <!-- /.box-body -->
   </div>
  <!-- /.box -->
 </section>
 <!-- /.content -->
 </div>
<!-- /.content-wrapper -->
