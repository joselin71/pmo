<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
 <!-- Content Header (Page header) -->
 <section class="content-header">
 <h1>Pedidos de Materiales<small>Listado</small></h1>
 </section>
 <!-- Main content -->
 <section class="content">
 <!-- Default box -->
  <div class="box box-solid">
    <div class="box-body">
     <div class="row">
       <div class="col-md-12">
       <table id="compras" class="table-bordered btn-hover" style="width:60%">
        <thead>
        <tr><th>Aleatorio</th><th>Secretaria</th>
        <th>Año</th><th>Pedido</th><th>Estimado$</th><th>Ped.Mat</th> <th>Año O.C</th><th>Nro.O.C</th> <th>Monto Asig.$</th><th>Fecha O.C</th><th>Nro.Prov.</th><th>Proveedor</th><th>Tipo contratacion</th><th>Fecha Contratacion</th><th>Opciones</th>
        </tr>
        </thead>
        <tbody> 
        <?php if(!empty($pedidos)):?>
        <?php foreach($pedidos as $ped):?>
        <tr style="font-family:verdana;font-size:65%;"><td><?php echo $ped->idpedidomateriales; ?><td><?php echo $ped->isecre; ?></td> <td ><?php echo $ped->aniopedido; ?></td><td><?php echo $ped->nropedido; ?></td><td><?php echo $ped->totalped;?></td><td><?php echo $ped->pedmat;?></td><td><?php echo $ped->aniooc;?></td><td><?php echo $ped->nrooc;?></td><td><?php echo $ped->asignado;?></td><td><?php echo $ped->fecoc; ?></td><td><?php echo $ped->nropv;?> <td><?php echo $ped->proveedor;?> <td><?php echo $ped->actuacions;?> <td><?php echo $ped->fecas;?></td> 
        <td>
        <div class="btn-group">
        <a href="<?php echo base_url();?>mantenimiento/materiales/editPedCompras/<?php echo $ped->idpedidomateriales;?>" ><span class="material-icons" style="color: #FB8C00;">create</span></a>
        </div>        
        </td></tr>
        <?php endforeach;?>
        <?php endif;?>
         </tbody>
       </table>
       </div>
     </div>
    </div>
    <!-- /.box-body -->
   </div>
  <!-- /.box -->
 </section>
 <!-- /.content -->
 </div>
<!-- /.content-wrapper -->