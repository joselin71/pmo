<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
 <!-- Content Header (Page header) -->
 <section class="content-header">
 <h1>Programas por Secretaria<small>Listado</small></h1>
 </section>
 <!-- Main content -->
 <section class="content">
 <!-- Default box -->
  <div class="box box-solid">
    <div class="box-body">
    <div class="row">
      <div class="col-md-12">
        <a href="<?php echo base_url();?>mantenimiento/materiales/addProgramas" class="btn btn-primary btn-flat"><span class="fa fa-plus"></span>Nuevo Programa</a>
      </div>
     </div>
     <hr>
     <div class="row">
       <div class="col-md-12">
       <table id="compras" class="table-bordered btn-hover" style="width:100%">
        <thead>
        <tr><th>Secretaria</th><th>Nombre</th><th>Nro.Programa</th><th>Descripcipn</th><th>Opciones</th>
        </tr>
        </thead>
        <tbody> 
        <?php if(!empty($programas)):?>
        <?php foreach($programas as $ped):?>
        <tr style="font-family:verdana;font-size:100%;"><td><?php echo $ped->sec; ?></td><td><?php echo $ped->detsec; ?></td><td><?php echo $ped->nroprg; ?></td> <td ><?php echo $ped->descripcion; ?></td>
        <td>
        <div class="btn-group">
        <a href="<?php echo base_url();?>mantenimiento/materiales/editProgramas/<?php echo $ped->id;?>" ><span class="material-icons" style="color: #FB8C00;">create</span></a>
        <a href="<?php echo base_url();?>mantenimiento/materiales/deleteProgramas/<?php  echo $ped->id;?>" ><span class="material-icons">delete</span></a>
        </div>        
        </td></tr>
        <?php endforeach;?>
        <?php endif;?>
         </tbody>
       </table>
       </div>
     </div>
    </div>
    <!-- /.box-body -->
   </div>
  <!-- /.box -->
 </section>
 <!-- /.content -->
 </div>
<!-- /.content-wrapper -->