<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
 <!-- Content Header (Page header) -->
 <section class="content-header">
 <h1>MIGRACION DE PEDIDOS DE MATERIALES EJERCICIO ANTERIOR</h1>
 </section>
 <!-- Main content -->
 <section class="content">
 <!-- Default box -->
  <div class="box box-solid">
    <div class="box-body">
      <div class="row">
        <form action="<?php echo base_url();?>mantenimiento/materiales/migrarPedDetalle" method="POST">
         <div class="col-md-4">
            <label class="nombre-campo"><p>NRO.DE PEDIDO:</p></label>
            <span class=""><input class="form-control" name="nroped" id="nroped" type='text' placeholder=""  onkeypress="return numeros(event)" required="required"></span>
        </div>
      </div>
      <br>
      <div class="row">
      <div class="col-md-4">
        <button type="submit" class="btn btn-block btn-success btn-flat" >MIGRAR PEDIDO</button>
      </div>
       </form>
      <div class="col-md-4">
       <form action="<?php echo base_url()?>mantenimiento/materiales/migrarPed" method="post">
        <button type="submit" class="btn btn-danger btn-block btn-flat" name="volver">VOLVER SIN GENERAR</button>
       </div>
       </form><br>
       </div>
       
       </div>
       
    </div>
    <!-- /.box-body -->
   </div>
  <!-- /.box -->
 </section>
 <!-- /.content -->
 </div>
<!-- /.content-wrapper -->