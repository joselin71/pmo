<div class="box-body">
    <div class="row">
       <div class="col-md-12">
       <table id="compras" class="table-bordered btn-hover" style="width:100%">
        <thead>
        <tr><th>Aleatorio</th><th>Fecha</th>
        <th>Sec</th><th>Operador</th><th>Nro</th><th>Año</th> <th>Uso Material/Servicio</th><th>Total $</th> <th>Estado</th><th>Observaciones</th> <th>Opciones</th>
        </tr>
        </thead>
        <tbody> 
        <?php if(!empty($pedidos)):?>
        <?php foreach($pedidos as $ped):?>
        <tr style="font-family:verdana;font-size:65%;"><td><?php echo $ped->idpedidomateriales; ?><td><?php echo $ped->fechapedido; ?></td> <td ><?php echo $ped->isecre; ?></td><td><?php echo $ped->idsolicitante; ?></td><td><?php echo $ped->nropedido;?></td><td><?php echo $ped->aniopedido;?></td><td><?php echo $ped->destinomat;?></td><td><?php echo $ped->totalped;?></td><td><?php echo $ped->estado;?></td><td><?php echo $ped->actuacion;?></td> 
        <td>
        <div class="btn-group">
        <a href="<?php echo base_url();?>mantenimiento/materiales/printPed/<?php echo $ped->idpedidomateriales;?>" target="_blank;"><span class="material-icons">print</span></a>
        </div>        
        </td></tr>
        <?php endforeach;?>
        <?php endif;?>
         </tbody>
       </table>
       </div>
     </div>
    </div>
    <!-- /.box-body -->
   </div>
  <!-- /.box -->
 </section>
 <!-- /.content -->
 </div>
<!-- /.content-wrapper -->