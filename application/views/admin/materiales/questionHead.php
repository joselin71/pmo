<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
<h1>CONSULTA DE PEDIDOS POR ESTADO</h1>
</section>
<!-- Main content -->
<section class="content">
<!-- Default box -->
<div class="box box-solid">
 <div class="box-body">
 <div class="row">
  <div class="col-md-3">  
   <form action="<?php echo base_url();?>mantenimiento/materiales/questionDetalle" method="POST">
   <div class="form-group">
    <label for="">SELECCIONE ESTADO</label>
    <select class="form-control" name="estado" size=1 id="estado"  required="required" >
        <option value="">Seleccione</option>
        <option value="1">CARGADO</option>
        <option value="2">VISADO</option>
        <option value="3">INGRESADO</option>
        <option value="4">AUTORIZADO</option>
        <option value="5">OBSERVADO</option>
        </select>
    </div>
  </div>
  <div class="col-md-3">
   <div class="form-group"><label for=""></label>
    <button type="submit" class="btn btn-block btn-success btn-flat" >CONSULTAR PEDIDOS</button>
   </div>
   </form>
  </div>
  <div class="col-md-2">
   <form action="<?php echo base_url()?>mantenimiento/materiales" method="post">
   <!--div class="row">
   <div class="col-xs-12"-->
   <div class="form-group"><label for=""></label>
    <button type="submit" class="btn btn-danger btn-block btn-flat" name="volver">VOLVER</button>
    </div><!-- /.col -->
    </div>
    </form><br>
   </div>  
 </div>
 <!--/div>
</section>
<div-->
