<script> 
function criterios(){ 
    if(document.form1.bp.checked == true){ 
        document.form1.bsprod.disabled = false; 
    } 
    else{ 
        document.form1.bsprod.disabled = true; 
    } 
    if(document.form1.bf.checked == true){ 
        document.form1.bddesde.disabled = false;
        document.form1.bdhasta.disabled = false; 
    } 
    else{ 
        document.form1.bddesde.disabled = true; 
        document.form1.bdhasta.disabled = true;
    } 
     if(document.form1.bfp.checked == true){ 
        document.form1.aleatoriod.disabled = false; 
        document.form1.aleatorioh.disabled = false;
    } 
    else{ 
        document.form1.aleatoriod.disabled = true; 
        document.form1.aleatorioh.disabled = true; 
    } 
    if(document.form1.bpr.checked == true){ 
        document.form1.nroprovd.disabled = false;
        document.form1.nroprovh.disabled = false; 
    } 
    else{ 
        document.form1.nroprovd.disabled = true; 
        document.form1.nroprovh.disabled = true;
    } 

    if(document.form1.bOC.checked == true){ 
        document.form1.nocd.disabled = false;
        document.form1.noch.disabled = false; 
    } 
    else{ 
        document.form1.nocd.disabled = true; 
        document.form1.noch.disabled = true;
    } 
    if(document.form1.bPM.checked == true){ 
        document.form1.npmd.disabled = false;
        document.form1.npmh.disabled = false; 
    } 
    else{ 
        document.form1.npmd.disabled = true; 
        document.form1.npmh.disabled = true;
    } 

} 
</script> 
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
<h1>SEGUIMIENTO DE PEDIDO DE MATERIALES</h1>
</section>
<!-- Main content -->
<section class="content">
<!-- Default box -->
<div class="box box-solid">
 <div class="box-body">
 <div class="row">
  <div class="col-md-8">  
  <p class="">Seleccione Criterio de Consulta</p>
   <form id="form1" name="form1" action="<?php echo base_url();?>mantenimiento/materiales/seguimientoPed" method="POST">
   <fieldset class="fieldset">
   <label>Por Nombre de Proveedor</label>
   <input class="form-check-input" type="checkbox" name="bp" onclick="criterios()">
   <span class="">
   <input class="form-control" type="text" placeholder="Ingrese Nombre Proveedor" id="bsprod" name="bsprod" disabled="true" />
   </span></fieldset><br>   
   <fieldset class="fieldset"><label>Por Fecha de Pedido Electrónico</label><input type="checkbox" name="bf" onclick="criterios()">
   <span class="contenedor-input"><input type="date" placeholder="dd/mm/aa" id="bddesde" name="bddesde" disabled="true" /><span>hasta</span><input type="date" placeholder="dd/mm/aa" id="bdhasta" name="bdhasta" disabled="true" /></span></fieldset><br>
   <fieldset class="fieldset"><label>Por Pedido Electrónico</label><input type="checkbox" name="bfp" onclick="criterios()">
   <span class="contenedor-input"><input class="form-control" type="text" placeholder="Id.Aleatorio del Pedido" id="aleatoriod" name="aleatoriod" disabled="true" onkeypress="return numeros(event)"/><span>hasta</span><input class="form-control" type="text" placeholder="Id.Aleatorio del Pedido" id="aleatorioh" name="aleatorioh" disabled="true" onkeypress="return numeros(event)"/></span></fieldset><br>
   <fieldset class="fieldset"><label>Por Nro de Proveedor</label>
   <input type="checkbox" name="bpr" onclick="criterios()">
   <span class="contenedor-input"><input class="form-control" type="text" placeholder="Ingrese Nro.de Proveedor" id="nroprovd" name="nroprovd" disabled="true" onkeypress="return numeros(event)"/><span>hasta</span>
   <input class="form-control" type="text" placeholder="Ingrese Nro.de Proveedor" id="nroprovh" name="nroprovh" disabled="true" onkeypress="return numeros(event)"/></span></fieldset><br>
   <fieldset class="fieldset"><label>Por Nro de O.C (Dpto.Compras)</label><input type="checkbox" name="bOC" onclick="criterios()">
   <span class="contenedor-input"><input class="form-control" type="text" placeholder="Ingrese Nro.de O.C" id="nocd" name="nocd" disabled="true" onkeypress="return numeros(event)"/><span>hasta</span><input class="form-control" type="text" placeholder="Ingrese Nro.de O.C" id="noch" name="noch" disabled="true" onkeypress="return numeros(event)"/></span></fieldset><br>
   <fieldset class="fieldset"><label style="line-height: 1em;">Por Nro de Pedido de Material (Dpto.Compras)</label>
   <input type="checkbox" name="bPM" onclick="criterios()">
   <span class="contenedor-input"><input class="form-control" type="text" placeholder="Ingrese Nro.de Pedido" id="npmd" name="npmd" disabled="true" onkeypress="return numeros(event)"/><span>hasta</span><input class="form-control" type="text" placeholder="Ingrese Nro.de Pedido" id="npmh" name="npmh" disabled="true" onkeypress="return numeros(event)"/></span></fieldset><br>
  </div></div><hr>




  <div class="row">
  <div class="col-md-3">
   <div class="form-group"><label for=""></label>
    <button type="submit" class="btn btn-block btn-success btn-flat" >CONSULTAR PEDIDOS</button>
   </div>
   </form>
  </div>
  <div class="col-md-2">
   <form action="<?php echo base_url()?>" method="post">
   <!--div class="row">
   <div class="col-xs-12"-->
   <div class="form-group"><label for=""></label>
    <button type="submit" class="btn btn-danger btn-block btn-flat" name="volver">VOLVER</button>
    </div><!-- /.col -->
    </div>
    </form><br>
   </div>  
 </div>
 <!--/div>
</section>
<div-->
