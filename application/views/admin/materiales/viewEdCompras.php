<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
 <!-- Content Header (Page header) -->
 <section class="content-header">
 <h1>Completar Pedido de Materiales</h1>
 </section>
 <!-- Main content -->
 <section class="content">
 <!-- Default box -->
  <div class="box box-solid">
    <div class="box-body">
     <div class="row">
       <div class="col-md-12">
       <?php if($this->session->flashdata("error")):?>
            <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <p><i class="icon fa fa-ban"></i><?php echo $this->session->flashdata("error")?></p>
            </div>
            <?php endif ?>
       <form id="formulario" action="<?php echo base_url();?>mantenimiento/materiales/edstoreCompras" method="POST">
       <div class="col-md-2">
       <label for="">Secretaria</label> 
       <input type="text" readonly="readonly" value="<?php echo $lista->isecre; ?>" name="nsec"> 
       </div>
       <div class="col-md-2">
       <label for="">Año Pedido</label>
       <input type="text" value="<?php echo $lista->aniopedido; ?>" name="anped">  
       </div>
       <div class="col-md-2">  
       <label for="">Nro. Pedido</label>
       <input type="text" value="<?php echo $lista->nropedido; ?>" name="nrop">  
       </div>
       <div class="col-md-2">
       <label for="">Estimado $</label>
       <input type="text" value="<?php echo $lista->totalped; ?>" name="estima">       
       </div></div></div><hr>
       <div class="row">
       <div class="col-md-12">      
       <div class="col-md-3">
       <label for="">Pedido Material:(Dpto.Compras)</label>
       <input type="text" required="required" name="pedmatcom" id="pedmatcom" onkeypress="return numeros(event)">       
       </div>
       <div class="col-md-2">
       <label for="">A&ntildeo O.C:(Dpto.Compras)</label>
       <select required="required" name="anoc" id="anoc" >
        <option value='1'><?php echo date("Y"); ?></option>
        <option value='2'><?php echo date("Y")-1; ?></option>
        <option value='3'><?php echo date("Y")-2; ?></option>
        <option value='4'><?php echo date("Y")-3; ?></option>
        <option value='5'><?php echo date("Y")-4; ?></option>
       </select>       
       </div>
       <div class="col-md-2">
       <label for="">Numero O.C:(Dpto.Compras)</label>
       <input type="text" required="required" name="noc" id="noc" maxlength="13" onkeypress="return numeros(event)">       
       </div>
       <div class="col-md-2">
       <label for="">Asignado $</label>
       <input type="text"required="required" name="asigna" id="asigna" onkeypress="return numeros(event)">       
       </div>
       <div class="col-md-2">
       <label for="">Fecha O.C</label>
       <input type="date" class="form-control" id="fecoc" name="fecoc" required="required" min="2021-01-01" max="2021-12-31">       
       </div></div></div><hr>
       <div class="row">
       <div class="col-md-12"> 
       <div class="col-md-2">
       <label for="">Proveedor(Nro)</label>
       <input type="text" required="required" name="provee" id="provee" maxlength="10" onkeypress="return numeros(event)">       
       </div>
       <div class="col-md-2">
       <label for="">Proveedor(Razon Social)</label>
       <input type="text" required="required" name="proveer" id="proveer" maxlength="85" onkeyup="javascript:this.value=this.value.toUpperCase();">       
       </div>
       <div class="col-md-2">
       <label for="">Tipo Contratacion</label>
       <select required="required" name="actuacions" id="actuacions" maxlenght="40" style="font-family:verdana;font-size:80%;">
       <option value='1'>COMPRA DIRECTA</option>
       <option value='2'>CONTRATACION DIRECTA</option>
       <option value='3'>CONCURSO DE PRECIOS</option>
       <option value='4'>LICITACION PRIVADA</option>
       <option value='5'>LICITACION PUBLICA</option>
       </select>       
       </div>
       <div class="col-md-2">
       <label for="">Fecha Contratacion</label>
       <input type="date" class="form-control" id="feccon" name="feccon" required="required" min="2021-01-01" max="2021-12-31">       
       </div></div></div><hr>
       <div class="row">
       <div class="col-md-12">
       <div class="form-group">
       <input type="hidden" value="<?php echo $lista->idpedidomateriales; ?>" name="idPed">
       <button type="submit" class="btn btn-block btn-success btn-flat" >ACTUALIZAR PEDIDO</button>
       </div>
       </form>
       <form action="<?php echo base_url()?>mantenimiento/materiales/gestionCompras" method="post">
        <div class="row">
        <div class="col-xs-12">
         <button type="submit" class="btn btn-danger btn-block btn-flat" name="volver">VOLVER SIN ACTUALIZAR</button>
       </div>
       </form><br>
       </div>
       
       </div>
       
     </div>
    </div>
    <!-- /.box-body -->
   </div>
  <!-- /.box -->
 </section>
 <!-- /.content -->
 </div>
<!-- /.content-wrapper -->