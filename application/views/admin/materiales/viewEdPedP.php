<script type="text/javascript">
    function cargarsub(idrubro){
    var select = document.getElementById(idrubro);
    var seleccionado = select.options[select.selectedIndex].value; 
    var selectEst = document.getElementById("subrubro");
    selectEst.disabled=false;
    selectEst.value=0;
   for (var i = 0; i < selectEst.length; i++) {
      var opt = selectEst[i];
        if (seleccionado == opt.id)
     {
     document.getElementById("subrubro").options[i].style.display ="";
      }else{
    document.getElementById("subrubro").options[i].style.display ="none";
  }
}
} 
</script>
<script>
  function fncSumar(){
caja=document.forms["formulario"].elements;
var numero1 = Number(caja["montof"].value);
var numero2 = Number(caja["montop"].value);
resultado=numero1*numero2;
if(!isNaN(resultado)){
caja["totalf"].value=(numero1*numero2).toFixed(2);
}
}
</script>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
 <!-- Content Header (Page header) -->
 <section class="content-header">
 <h1>Modificacion Individual de Bienes / Servicios</h1>
 </section>
 <!-- Main content -->
 <section class="content">
 <!-- Default box -->
  <div class="box box-solid">
    <div class="box-body">
     <div class="row">
       <div class="col-md-12">
       <?php if($this->session->flashdata("error")):?>
            <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <p><i class="icon fa fa-ban"></i><?php echo $this->session->flashdata("error")?></p>
            </div>
            <?php endif ?>
       <form id="formulario" action="<?php echo base_url();?>mantenimiento/materiales/edstoreP" method="POST">
       <div class="form-group">
       <label for="">RUBRO</label>
       <select class="form-control" name="rubro" size=1 id="rubro"  required="required" onchange="cargarsub(id)">
        <option value="">Seleccione</option>
        <?php if(!empty($rubro)):?>
        <?php foreach($rubro as $rb):?>
        <option value="<?php echo $rb->rubro?>"><?php echo utf8_decode($rb->rubrodesc);?></option>
        <?php endforeach;?>
        <?php endif;?>
        </select>
        </div>
        <div class="form-group">
       <label for="">SUBRUBRO</label><h6></h6>
       <select class="form-control" name="subrubro" size=1 id="subrubro"  required="required" disabled>
        <option value="">Seleccione</option>
        <?php if(!empty($subrubro)):?>
        <?php foreach($subrubro as $srb):?>
        <option id="<?php echo $srb->irubro?>" value="<?php echo $srb->subrubro?>"><?php echo utf8_decode($srb->subrubdesc);?></option>
        <?php endforeach;?>
        <?php endif;?>
        </select>
       </div>
       <?php if(!empty($lista)):?>
      
       <div class="col-md-2">
       <input type="hidden" value="<?php echo $lista->iddetallepm; ?>" name="idCod">
       <input type="hidden" value="<?php echo $id; ?>" name="idPed">
       <label for="">Cantidad (con 2 decimales)</label>
       <input type="text" class="form-control" required="required" name="montof" id="montof" onkeypress="return numeros(event)" pattern="[0-9]{1,10}[.][0-9]{2}" onKeyUp="fncSumar()" value="<?php echo $lista->cantidad?>">
       </div>
       <div class="col-md-2">
       <label for="">P.V. Unitario ($ con 2 decimales)</label>
       <input type="text" class="form-control" name="montop" id="montop" onkeypress="return numeros(event)" required="required" onPaste="return false" pattern="[0-9]{1,8}[.][0-9]{2}" onKeyUp="fncSumar()" value="<?php echo $lista->importelinea?>">
       </div>
       <div class="col-md-3">
       <label for="">Importe Estimado (Se calcula automáticamente) $:</label>
       <input type="text" class="form-control" name="totalf" id="totalf" readonly="readonly" value="<?php echo $lista->importedetalle?>">
       </div>

       </div></div>
       <div class="row">
       <div class="col-md-12">
       <div class="form-group">
       <label for="">Descripción de Bienes/Servicios</label>
       <textarea name="bienes" id="bienes" class="form-control" rows="5" cols="300" required="required" onkeyup="javascript:this.value=this.value.toUpperCase();" ><?php echo $lista->detallepedido?></textarea>
       </div>
       <?php endif;?>
       <input type="hidden" value="<?php echo $id; ?>" name="idPed">
       <div class="form-group">
       <button type="submit" class="btn btn-block btn-success btn-flat" >MODIFICAR Y CONTINUAR PEDIDO</button>
       </div>
       </form>
       <form action="<?php echo base_url()?>mantenimiento/materiales/editPed/<?php echo $id; ?>" method="post">
        <div class="row">
        <div class="col-xs-12">
         <button type="submit" class="btn btn-danger btn-block btn-flat" name="volver">VOLVER SIN GRABAR BIEN/SERVICIO</button>
       </div>
       </form><br>
       </div>
       
       </div>
       
     </div>
    </div>
    <!-- /.box-body -->
   </div>
  <!-- /.box -->
 </section>
 <!-- /.content -->
 </div>
<!-- /.content-wrapper -->