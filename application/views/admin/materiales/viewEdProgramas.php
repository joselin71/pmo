<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
 <!-- Content Header (Page header) -->
 <section class="content-header">
 <h1>Modificacion Individual de Programas</h1>
 </section>
 <!-- Main content -->
 <section class="content">
 <!-- Default box -->
  <div class="box box-solid">
    <div class="box-body">
     <div class="row">
     <div class="col-md-12">
       <?php if($this->session->flashdata("error")):?>
        <div class="alert alert-danger alert-dismissible">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          <p><i class="icon fa fa-ban"></i><?php echo $this->session->flashdata("error")?></p>
        </div></div>
       <?php endif ?>
       <div class="row">
        <div class="col-md-8"><label for="">Secretaria Actual</label>
        <input type="text" class="form-control" readonly="readonly" value="<?php $actual=$lista->idsec;
        if($actual==1){$secre="HACIENDA Y FINANZAS";}
        if($actual==4){$secre="PLANIFICACION, INFRAESTRUCTURA Y AMBIENTE";}
        if($actual==5){$secre="SERVICIOS PUBLICOS";}
        if($actual==7){$secre="GOBIERNO";}
        if($actual==8){$secre="DESARROLLO HUMANO E IDENTIDAD";}
        if($actual==9){$secre="CONCEJO DELIBERANTE";}
        if($actual==10){$secre="INTENDENCIA";}
        if($actual==11){$secre="JUZGADO";}
        if($actual==12){$secre="JUZGADO N-1";}
        if($actual==13){$secre="JUZGADO N-2";}
        
        echo $lista->idsec."-".$secre;?>">

        </div>
       </div>
       <div class="row">
       <form id="formulario" action="<?php echo base_url();?>mantenimiento/materiales/storeEdProgramas/<?php echo $lista->id; ?>" method="POST">
       <div class="col-md-8">
       <label for="">Secretaria</label>
       <select class="form-control" name="nsec" size=1 id="nsec"  required="required" >
        <option value="">Seleccione</option>
        <?php if(!empty($dependencia)):?>
        <?php foreach($dependencia as $dp):?>
        <option value="<?php echo $dp->sec?>"><?php echo utf8_decode($dp->detsec);?></option>
        <?php endforeach;?>
        <?php endif;?>
        </select>
       </div>
    </div>
    <div class="row">
      <div class="col-md-2">
       <label for="">Programa</label>
       <input type="text" class="form-control" required="required" name="numprg" id="numprg" onkeypress="return numeros(event)" maxlength="3" required="required" value="<?php echo $lista->nroprg ?>">
       </div>       
      <div class="col-md-6">
       <label for="">Detalle Programa</label>
       <input type="text" class="form-control" name="detprg" id="detprg" maxlength="80" onkeyup="javascript:this.value=this.value.toUpperCase();" required="required" value="<?php echo $lista->descripcion;?>">
      </div>
    </div>
    <div class="row">
      <div class="col-md-3">
       <label for="">Fecha Inicial</label>
       <input type="date" class="form-control" placeholder="dd/mm/aa" id="bddesde" name="bddesde" required="required">
      </div>
      <div class="col-md-3">
       <label for="">Fecha Final</label>
       <input type="date" class="form-control" placeholder="dd/mm/aa" id="bdhasta" name="bdhasta" required="required">
      </div>
    </div><hr>
    <input type="text" name="ids" id="ids" readonly="readonly" style="height:2px; visibility:hidden;" value="<?php echo $lista->id; ?>">
    <div class="row">
      <div class="col-md-4">
         <button type="submit" class="btn btn-block btn-success btn-flat" >GRABAR PROGRAMA</button>
      </div>
    
    </form>
    <form action="<?php echo base_url()?>mantenimiento/materiales/programas" method="post">
    
     <div class="col-md-4">
       <button type="submit" class="btn btn-danger btn-block btn-flat" name="volver">VOLVER SIN GRABAR PROGRAMA</button>
     </div>
    </form><br>
   </div>
       
   </div>
    <!-- /.box-body -->
   </div>
  <!-- /.box -->
 </section>
 <!-- /.content -->
 </div>
<!-- /.content-wrapper -->