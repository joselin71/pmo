<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
 <!-- Content Header (Page header) -->
 <section class="content-header">
 <h1>Agregar Proveedores</h1>
 </section>
 <!-- Main content -->
 <section class="content">
 <!-- Default box -->
  <div class="box box-solid">
    <div class="box-body">
     
     <div class="row">
       <div class="col-md-12">
        <?php if($this->session->flashdata("error")):?>
         <div class="alert alert-danger alert-dismissible">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          <p><i class="icon fa fa-ban"></i><?php echo $this->session->flashdata("error")?></p>
         </div>
       </div>
       <?php endif ?>
       <form action="<?php echo base_url();?>mantenimiento/proveedores/storeProve" method="POST">
    <div class="row">
       <div class="col-md-4">
       <label for="">Numero de Proveedor-Ingrese el numero sin puntos</label>
       <input class="form-control" name="nroprov" id="nroprov" type="text" placeholder="Solo numeros"  onkeypress="return numeros(event)" required="required" onPaste="return false" maxlength="10" >
       </div>
       <div class="col-md-6">
       <label for="">Razon Social</label>
       <input type="text" class="form-control" name="razonsocial" id="razonsocial" placeholder="Razon Social"  onkeyup="javascript:this.value=this.value.toUpperCase();" required="required" maxlength="45" onPaste="return false" >
       </div>
    </div><br>
    <div class="row">
       <div class="col-md-5">
       <button type="submit" class="btn btn-block btn-success btn-flat" >AGREGAR PROVEEDOR</button>
       </div>
       </form>
       <form action="<?php echo base_url()?>mantenimiento/proveedores" method="post">
       <div class="col-md-5">
         <button type="submit" class="btn btn-danger btn-block btn-flat" name="volver">VOLVER</button>
       </div>
       </form><br>
    </div>
      
    </div>
    <!-- /.box-body -->
   </div>
  <!-- /.box -->
 </section>
 <!-- /.content -->
 </div>
<!-- /.content-wrapper -->