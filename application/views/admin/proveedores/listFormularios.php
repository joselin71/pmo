 <!-- Content Wrapper. Contains page content -->
 <div class="content-wrapper">
 <!-- Content Header (Page header) -->
 <section class="content-header">
 <h1>Formularios Presentados<small>Listado</small></h1>
 </section>
 <!-- Main content -->
 <section class="content">
 <!-- Default box -->
  <div class="box box-solid">
    <div class="box-body">
     <div class="row">
       <div class="col-md-12">
       <table id="usuarios" class="table-bordered btn-hover" style="width:100%">
        <thead>
        <tr><th>#</th><th>Form</th><th>Año</th><th>Nro.Prov</th>
        <th>Razon Social</th><th>Presento</th><th>Reclamado</th><th>Pagos Recibidos</th> <th>Total Deuda</th><th>Observaciones</th>
        </tr>
        </thead>
        <tbody> 
        <?php if(!empty($pedidos)):?>
        <?php foreach($pedidos as $ped):?>
        <tr style="font-family:verdana;font-size:65%;"><td><?php echo $ped->idresumensg; ?><td><?php echo $ped->nroform; ?></td><td><?php echo $ped->anioform; ?></td> <td ><?php echo $ped->nroprov; ?></td><td><?php echo $ped->razonsocial; ?></td><td><?php echo $ped->fechapresenta;?></td><td><?php echo $ped->montoreclamado;?></td><td><?php echo $ped->pagosrecibidos;?></td><td><?php echo $ped->totaldeuda;?></td><td><?php echo $ped->observaciones;?></td>
        </tr>
        <?php endforeach;?>
        <?php endif;?>
         </tbody>
       </table>
       </div>
     </div>
    </div>
    <!-- /.box-body -->
   </div>
  <!-- /.box -->
 </section>
 <!-- /.content -->
 </div>
<!-- /.content-wrapper -->