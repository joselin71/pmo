<div class="box-body">
    <div class="row">
       <div class="col-md-12">
       <table id="usuarios" class="table-bordered btn-hover" style="width:100%">
        <thead>
        <tr><th width="100" style="font-family:verdana;font-size:60%;">PROVEEDOR</th>
        <th width="100" style="font-family:verdana;font-size:60%;">RAZON SOCIAL</th>
        <th width="50" style="font-family:verdana;font-size:60%;">LOCALIDAD</th>
        <th width="50" style="font-family:verdana;font-size:60%;">PROVINCIA</th>
        <th width="50" style="font-family:verdana;font-size:60%;">CONTACTO</th>
        <th width="100" style="font-family:verdana;font-size:60%;">CORREO</th>
        </tr>
        </thead>
        <tbody> 
        <?php if(!empty($pedidos)):?>
        <?php foreach($pedidos as $ped):?>
        <tr style="font-family:verdana;font-size:65%;"><td><?php echo $ped->nroprov; ?><td><?php echo $ped->razonsocial; ?></td> <td ><?php echo $ped->localidad; ?></td><td><?php echo $ped->provincia; ?></td><td><?php echo $ped->tel1;?></td><td><?php echo $ped->email;?></td>
        </tr>
        <?php endforeach;?>
        <?php endif;?>
         </tbody>
       </table>
       </div>
     </div>
    </div>
    <!-- /.box-body -->
   </div>
  <!-- /.box -->
 </section>
 <!-- /.content -->
 </div>
<!-- /.content-wrapper -->