<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
<h1>Montos Reclamados No Presentados por Proveedor</h1>
</section>
<!-- Main content -->
<section class="content">
<!-- Default box -->
<div class="box box-solid">

<div class="box-body">
    <div class="row">
       <div class="col-md-12">
       <table id="example" class="table-bordered btn-hover" style="width:100%">
        <thead>
        <tr><th width="100" style="font-family:verdana;font-size:60%;">PROVEEDOR</th><th width="100" style="font-family:verdana;font-size:60%;">RAZON SOCIAL</th> <th width="50" style="font-family:verdana;font-size:60%;">SUJETO PASIVO</th>
        <th width="50" style="font-family:verdana;font-size:60%;">TRIBUTO</th><th width="50" style="font-family:verdana;font-size:60%;">IMPORTE</th><th width="100" style="font-family:verdana;font-size:60%;">FECHA CARGA</th>
        <th width="50" style="font-family:verdana;font-size:60%;">DOCUMENTO</th>
        <th width="50" style="font-family:verdana;font-size:60%;">FORMULARIO-AÑO</th>
        </tr>
        </thead>
        <tbody> 
        <?php if(!empty($pedidos)): ?>
        <?php foreach($pedidos as $ped):?>
        <tr style="font-family:verdana;font-size:65%;"><td><?php echo $ped->provcom; ?></td><td><?php echo $ped->razonsocial; ?></td> <td ><?php echo $ped->sujetop; ?></td><td><?php echo $ped->tributop; ?></td><td><?php echo $ped->importep;?></td><td><?php echo $ped->fechacarga;?></td><td><?php echo $ped->documento;?></td><td><?php echo $ped->nroform."-".$ped->anioform;?></td>
        </tr>
        <?php endforeach;?>
        <?php endif;
        ?>
        </tbody>
       </table>
       
       </div>
     </div>
    </div>
    <!-- /.box-body -->
   </div>
  <!-- /.box -->
 </section>
 <!-- /.content -->
 </div>
<!-- /.content-wrapper -->