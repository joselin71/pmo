<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
<h1>Detalle de Anexo I</h1>
</section>
<!-- Main content -->
<section class="content">
<!-- Default box -->
<div class="box box-solid">

<div class="box-body">
    <div class="row">
       <div class="col-md-12">
       <table id="example" class="table-bordered btn-hover" style="width:100%">
        <thead>
        <tr><th width="100" style="font-family:verdana;font-size:60%;">PROVEEDOR</th><th width="100" style="font-family:verdana;font-size:60%;">RAZON SOCIAL</th> <th width="50" style="font-family:verdana;font-size:60%;">INSTRUMENTO-OC</th>
        <th width="50" style="font-family:verdana;font-size:60%;">T-FACTURA</th><th width="50" style="font-family:verdana;font-size:60%;">N-FACTURA</th><th width="100" style="font-family:verdana;font-size:60%;">FACTURA</th>
        <th width="50" style="font-family:verdana;font-size:60%;">PAGOS $</th>
        <th width="50" style="font-family:verdana;font-size:60%;">TOTAL DEUDA $</th>
        </tr>
        </thead>
        <tbody> 
        <?php if(!empty($pedidos)): ?>
        <?php foreach($pedidos as $ped):?>
        <tr style="font-family:verdana;font-size:65%;"><td><?php echo $ped->nroproveedor; ?></td><td><?php echo $ped->razonsocial; ?></td> <td ><?php echo $ped->intrumento."-".$ped->ordencompra; ?></td><td><?php echo $ped->tipofactura;?></td><td><?php echo $ped->nrofactura;?></td><td><?php echo $ped->montofactura;?></td><td><?php echo $ped->pagosparciales;?></td><td><?php echo $ped->totaldeuda;?></td>
        </tr>
        <?php endforeach;?>
        <?php endif;
        ?>
        </tbody>
       </table>
       
       </div>
     </div>
    </div>
    <!-- /.box-body -->
   </div>
  <!-- /.box -->
 </section>
 <!-- /.content -->
 </div>
<!-- /.content-wrapper -->