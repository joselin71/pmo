<script> 
function criterios(){ 
    if(document.form1.bp.checked == true){ 
        document.form1.bsprod.disabled = false; 
    } 
    else{ 
        document.form1.bsprod.disabled = true; 
    } 
     if(document.form1.bf.checked == true){ 
        document.form1.bddesde.disabled = false;
        document.form1.bdhasta.disabled = false; 
    } 
    else{ 
        document.form1.bddesde.disabled = true; 
        document.form1.bdhasta.disabled = true;
    } 
     if(document.form1.bfp.checked == true){ 
        document.form1.nroformd.disabled = false; 
        document.form1.nroformh.disabled = false;
    } 
    else{ 
        document.form1.nroformd.disabled = true; 
        document.form1.nroformh.disabled = true; 
    } 
     if(document.form1.bpr.checked == true){ 
        document.form1.nroprovd.disabled = false;
        document.form1.nroprovh.disabled = false; 
    } 
    else{ 
        document.form1.nroprovd.disabled = true; 
        document.form1.nroprovh.disabled = true;
    } 

} 
</script> 
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
<h1>Pedidos de Compensaciones de Proveedores</h1>
</section>
<!-- Main content -->
<section class="content">
<!-- Default box -->
<div class="box box-solid">
 <div class="box-body">
 <div class="row">
  <div class="col-md-8">  
  <p class="">Seleccione Criterio de Consulta</p>
   <form id="form1" name="form1" action="<?php echo base_url();?>mantenimiento/proveedores/proveeCompensa" method="POST">
   <fieldset class="fieldset">
   <label>Por Nombre de Proveedor</label>
   <input class="form-check-input" type="checkbox" name="bp" onclick="criterios()">
   <span class="">
   <input class="form-control" type="text" placeholder="Ingrese Nombre Proveedor" id="bsprod" name="bsprod" disabled="true" />   
   </span></fieldset><br>   
   <fieldset class="fieldset"><label>Por Fecha de Presentacion</label><input type="checkbox" name="bf" onclick="criterios()">
   <span class="contenedor-input"><input type="date" placeholder="dd/mm/aa" id="bddesde" name="bddesde" disabled="true" /><span>hasta</span><input type="date" placeholder="dd/mm/aa" id="bdhasta" name="bdhasta" disabled="true" /></span></fieldset><br>
   <fieldset class="fieldset"><label>Por Formulario presentado</label>
   <input type="checkbox" name="bfp" onclick="criterios()">
   <span class="contenedor-input"><input class="form-control" type="text" placeholder="Ingrese N° del Formulario" id="nroformd" name="nroformd" disabled="true" onkeypress="return numeros(event)"/><span>hasta</span>
   <input class="form-control" type="text" placeholder="Ingrese N° del Formulario" id="nroformh" name="nroformh" disabled="true" onkeypress="return numeros(event)"/></span></fieldset><br>
   <fieldset class="fieldset"><label>Por Número de Proveedor</label>
   <input type="checkbox" name="bpr" onclick="criterios()"><span class="contenedor-input"><input class="form-control" type="text" placeholder="Ingrese N° de Proveedor" id="nroprovd" name="nroprovd" disabled="true" onkeypress="return numeros(event)"/><span>hasta</span>
   <input class="form-control" type="text" placeholder="Ingrese N° de Proveedor" id="nroprovh" name="nroprovh" disabled="true" onkeypress="return numeros(event)"/></span></fieldset><br>  
 </div></div><hr>
 <div class="row">
  <div class="col-md-3">
   <div class="form-group"><label for=""></label>
    <button type="submit" class="btn btn-block btn-success btn-flat" >CONSULTAR SOLICITUD</button>
   </div>
   </form>
  </div>
  <div class="col-md-2">
   <form action="<?php echo base_url()?>" method="post">
   <!--div class="row">
   <div class="col-xs-12"-->
   <div class="form-group"><label for=""></label>
    <button type="submit" class="btn btn-danger btn-block btn-flat" name="volver">VOLVER</button>
    </div><!-- /.col -->
    </div>
    </form><br>
   </div>  
 </div>
 <!--/div>
</section>
<div-->
