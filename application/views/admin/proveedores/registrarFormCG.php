<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
 <!-- Content Header (Page header) -->
 <section class="content-header">
 <h1>GENERACION DE NUMERACION DE FORMULARIOS CON GESTION JUDICIAL</h1>
 </section>
 <!-- Main content -->
 <section class="content">
 <!-- Default box -->
  <div class="box box-solid">
    <div class="box-body">
     
     <div class="row">
       <div class="col-md-12">
        <?php if($this->session->flashdata("error")):?>
         <div class="alert alert-danger alert-dismissible">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          <p><i class="icon fa fa-ban"></i><?php echo $this->session->flashdata("error")?></p>
         </div>
       </div>
       <?php endif ?>
       <form action="<?php echo base_url();?>mantenimiento/proveedores/addFormCG" method="POST">
    <div class="row">
       <div class="col-md-4">
       <label for="">D.N.I Demandante-Ingrese el numero sin puntos</label>
       <input class="form-control" name="dnidem" id="dnidem" type="text" placeholder="Solo numeros"  onkeypress="return numeros(event)" required="required" onPaste="return false" maxlength="10" >
       </div>
       <div class="col-md-6">
       <label for="">Nombre / Razon Social</label>
       <input type="text" class="form-control" name="ayn" id="ayn" placeholder="Razon Social"  onkeyup="javascript:this.value=this.value.toUpperCase();" required="required" maxlength="100" onPaste="return false" >
       </div>
    </div><br>
    <div class="row">
       <div class="col-md-4">
       <label for="">Ingrese MONTO ORIGINAL $:(con 2 decimales)</label>
       <input class="form-control" name="montor" id="montor" type="text"   onkeypress="return numeros(event)" required="required" onPaste="return false" pattern="[0-9]{1,8}[.][0-9]{2}" placeholder="0.00" >
       </div>
       <div class="col-md-4">
       <label for="">Ingrese HONORARIOS REGULADOS $:(con 2 decimales)</label>
       <input class="form-control" name="hr" id="hrp" type="text"   onkeypress="return numeros(event)" required="required" onPaste="return false" pattern="[0-9]{1,8}[.][0-9]{2}" placeholder="0.00" >
       </div>
    </div><br>
    <div class="row">
       <div class="col-md-4">
       <label for="">Ingrese COSTAS REGULADAS $:(con 2 decimales)</label>
       <input class="form-control" name="totald" id="totald" type="text"   onkeypress="return numeros(event)" required="required" onPaste="return false" pattern="[0-9]{1,8}[.][0-9]{2}" placeholder="0.00" >
       </div>
       <div class="col-md-4">
       <label for="">Ingrese TOTAL DEUDA RECLAMADA $:(con 2 decimales)</label>
       <input class="form-control" name="totald" id="totald" type="text"   onkeypress="return numeros(event)" required="required" onPaste="return false" pattern="[0-9]{1,8}[.][0-9]{2}" placeholder="0.00" >
       </div>
       </div><br>
    <div class="row">
       <div class="col-md-4">
       <label for="">OBSERVACIONES</label>
       <input class="form-control" name="observa" id="observa" maxlength="100" type="text" onkeyup="javascript:this.value=this.value.toUpperCase();" onPaste="return false" >
       </div>
    </div><br>
    <div class="row">
       <div class="col-md-5">
       <button type="submit" class="btn btn-block btn-success btn-flat" >REGISTRAR</button>
       </div>
       </form>
       <form action="<?php echo base_url()?>principal" method="post">
       <div class="col-md-5">
         <button type="submit" class="btn btn-danger btn-block btn-flat" name="volver">VOLVER</button>
       </div>
       </form><br>
    </div>
      
    </div>
    <!-- /.box-body -->
   </div>
  <!-- /.box -->
 </section>
 <!-- /.content -->
 </div>
<!-- /.content-wrapper -->