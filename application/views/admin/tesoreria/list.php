 <!-- Content Wrapper. Contains page content -->
 <div class="content-wrapper">
 <!-- Content Header (Page header) -->
 <section class="content-header">
 <h1>Ordenes de Compra<small>Listado</small></h1>
 </section>
 <!-- Main content -->
 <section class="content">
 <!-- Default box -->
  <div class="box box-solid">
    <div class="box-body">
     <div class="row">
      <div class="col-md-12">
        <a href="<?php echo base_url();?>mantenimiento/tesoreria/add" class="btn btn-primary btn-flat"><span class="fa fa-plus"></span>Nueva Orden</a>
      </div>
     </div>
     <hr>
     
     <div class="row">
       <div class="col-md-12">
       <table id="compras" class="table-bordered btn-hover" style="width:100%">
        <thead>
        <tr><th>#</th><th>Nro.Prov</th>
        <th>Razon Social</th><th>C.U.I.T</th><th>Apellido</th><th>Nombre</th> <th>Domicilio</th><th>Telefono</th> <th>Correo</th>
        </tr>
        </thead>
        <tbody> 
        <?php if(!empty($pedidos)):?>
        <?php foreach($pedidos as $ped):?>
        <tr style="font-family:verdana;font-size:65%;"><td><?php echo $ped->idacreedoreconomia; ?><td><?php echo $ped->nroprov; ?></td> <td ><?php echo $ped->razonsocial; ?></td><td><?php echo $ped->cuit; ?></td><td><?php echo $ped->apellido;?></td><td><?php echo $ped->nombre;?></td><td><?php echo $ped->domicilio;?></td><td><?php echo $ped->tel1;?></td><td><?php echo $ped->email;?></td>
        </tr>
        <?php endforeach;?>
        <?php endif;?>
         </tbody>
       </table>
       </div>
     </div>
    </div>
    <!-- /.box-body -->
   </div>
  <!-- /.box -->
 </section>
 <!-- /.content -->
 </div>
<!-- /.content-wrapper -->