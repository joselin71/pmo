<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
 <!-- Content Header (Page header) -->
 <section class="content-header">
 <h1>Editar Usuarios</h1>
 </section>
 <!-- Main content -->
 <section class="content">
 <!-- Default box -->
  <div class="box box-solid">
    <div class="box-body">
     
     <div class="row">
       <div class="col-md-12">
        <?php if($this->session->flashdata("error")):?>
         <div class="alert alert-danger alert-dismissible">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          <p><i class="icon fa fa-ban"></i><?php echo $this->session->flashdata("error")?></p>
         </div>
       </div>
       <?php endif ?>
       <form action="<?php echo base_url();?>administrador/usuarios/storeEdUser/<?php echo $usuario->idusersistema; ?>" method="POST">
    <div class="row">
       <div class="col-md-4">
       <label for="">DOCUMENTO-Ingrese el numero sin puntos</label>
       <input class="form-control" name="dni" type="text" id="dni" value="<?php echo $usuario->dni; ?>"  onkeypress="return numeros(event)" required="required" onPaste="return false" maxlength="10" readonly="readonly">
       </div>
       <div class="col-md-6">
       <label for="">APELLIDO Y NOMBRE</label>
       <input type="text" class="form-control" name="apellido" type="text" id="apellido" value="<?php echo $usuario->apynom; ?>"  onkeyup="javascript:this.value=this.value.toUpperCase();" required="required" maxlength="45" onPaste="return false" >
       </div>
    </div>
    <div class="row">
      <div class="col-md-2">
       <label for="">ROL</label>
       <h5>Rol Actual:<?php echo $usuario->nivel;?></h5>
       <select name="nivel"  id="nivel" size=1 class="form-control" required="required">
       <option value="">Seleccione</option>
       <option value='1'>OPERADOR</option>
       <option value='2'>USUARIO ECONOMIA</option>
       <option value='3'>COMPRAS</option>
       <option value='4'>SECRETARIO</option>
       </select>
       </div>
       <div class="col-md-8">
       <label for="">SECRETARIA</label>
       <h5>Secretaria Actual:<?php echo $usuario->secretaria;?></h5>
       <select name="secret" id="secret" size=1 class="form-control" required="required" >
       <option value="">Seleccione</option>
       <?php if(!empty($dependencia)):?>
        <?php foreach($dependencia as $ped):?>       
       <option value="<?php echo $ped->sec;?>"><?php echo $ped->detsec;?></option>
       <?php endforeach;?>
        <?php endif;?>
       </select>
       </div>
    </div>
    <div class="row">
       <div class="col-md-2">
       <label for="">TIPO DE USUARIO</label>
       <h5>Tipo de Usuario Actual:<?php echo $usuario->horas; ?></h5>
       <select class="form-control" name="tipouser" size=1 required="required">
       <option value="">Seleccione</option>
       <option value='0'>PEDIDOS</option>
       </select>
       </div>
       <div class="col-md-8">
       <label for="">CORREO ELECTRONICO</label>
       <h5>Correo Actual:<?php echo $usuario->email; ?></h5>
       <input type="text" class="form-control" name="correo" type="email" id="correo" value="<?php echo $usuario->email; ?>" required="required" maxlength="255" onPaste="return false">
       </div>
    </div><br>
    <div class="row">
       <div class="col-md-5">
       <button type="submit" class="btn btn-block btn-success btn-flat" >MODIFICAR USUARIO</button>
       </div>
       </form>
       <form action="<?php echo base_url()?>administrador/usuarios" method="post">
       <div class="col-md-5">
         <button type="submit" class="btn btn-danger btn-block btn-flat" name="volver">VOLVER SIN MODIFICAR</button>
       </div>
       </form><br>
    </div>
      
    </div>
    <!-- /.box-body -->
   </div>
  <!-- /.box -->
 </section>
 <!-- /.content -->
 </div>
<!-- /.content-wrapper -->