<?php
if($this->session->userdata("rol")==99)
{
  ?> 
 
 <!-- Content Wrapper. Contains page content -->
 <div class="content-wrapper">
 <!-- Content Header (Page header) -->
 <section class="content-header">
 <h1>Usuarios del Sistema<small>Listado</small></h1>
 </section>
 <!-- Main content -->
 <section class="content">
 <!-- Default box -->
  <div class="box box-solid">
    <div class="box-body">
     <div class="row">
      <div class="col-md-12">
        <a href="<?php echo base_url();?>administrador/usuarios/add" class="btn btn-primary btn-flat"><span class="fa fa-plus"></span>Agregar Usuario</a>
      </div>
     </div>
     <hr>
     <div class="row">
       <div class="col-md-12">
       <table id="usuarios" class="table table-bordered btn-hover">
        <thead>
        <tr>
        <th>#</th><th>ApyNom</th><th>Usuario</th><th>Estado</th><th>Secretaria</th><th>Opciones</th>
        </tr>
        </thead>
        <tbody> 
        <?php if(!empty($usuarios)):?>
        <?php foreach($usuarios as $usuario):?>
        <tr> <td><?php echo $usuario->idusersistema; ?></td><td><?php echo $usuario->apynom;?></td><td><?php echo $usuario->dni;?></td><td><?php echo $usuario->estado;?></td><td><?php echo $usuario->secretaria;?></td>
        <td>
        <div class="btn-group">
        <a href="<?php echo base_url();?>administrador/usuarios/editUser/<?php echo $usuario->idusersistema;?>"><span class="material-icons" style="color:orange" >edit_off</span></a>
        <a href="<?php echo base_url();?>administrador/usuarios/blanqueaUser/<?php  echo $usuario->idusersistema;?>"><span class="material-icons">build_circle</span></a>
        <a href="<?php echo base_url();?>administrador/usuarios/deleteUser/<?php  echo $usuario->idusersistema;?>" ><span class="material-icons" style="color:red">dangerous</span></a>

        </div>
        
        </td></tr>
        <?php endforeach;?>
        <?php endif;?>
        <?php
        }
     ?>
         </tbody>
       </table>
       </div>
     </div>
    </div>
    <!-- /.box-body -->
   </div>
  <!-- /.box -->
 </section>
 <!-- /.content -->
 </div>
<!-- /.content-wrapper -->