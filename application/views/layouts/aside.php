<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar" id="side-bar">
<!-- sidebar: style can be found in sidebar.less -->
 <section class="sidebar">   
 <!-- sidebar menu: : style can be found in sidebar.less -->
 <ul class="sidebar-menu" data-widget="tree">
  <li class="header">MENU PRINCIPAL</li>
   <li><a href="<?php echo base_url();?>principal"><i class="fa fa-home"></i> <span>Inicio</span></a>
   </li>
     <li class="treeview">
     <a href="#"><i class="fa fa-share-alt"></i> <span>PEDIDOS DE MATERIALES</span><span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a>
     <ul class="treeview-menu">
      <li><a href="<?php echo base_url();?>mantenimiento/materiales"><i class="fa fa-circle-o"></i>Pedir</a></li>
      <?php if(($this->session->userdata("rol")==16)||($this->session->userdata("rol")==99))
        { ?>
      <li><a href="<?php echo base_url();?>mantenimiento/materiales/pedCentral"><i class="fa fa-circle-o"></i>Pedir por Secretaria</a>
      </li>
      <?php } ?>
      <?php if(($this->session->userdata("rol")==3)||($this->session->userdata("rol")==99))
        { ?>
      <li><a href="<?php echo base_url();?>mantenimiento/materiales/gestionCompras"><i class="fa fa-circle-o"></i>Gestionar Compras</a>
      </li>
      <?php } ?>
      <li><a href="<?php echo base_url();?>mantenimiento/materiales/question"><i class="fa fa-circle-o"></i>Consultar Pedidos</a>
      </li>
      <li><a href="<?php echo base_url();?>mantenimiento/materiales/seguimientoPed"><i class="fa fa-circle-o"></i>Seguir Pedidos</a>
      </li>
      <li><a href="<?php echo base_url();?>mantenimiento/materiales/migrarPed"><i class="fa fa-circle-o"></i>Migrar Pedidos</a>
      </li>
      <li class="treeview">
     <a href="#"><i class="fa fa-share-alt"></i> <span>REPORTES</span><span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a>
     <ul class="treeview-menu">
     <li><a href="<?php echo base_url();?>mantenimiento/proveedores/proveeList"><i class="fa fa-circle-o"></i>Listar Proveedores</a></li>
     <?php if(($this->session->userdata("rol")==16)||($this->session->userdata("rol")==99)){ ?>
      <li><a href="<?php echo base_url();?>mantenimiento/materiales/detallePM"><i class="fa fa-circle-o"></i>Exportar Detalle Pedidos</a></li>
     <?php } ?>
     </ul>
      
     </li>
     </ul>
      </li>
  <!--/ul>
    </li-->
    <?php
    if(($this->session->userdata("rol")==16)||($this->session->userdata("rol")==99)){?>
    <li class="treeview">
     <a href="#"><i class="fa fa-share-alt"></i> <span>EMERGENCIA</span><span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a>
     <ul class="treeview-menu">     
      <li><a href="<?php echo base_url();?>mantenimiento/proveedores"><i class="fa fa-circle-o"></i>Registrar Proveedores</a></li>
      <li><a href="<?php echo base_url();?>mantenimiento/proveedores/genClaveProveedor"><i class="fa fa-circle-o"></i>Crear Clave de Proveedores</a></li>
      <li><a href="<?php echo base_url();?>mantenimiento/proveedores/registraFormSG"><i class="fa fa-circle-o"></i>Registrar Acreedores Sin Juicio</a></li>
      <li><a href="<?php echo base_url();?>mantenimiento/proveedores/registraFormCG"><i class="fa fa-circle-o"></i>Registrar Acreedores Con Juicio</a></li>
      
      <li class="treeview">
     <a href="#"><i class="fa fa-share-alt"></i> <span>REPORTES</span><span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a>
     <ul class="treeview-menu">
     <li><a href="<?php echo base_url();?>mantenimiento/proveedores/proveeList"><i class="fa fa-circle-o"></i>Listar Proveedores</a></li>
     <li><a href="<?php echo base_url();?>mantenimiento/proveedores/listFormularios"><i class="fa fa-circle-o"></i>Listar Formularios Registrados</a></li>
     <li><a href="<?php echo base_url();?>mantenimiento/proveedores/proveePresenta"><i class="fa fa-circle-o"></i>Listar Deuda Presentada</a></li>
      <li><a href="<?php echo base_url();?>mantenimiento/proveedores/proveeNoPresenta"><i class="fa fa-circle-o"></i>Listar Deuda No Presentada</a></li>
      <li><a href="<?php echo base_url();?>mantenimiento/proveedores/proveeCompensa"><i class="fa fa-circle-o"></i>Listar Monto de Compensaciones</a></li>
      <li><a href="<?php echo base_url();?>mantenimiento/proveedores/proveeCompensaDetalle"><i class="fa fa-circle-o"></i>Listar Detalle de Compensaciones</a></li>
      <li><a href="<?php echo base_url();?>mantenimiento/proveedores/proveeCompensaDetalleI"><i class="fa fa-circle-o"></i>Listar Detalle de Anexo I</a></li>     
     </ul></li>
   </ul>
  </li>
  <?php }?>
  <?php
        if(($this->session->userdata("rol")==16)||($this->session->userdata("rol")==99))
        {
       ?>
    <li class="treeview">
     <a href="#"><i class="fa fa-share-alt"></i> <span>DEPOSITO</span><span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a>
     <ul class="treeview-menu">
      <li><a href="<?php echo base_url();?>mantenimiento/catalogo/depositos"><i class="fa fa-circle-o"></i>Administrar Depositos </a></li>
      <li><a href="<?php echo base_url();?>mantenimiento/catalogo/unidadesMedida"><i class="fa fa-circle-o"></i>Administrar Unid.Medidas</a></li>
      <li><a href="<?php echo base_url();?>mantenimiento/catalogo/proveedores"><i class="fa fa-circle-o"></i>Administrar Proveedores</a></li>
      <!--li><a href="<!?php echo base_url();?>mantenimiento/catalogo/proveedores"><i class="fa fa-circle-o"></i>Administrar Actividades</a></li-->
      <li class="treeview">
     <a href="#"><i class="fa fa-share-alt"></i> <span>CATALOGO</span><span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a>
     <ul class="treeview-menu">
      <li><a href="<?php echo base_url();?>mantenimiento/catalogo/catalogo"><i class="fa fa-circle-o"></i>ABM Articulos</a></li>
      
     </ul></li>
     <li class="treeview">
     <a href="#"><i class="fa fa-share-alt"></i> <span>MOVIMIENTOS</span><span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a>
     <ul class="treeview-menu">
      <li><a href=""><i class="fa fa-circle-o"></i>Cargar Recepcion </a></li>
      <li><a href=""><i class="fa fa-circle-o"></i>Cargar Transferencias</a></li>
     </ul></li>
     <li class="treeview">
     <a href="#"><i class="fa fa-share-alt"></i> <span>REPORTES</span><span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a>
     <ul class="treeview-menu">
      <li><a href=""><i class="fa fa-circle-o"></i>Ver Movimientos </a></li>
      
     </ul></li>
     </ul></li>
    <?php
        }
        ?>
    
    
    <?php
        if(($this->session->userdata("rol")==16)||($this->session->userdata("rol")==99))
        {
       ?>
    <li class="treeview">
     <a href="#"><i class="fa fa-share-alt"></i> <span>CONFIGURACION</span><span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a>
     <ul class="treeview-menu">
      <li><a href="<?php echo base_url();?>mantenimiento/materiales/programas"><i class="fa fa-circle-o"></i>Cargar Programas </a></li>
      <li><a href="<?php echo base_url();?>mantenimiento/materiales/subprogramas"><i class="fa fa-circle-o"></i>Cargar Sub-Programas</a></li>
      <li><a href="<?php echo base_url();?>mantenimiento/catalogo/rubros"><i class="fa fa-circle-o"></i>Administrar Rubros</a></li>
     <li><a href="<?php echo base_url();?>mantenimiento/catalogo/subrubros"><i class="fa fa-circle-o"></i>Administrar Sub-Rubros</a></li>
     </ul></li>
    <?php
        }
        ?>
        <li class="treeview">
     <a href="#"><i class="fa fa-share-alt"></i> <span>USUARIOS</span><span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a>
     <ul class="treeview-menu">
      <?php if($this->session->userdata("rol")==99){ ?>
      <li><a href="<?php echo base_url();?>administrador/usuarios"><i class="fa fa-circle-o"></i>ABM</a></li>
      <?php } ?>
      <li><a href="<?php echo base_url();?>administrador/usuarios/modificaClave"><i class="fa fa-circle-o"></i>Modificar Clave</a></li>
     </li>
 </ul>
 <?php
        if($this->session->userdata("rol")==99)
        {
       ?>
    <li class="treeview">
     <a href="#"><i class="fa fa-share-alt"></i> <span>TESORERIA</span><span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a>
     <ul class="treeview-menu">
      <li><a href="<?php echo base_url();?>mantenimiento/tesoreria/compras"><i class="fa fa-circle-o"></i>Ordenes de Compra</a></li>
      <li><a href="<?php echo base_url();?>mantenimiento/tesoreria/pagos"><i class="fa fa-circle-o"></i>Ordenes de Pago</a></li>
      <li><a href="<?php echo base_url();?>mantenimiento/tesoreria/recibos"><i class="fa fa-circle-o"></i>Recibos</a></li>
      <li><a href="<?php echo base_url();?>mantenimiento/tesoreria/rendiciones"><i class="fa fa-circle-o"></i>Rendiciones</a></li>
      <li class="treeview">
     <a href="#"><i class="fa fa-share-alt"></i> <span>CONFIGURACIONES</span><span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a>
     <ul class="treeview-menu">
      <li><a href="<?php echo base_url();?>mantenimiento/materiales/programas"><i class="fa fa-circle-o"></i>Combustible</a></li>
      <li><a href="<?php echo base_url();?>mantenimiento/materiales/programas"><i class="fa fa-circle-o"></i>Responsables</a></li>
      <li><a href="<?php echo base_url();?>mantenimiento/materiales/programas"><i class="fa fa-circle-o"></i>Numeracion</a></li>
      <li><a href="<?php echo base_url();?>mantenimiento/materiales/programas"><i class="fa fa-circle-o"></i>Valores</a></li>
      
     
     
     </ul></li>
     <li class="treeview">
     <a href="#"><i class="fa fa-share-alt"></i> <span>WebService</span><span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a>
     <ul class="treeview-menu">
      <li><a href="<?php echo base_url();?>mantenimiento/apirest/apiCatalogo"><i class="fa fa-circle-o"></i>Catalogo</a></li>
      
      
     
     
     </ul></li>
      
     
     
     </ul></li>
     <li class="treeview">
     <a href="#"><i class="fa fa-share-alt"></i> <span>WebService</span><span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a>
     <ul class="treeview-menu">
     <li><a href="<?php echo base_url();?>mantenimiento/apirest"><i class="fa fa-circle-o"></i>Documentacion</a></li>
     <!--li><a href="<?php echo base_url();?>mantenimiento/apirest/apiCatalogo"><i class="fa fa-circle-o"></i>Catalogo</a></li-->
     </ul></li>
    <?php
        }
        ?>
    
    </li>
  </ul>
 </section>
 <!-- /.sidebar -->
</aside>
