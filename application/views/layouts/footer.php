<footer class="main-footer">
            <div class="pull-right hidden-xs">
                <b>Version</b> 4.0.1
            </div>
            <strong>Copyright &copy; 2021-2022 <a href="">SECRETARIA DE HACIENDA Y FINANZAS - MUNICIPALIDAD DE LA CIUDAD DE RESISTENCIA</a>.</strong> All rights
            reserved.
        </footer>
    </div>
    <!-- ./wrapper -->
<!-- jQuery 3 -->
<script src="<?php echo base_url();?>assets/template/jquery/jquery.min.js"></script>
<!-- jQuery Print -->
<script src="<?php echo base_url();?>assets/template/jquery-print/jquery.print.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url();?>assets/template/bootstrap/js/bootstrap.js"></script>
<!-- SlimScroll -->
<!--script src="<!?php echo base_url();?>assets/template/jquery-slimscroll/jquery.slimscroll.min.js"></script-->

<!-- DataTables -->
<script src="<?php echo base_url();?>assets/template/datatables/jquery.dataTables.js"></script>
<script src="<?php echo base_url();?>assets/template/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<!-- DataTables Export -->
<script src="<?php echo base_url();?>assets/template/dataTables-export/js/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url();?>assets/template/dataTables-export/js/buttons.flash.min.js"></script>
<script src="<?php echo base_url();?>assets/template/dataTables-export/js/jszip.min.js"></script>
<script src="<?php echo base_url();?>assets/template/dataTables-export/js/pdfmake.min.js"></script>
<script src="<?php echo base_url();?>assets/template/dataTables-export/js/vfs_fonts.js"></script>
<script src="<?php echo base_url();?>assets/template/dataTables-export/js/buttons.html5.min.js"></script>
<script src="<?php echo base_url();?>assets/template/dataTables-export/js/buttons.print.min.js"></script>
<!-- FastClick -->
<!--script src="<!?php echo base_url();?>assets/template/fastclick/lib/fastclick.js"></script-->
<!-- AdminLTE App -->
<script type="text/javascript" src="<?php echo base_url();?>assets/template/js/adminlte.js"></script>
<!--solo numeros -->
<script>
function numeros(e){
tecla = (document.all) ? e.keyCode : e.which;
//Tecla de retroceso para borrar, siempre la permite
if (tecla==8 || tecla==46){return true;}
// Patron de entrada, en este caso solo acepta numeros
patron =/[0-9]/;tecla_final = String.fromCharCode(tecla);
return patron.test(tecla_final);
}
</script>
<script>
$(document).ready(function(){
    var base_url="<?php echo base_url();?>";
    $('#example').DataTable({
        dom: 'Bfrtip',
        buttons:[
            {
                extend: 'excelHtml5',
                title: "Listado ",
                exportOptions:{
                    columns:[0,1,2,3,4,5,6,7]
                }
            },
            {
                extend: 'pdfHtml5',
                title: "Listado",
                exportOptions:{
                    columns:[0,1,2,3,4,5,6,7]
                }
            }
            ],
            "language": {
            "lengthMenu": "Mostrar _MENU_ registros por pagina",
            "zeroRecords": "No se encontraron resultados en su busqueda",
            "searchPlaceholder": "Buscar registros",
            "info": "Mostrando registros de _START_ al _END_ de un total de  _TOTAL_ registros",
            "infoEmpty": "No existen registros",
            "infoFiltered": "(filtrado de un total de _MAX_ registros)",
            "search": "Buscar:",
            "paginate": {
                "first": "Primero",
                "last": "Último",
                "next": "Siguiente",
                "previous": "Anterior"
            },
        }
    });
    $('#compras').DataTable({
        dom: 'Bfrtip',
        buttons:[],
            "language": {
            "lengthMenu": "Mostrar _MENU_ registros por pagina",
            "zeroRecords": "No se encontraron resultados en su busqueda",
            "searchPlaceholder": "Buscar registros",
            "info": "Mostrando registros de _START_ al _END_ de un total de  _TOTAL_ registros",
            "infoEmpty": "No existen registros",
            "infoFiltered": "(filtrado de un total de _MAX_ registros)",
            "search": "Buscar:",
            "paginate": {
                "first": "Primero",
                "last": "Último",
                "next": "Siguiente",
                "previous": "Anterior"
            },
        }
    });
    $('#subrubros').DataTable({
        dom: 'Bfrtip',
        buttons:[{
                extend: 'pdfHtml5',
                title: "Listado",
                orientation : 'landscape',
                exportOptions:{
                    columns:[1,2,3,4,5]
                }
            
        }],
            "language": {
            "lengthMenu": "Mostrar _MENU_ registros por pagina",
            "zeroRecords": "No se encontraron resultados en su busqueda",
            "searchPlaceholder": "Buscar registros",
            "info": "Mostrando registros de _START_ al _END_ de un total de  _TOTAL_ registros",
            "infoEmpty": "No existen registros",
            "infoFiltered": "(filtrado de un total de _MAX_ registros)",
            "search": "Buscar:",
            "paginate": {
                "first": "Primero",
                "last": "Último",
                "next": "Siguiente",
                "previous": "Anterior"
            },
        }
    });
    $('#usuarios').DataTable({
        dom: 'Bfrtip',
        buttons:[
            {
                extend: 'excelHtml5',
                title: "Listado",
                exportOptions:{
                    columns:[0,1,2,3,4]
                }
            },
            {
                extend: 'pdfHtml5',
                title: "Listado",
                exportOptions:{
                    columns:[0,1,2,3,4]
                }
            }
            ],
            "order": [[ 1, "desc" ]],
            "language": {
            "lengthMenu": "Mostrar _MENU_ registros por pagina",
            "zeroRecords": "No se encontraron resultados en su busqueda",
            "searchPlaceholder": "Buscar registros",
            "info": "Mostrando registros de _START_ al _END_ de un total de  _TOTAL_ registros",
            "infoEmpty": "No existen registros",
            "infoFiltered": "(filtrado de un total de _MAX_ registros)",
            "search": "Buscar:",
            "paginate": {
                "first": "Primero",
                "last": "Último",
                "next": "Siguiente",
                "previous": "Anterior"
            },
        }
    });
    $('#articulos').DataTable({
        dom: 'Bfrtip',
        buttons:[{
                extend: 'pdfHtml5',
                title: "Catalogo",
                orientation : 'landscape',
                exportOptions:{
                    columns:[0,1,2,3,4,5,6,7,8]
                }
            
        }],
            "language": {
            "lengthMenu": "Mostrar _MENU_ registros por pagina",
            "zeroRecords": "No se encontraron resultados en su busqueda",
            "searchPlaceholder": "Buscar registros",
            "info": "Mostrando registros de _START_ al _END_ de un total de  _TOTAL_ registros",
            "infoEmpty": "No existen registros",
            "infoFiltered": "(filtrado de un total de _MAX_ registros)",
            "search": "Buscar:",
            "paginate": {
                "first": "Primero",
                "last": "Último",
                "next": "Siguiente",
                "previous": "Anterior"
            },
        }
    });

    $('#example1').DataTable({
        dom: 'Bfrtip',
        buttons:[
            {
                extend: 'excelHtml5',
                title: "Movimientos",
                exportOptions:{
                    columns:[0,1,2,3,4,5,6,7,8,9,10]
                }
            },
            {
                extend: 'pdfHtml5',
                orientation: 'landscape',
                title: "Movimientos",
                exportOptions:{
                    columns:[0,1,2,3,4,5,6,7,8,9,10]
                }
            }
            ],
        "order": [[ 1, "desc" ]],
        "pageLength": 15,
        "language": {
            "lengthMenu": "Mostrar _MENU_ registros por pagina",
            "zeroRecords": "No se encontraron resultados en su busqueda",
            "searchPlaceholder": "Buscar registros",
            "info": "Mostrando registros de _START_ al _END_ de un total de  _TOTAL_ registros",
            "infoEmpty": "No existen registros",
            "infoFiltered": "(filtrado de un total de _MAX_ registros)",
            "search": "Buscar:",
            "paginate": {
                "first": "Primero",
                "last": "Último",
                "next": "Siguiente",
                "previous": "Anterior"
            },
        }
    });
    $('#example2').DataTable({
        dom: 'Bfrtip',
        buttons:[
            {
                extend: 'excelHtml5',
                title: "Movimientos",
                exportOptions:{
                    columns:[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17]
                }
            }
            ],
        "pageLength": 15,
        "language": {
            "lengthMenu": "Mostrar _MENU_ registros por pagina",
            "zeroRecords": "No se encontraron resultados en su busqueda",
            "searchPlaceholder": "Buscar registros",
            "info": "Mostrando registros de _START_ al _END_ de un total de  _TOTAL_ registros",
            "infoEmpty": "No existen registros",
            "infoFiltered": "(filtrado de un total de _MAX_ registros)",
            "search": "Buscar:",
            "paginate": {
                "first": "Primero",
                "last": "Último",
                "next": "Siguiente",
                "previous": "Anterior"
            },
        }
    });
    $(document).on("click",".btn-view-Catalogo",function(){
        valor_id=$(this).val();
        $.ajax({
            url:base_url + "mantenimiento/catalogo/view",
            type:"POST",
            dataType:"html",
            data:{id:valor_id},
            success:function(data){
                $("#modal-default .modal-body").html(data);
            }
        });
    });
    $(document).on("click",".btn-print",function(){
        $("#modal-default .modal-body").print();
    });
   

})
</script>
<script>
    window.onunload = function(){};

if (window.history.state != null && window.history.state.hasOwnProperty('historic')) {
       if (window.history.state.historic == true) {
           document.body.style.display = 'none';
           window.history.replaceState({historic: false}, '');
           window.location.reload();
       } else {
           window.history.replaceState({historic  : true}, '');
       }
} else {
       window.history.replaceState({historic  : true}, '');
}
</script>


<script>
    window.onload=function(){
       $('#onload').fadeOut();       
    }
</script>

</body>
</html>
